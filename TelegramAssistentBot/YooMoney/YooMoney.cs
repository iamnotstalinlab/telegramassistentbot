using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

class YooMoneyNotification {
        public string url { get; set; }
        public string operation_id { get; set; }
        public string notification_type { get; set; }
        public string datetime { get; set; }
        public string sha1_hash { get; set; }
        public string sender { get; set; }
        public string codepro { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string withdraw_amount { get; set; }
        public string label { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string fathersname { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string building { get; set; }
        public string suite { get; set; }
        public string flat { get; set; }
        public string phone { get; set; }
        public string email { get; set; }

        public YooMoneyNotification() {}
    }
class YooMoney{

class AddrPort {
    public IPAddress Ip;
    public string strIP = "*";
    public int Port = 8080;
    //public string[] Certificates = new string[2] { "server.pfx", "localhost.p12" };
    public string[] Certificates = new string[1] { "server.pfx" };

    public AddrPort() {
        //// Getting host name 
        //string host = Dns.GetHostName();

        ////// Getting ip address using host name 
        //IPHostEntry ip = Dns.GetHostEntry(host);
        //for (int i = 0; i < ip.AddressList.Length - 1; i++) {
        //    Console.WriteLine(ip.AddressList[i].ToString());
        //}

        //Ip = ip.AddressList[1];

        Ip = IPAddress.Any;
    }
}

    private void TcpSslServerRun(object inServer) {
        //Starts Tcp Listener


        AddrPort server = (AddrPort)inServer;
        TcpListener listener = new TcpListener(server.Ip, server.Port);
        listener.Start();

        //Wait for clients
        Console.WriteLine($"Server is runned on: https://{server.Ip}:{server.Port}/");

        //Read self-signed certificate
        var certificate = new X509Certificate2(server.Certificates[1], "password");
        Console.WriteLine("Thumprint:\n" + certificate.Thumbprint);

        while (true) {

            var client = listener.AcceptTcpClient();

            ////Get client stream
            NetworkStream stream = client.GetStream();

            //Wrap client in SSLstream
            SslStream sslStream = new SslStream(stream, false);
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            try {
                //Authenticate server with self-signed certificate, false(for not requiring client authentication), SSL protocol type, ...
                sslStream.AuthenticateAsServer(certificate,
                    false,
                    System.Security.Authentication.SslProtocols.Tls12,
                    false);

            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                if (ex.InnerException.Message != null) {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }


            if (sslStream.IsAuthenticated) {

                // Read the  message sent by the client. The client signals the end of the message using the "<EOF>" marker.
                byte[] buffer = new byte[2048];
                StringBuilder messageData = new StringBuilder();
                int bytes = -1;
                do {
                    // Read the client's test message.
                    bytes = sslStream.Read(buffer, 0, buffer.Length);

                    // Use Decoder class to convert from bytes to UTF8 in case a character spans two buffers.
                    //Decoder decoder = Encoding.UTF8.GetDecoder();
                    //char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
                    //decoder.GetChars(buffer, 0, bytes, chars, 0);
                    //messageData.Append(chars);

                    string message = System.Text.Encoding.UTF8.GetString(buffer);

                    //YooMoneyNotification yooMoneyNotification = ParseYooMoneyNotification(message.ToString());
                    //CompareSha1Hash(yooMoneyNotification, yooMoneyNotification.sha1_hash);
                    Console.WriteLine(messageData);

                    string strSSLResponse = "HTTP/1.0 200 Connection established\r\n\r\n";
                    byte[] bytSSLResponse = Encoding.UTF8.GetBytes(strSSLResponse);
                    sslStream.Write(bytSSLResponse, 0, bytSSLResponse.Length);

                    byte[] bytSSLResponse2 = Encoding.UTF8.GetBytes($"Iosif Stalin! {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                    sslStream.Write(bytSSLResponse2);
                    sslStream.Close();
                    break;
                    // Check for EOF or an empty message.
                    if (messageData.ToString().IndexOf("<EOF>") != -1) {
                        break;
                    }
                } while (bytes != 0);
            }
        }

    }

    string secretWord = "vyk6u+r21j5oAGP/0zbTy+Ww";
    private void CompareSha1Hash(YooMoneyNotification yooNotify, string incomingHash ) {
        string testUrl = "p2p-incoming&1234567&300.00&643&2011-07-01T09:00:00.000+04:00&41001XXXXXXXX&false&01234567890ABCDEF01234567890&YM.label.12345";
        string testHash = "a2ee4a9195f4a90e893cff4f62eeba0b662321f9";

        
        string url = $"{yooNotify.notification_type}&{yooNotify.operation_id}&{yooNotify.amount}&{yooNotify.currency}" +
            $"&{yooNotify.datetime}&{yooNotify.sender}&{yooNotify.codepro.ToString().ToLower()}&{secretWord}&{yooNotify.label}";
        
        
        byte[] hashBytes= SHA1.HashData(Encoding.UTF8.GetBytes(testUrl));
        Console.WriteLine();

        using var sha1 = SHA1.Create();
        string sha1Hash =Convert.ToHexString(sha1.ComputeHash(Encoding.UTF8.GetBytes(url)));
        sha1Hash = sha1Hash.ToLower();

        Console.WriteLine();
        Console.WriteLine("URL:" + url);
        Console.WriteLine();

        if (sha1Hash == yooNotify.sha1_hash) {
            Console.WriteLine("The hash is EQUAL to testhash.");
            
        } else {
            Console.WriteLine("The hash in NOT EQUAL to testhash.");
        }

    }





    private void TcpServerRun(object addressPort) {

        AddrPort addrPort = (AddrPort) addressPort;
        IPAddress address = addrPort.Ip;
        int port = addrPort.Port;

        TcpListener listener = new TcpListener(address, port);
        listener.Start();
        
        //Wait for clients
        Console.WriteLine($"Server is runned on: https://localhost:{port}/");

        while (true) {

            var client = listener.AcceptTcpClient();

            NetworkStream stream = client.GetStream();

            // Read the  message sent by the client. The client signals the end of the message using the "<EOF>" marker.
            byte[] buffer = new byte[2048];
            StringBuilder messageData = new StringBuilder();
            int bytes = -1;
            do {
                // Read the client's test message.
                //bytes = sslStream.Read(buffer, 0, buffer.Length);
                bytes = stream.Read(buffer, 0, buffer.Length);

                // Use Decoder class to convert from bytes to UTF8 in case a character spans two buffers.
                Decoder decoder = Encoding.UTF8.GetDecoder();
                char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
                decoder.GetChars(buffer, 0, bytes, chars, 0);
                messageData.Append(chars);
                string sMessage = messageData.ToString().Replace("%3A", ":");
                string console = System.Uri.UnescapeDataString(sMessage);
                Console.WriteLine(console);

                //YooMoneyNotification yooMoneyNotification = ParseYooMoneyNotification(sMessage);
                //CompareSha1Hash(yooMoneyNotification, yooMoneyNotification.sha1_hash);
                //Console.WriteLine(messageData);

                string strSSLResponse = "HTTP/1.0 200 Connection established\r\n\r\n";
                byte[] bytSSLResponse = Encoding.ASCII.GetBytes(strSSLResponse);
                //sslStream.Write(bytSSLResponse, 0, bytSSLResponse.Length);
                stream.Write(bytSSLResponse, 0, bytSSLResponse.Length);

                byte[] bytSSLResponse2 = Encoding.UTF8.GetBytes("Hello World!");
                //sslStream.Write(bytSSLResponse2);
                //sslStream.Close();
                stream.Write(bytSSLResponse2);
                stream.Close();
                break;
                // Check for EOF or an empty message.
                if (messageData.ToString().IndexOf("<EOF>") != -1) {
                    break;
                }
            } while (bytes != 0);
            //}
        }
    }


    private void SendTcpPost(ref TcpClient client) {

        string postDataAsString = @"POST /method/auth.exchangeSilentAuthToken HTTP/1.1" + Environment.NewLine +
            "Host: 170.55.7.163" + Environment.NewLine +
            "Content-Length: 25" + Environment.NewLine +
            Environment.NewLine +
            Environment.NewLine +
            "P270=2&sid=6aec588bfe62";

        byte[] postDataBinary = System.Text.Encoding.UTF8.GetBytes(postDataAsString);

            // make post request
            client.Client.Send(postDataBinary);

            // get response
            byte[] bytes = new byte[1024];
            int lengthOfResponse = client.Client.Receive(bytes);

            var resp = System.Text.Encoding.UTF8.GetString(bytes, 0, lengthOfResponse);

    }


    //Callback function that allows all certificates
    static bool CertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
        return true;
    }

    


    private void SendSslRequest() {
        // Create a TCP/IP client socket.
        // machineName is the host running the server application.

        string hostName = $"https://id.vk.com";
        //string hostName= $"https://yoomoney.ru/api/request-payment";
        int port = 443;
        

        TcpClient client = new TcpClient(hostName, port);
        Console.WriteLine("Client connected.");
        // Create an SSL stream that will close the client's stream.
        SslStream sslStream = new SslStream(
            client.GetStream(),
            false,
            CertificateValidationCallback);

        // The server name must match the name on the server certificate.
        var certificate = new X509Certificate2("server.pfx", "password");
        Console.WriteLine("Thumprint:\n" + certificate.Thumbprint);

        X509Certificate2Collection certificates = new X509Certificate2Collection();
        certificates.Add(certificate);

        try {  
            sslStream.AuthenticateAsClient(hostName, certificates, SslProtocols.Tls12, true);

            byte[] buffer = new byte[5120];
            int bytes;
            string url = $"https://id.vk.com/auth";
            var pqr = string.Format("GET {0}  HTTP/1.1\r\nHost: {1}\r\n\r\n", url, "mytestapp.azurewebsites.net");
            byte[] request = Encoding.UTF8.GetBytes(pqr);
            sslStream.Write(request, 0, request.Length);
            var ppp = ReadStream(sslStream);
            sslStream.Flush();

        }
                catch (AuthenticationException e)
                {
            Console.WriteLine("Exception: {0}", e.Message);
            if (e.InnerException != null) {
                Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
            }
            Console.WriteLine("Authentication failed - closing the connection.");
            client.Close();
            return;
        }


    }
    private static string ReadStream(Stream stream) {
        byte[] resultBuffer = new byte[2048];
        string value = "";
        //requestStream.BeginRead(resultBuffer, 0, resultBuffer.Length, new AsyncCallback(ReadAsyncCallback), new result() { buffer = resultBuffer, stream = requestStream, handler = callback, asyncResult = null });
        do {
            try {
                int read = stream.Read(resultBuffer, 0, resultBuffer.Length);
                value += Encoding.UTF8.GetString(resultBuffer, 0, read);

                if (read < resultBuffer.Length)
                    break;
            } catch { break; }
        } while (true);
        return value;
    }










    string redirectUrl = "https://e0f1-185-16-29-189.ngrok-free.app";

    string recieverNumber = "4100118435463953";
    string recieverPhone = "79782541571";
    string token = "4100118400340710.623A6B4510188770EA507D315B66D14E4F1315FA8C852F1A69379078AE89250AE48D5EB4B9593C43DE2A55F9A1AE56F9C8279D4D319822EB1D5F7818AF15C4F602D135145DAF9425E5D420CC76D0DA1729E9179B56ADAF0467ABCBA6FA3BE09531DB9013567378E76DA1FE59E55AB90250D7E3009AC921FC558567DFFBA882B4";

    private void SendPayingOut() {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

        //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + token);
        //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

        string req = "pattern_id=p2p" +
            $"&to={recieverPhone}" +
            "&identifier_type=phone" +
            "&amount=10.00" +
            "&message=Проверочный платёж" +
            "&comment=Проверяем";
        var data = new StringContent(req, Encoding.UTF8, "application/x-www-form-urlencoded");

        string url = $"https://yoomoney.ru/api/request-payment";

        //HttpResponseMessage response = client.PostAsync(url, data).Result;
        //string result = response.Content.ReadAsStringAsync().Result;
        //Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(result));

    }

    private void SendProcessPayment() {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

        //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + token);
        //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

        string req = "request_id=2ce3062a-0011-5000-9000-1787f3aecaa3";
        var data = new StringContent(req, Encoding.UTF8, "application/x-www-form-urlencoded");

        string url = $"https://yoomoney.ru/api/process-payment";
        HttpResponseMessage response = client.PostAsync(url, data).Result;
        string result = response.Content.ReadAsStringAsync().Result;
        Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(result));

    }

    private YooMoneyNotification ParseYooMoneyNotification(string request) {
        YooMoneyNotification yooNotify = new YooMoneyNotification();

        int index = request.IndexOf("notification_type");
        yooNotify.url = request.Remove(0, index);
        string[] parameters = yooNotify.url.Split('&');
        for (int i =0; i <parameters.Length; i++) {
            string[] pair = parameters[i].Split('=');
            string key = pair[0];
            string value = pair[1];
            
            switch (key) {
                case "operation_id":
                    //yooNotify.operation_id = long.Parse(value);
                    yooNotify.operation_id = value;
                    break;
                case "notification_type":
                    yooNotify.notification_type = value;
                    break;
                case "datetime":
                    yooNotify.datetime = value;
                    break;
                case "sha1_hash":
                    yooNotify.sha1_hash = value;
                    break;
                case "sender":
                    yooNotify.sender = value;
                    break;
                case "codepro":
                    yooNotify.codepro = value;
                    break;
                case "currency":
                    yooNotify.currency = value;
                    break;
                case "amount":
                    yooNotify.amount = value;
                    break;
                case "withdraw_amount":
                    yooNotify.withdraw_amount = value;
                    break;
                case "label":
                    yooNotify.label = value;
                    break;
                case "lastname":
                    yooNotify.lastname = value;
                    break;
                case "firstname":
                    yooNotify.firstname = value;
                    break;
                case "fathersname":
                    yooNotify.fathersname = value;
                    break;
                case "zip":
                    yooNotify.zip = value;
                    break;
                case "city":
                    yooNotify.city = value;
                    break;
                case "street":
                    yooNotify.street = value;
                    break;
                case "building":
                    yooNotify.building = value;
                    break;
                case "suite":
                    yooNotify.suite = value;
                    break;
                case "flat":
                    yooNotify.flat = value;
                    break;
                case "phone":
                    yooNotify.phone = value;
                    break;
                case "email":
                    yooNotify.email = value;
                    break;
            }
        }

        return yooNotify;
    }

    private void TCPRunServer() {

        //HttpClientHandler handler = new HttpClientHandler();
        //handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
        //serverCertificate = new X509Certificate2("./certificate.pfx", "12345678");// Has to be a private X509cert with key
        TcpListener listener = new TcpListener(IPAddress.Loopback, 8080);
        try {

            listener.Start();
            while (true) {
                Console.WriteLine("Waiting for a client to connect on  " + IPAddress.Loopback + ":" + 8080);

                // Application blocks while waiting for an incoming connection.
                // Type CNTL-C to terminate the server.

                TcpClient client = new TcpClient();
                client = listener.AcceptTcpClient();
                //ProcessClient(client);
            }
        } catch (Exception ex) {

            Console.WriteLine("Ошибка в РанСервере");
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.Data);
            Console.WriteLine(ex.HResult);
            Console.WriteLine(ex.StackTrace);
            Console.WriteLine(ex.Source);

        } finally {
            listener.Stop();
        }
    }



            private async void HTTPSServerRun() {
        var handler = new HttpClientHandler();
        handler.ClientCertificateOptions = ClientCertificateOption.Manual;
        handler.SslProtocols = SslProtocols.Tls12;
        handler.ClientCertificates.Add(new X509Certificate2("cert.crt"));
        var client = new HttpClient(handler);
        var result = client.GetAsync("http://*:8080/").GetAwaiter().GetResult();
        string s = "STALIN";
    }



    private void SendPost(string url, string request) {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Host = "yoomoney.ru";

        var data = new StringContent(request, Encoding.UTF8, "application/x-www-form-urlencoded");
        HttpResponseMessage response = client.PostAsync(url, data).Result;
        string result = response.Content.ReadAsStringAsync().Result;
        Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(result));
    }

            //Авторизуемся
    private async void AuthorizationYooMoney() {

        //var values = new Dictionary<string, string>{
        //    { "client_id", "261B29DEE24729842FD6433A60CAC66AD0CB2A6E52EBBC1E5C3464794E735131" },
        //    { "response_type", "code" },
        //    { "redirect_uri", redirectUrl},
        //    { "scope", "account-info operation-history payment-p2p"}
        //};

        string req = "client_id=261B29DEE24729842FD6433A60CAC66AD0CB2A6E52EBBC1E5C3464794E735131" +
            "&response_type=code" +
            $"&redirect_uri={redirectUrl}" +
            "&scope=account-info operation-history payment-p2p";
        string url = $"https://yoomoney.ru/oauth/authorize";
        SendPost(url, req);


    }

        private void SendTimingToken(HttpListenerRequest request) {
        string token = string.Empty;
        if (!string.IsNullOrEmpty(request.Url.Query)) {
            token = request.Url.Query.Remove(0, "?code=".Length);
        }
        string req = $"code={token}" +
            "&client_id=261B29DEE24729842FD6433A60CAC66AD0CB2A6E52EBBC1E5C3464794E735131" +
            "&grant_type=authorization_code" +
            $"&redirect_uri={redirectUrl}";
        string url = $"https://yoomoney.ru/oauth/token";
        //SendPost(url, req);
    }
}