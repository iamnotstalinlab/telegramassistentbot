﻿//Модуль объекта автора обращения.
//

using Telegram.Bot.Types;

namespace TelegramAssistentBot {
    public class Author {

        public long Id { get;}

        public string CallbackData { get; set; } = "null";
        public int IdEditMessage { get; set; }

        //Создаём экземпляр автора
        public Author(User user) {
            Id = user.Id;
        }
    }
}
