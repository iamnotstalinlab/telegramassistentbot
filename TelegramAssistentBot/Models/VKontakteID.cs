﻿using System;
using System.Collections.Generic;

namespace TelegramAssistentBot.Models{

    public class VK_VerificationRequest {
        public string type { get; set; }
        public int auth { get; set; }
        public VkUser user { get; set; }
        public string token { get; set; }
        public int ttl { get; set; }
        public string uuid { get; set; }
        public string hash { get; set; }
        public bool loadExternalUsers { get; set; }
    }

    public class VkUser {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string avatar { get; set; }
        public object avatar_base { get; set; }
        public string phone { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class VkIdAccessToken {
        public string access_token { get; set; }
        public string access_token_id { get; set; }
        public int user_id { get; set; }
        public bool additional_signup_required { get; set; }
        public bool is_partial { get; set; }
        public bool is_service { get; set; }
        public int source { get; set; }
        public string source_description { get; set; }
        public int expires_in { get; set; }
    }

    public class VkIdAccessTokenResponse {
        public VkIdAccessToken response { get; set; }
    }


    
    
    // Подробное описание всех параметров https://dev.vk.com/ru/method/account.getProfileInfo
    public class AccountVerificationProfile {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int sex { get; set; }
        public string middle_name { get; set; }
        public string birthdate { get; set; }
    }

    public class VkIdProfileInfo {
        public int id { get; set; }
        public string home_town { get; set; }
        public string status { get; set; }  
        public string photo_200 { get; set; }
        public bool is_service_account { get; set; }
        public string bdate { get; set; }
        public AccountVerificationProfile Account_verification_profile { get; set; }
        public string verification_status { get; set; }
        public List<object> promo_verifications { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int bdate_visibility { get; set; }
        public string phone { get; set; }
        public int relation { get; set; }
        public string screen_name { get; set; }
        public int sex { get; set; }
    }

    public class VkIdProfileInfoResponse {
        public VkIdProfileInfo response { get; set; }
    }





}

