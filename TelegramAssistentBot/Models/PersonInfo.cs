﻿//Модуль объекта с личной информацией пользователя из архива.
//

using System;
using Telegram.Bot.Types;
using TelegramAssistentBot.Appealsandorders;
using TelegramAssistentBot.Models;

namespace TelegramAssistentBot {
    public class PersonInfo : User {

        public string LastSolutionLink { get; set; }
        public string ArrestReportLink { get; set; }
        public string RemovingReportLink { get; set; }
        public string SpamReportLink { get; set; }

        public System.DateTime ArrestEndDateTime { get; set; }
        public long ArrestEndDateTimeNumeric { get; set; }
        public string Status { get; set; }
        public int SpamCount { get; set; }

        public VkVerification Verification { get; set; }
        public PersonInfo(long idPerson) {
            Id = idPerson;
            Verification = new VkVerification();
        }

        public PersonInfo(long idPerson, string firstName) {
            Id = idPerson;
            FirstName = firstName;
            Status = null;
            Verification = new VkVerification();
        }

    }
}
