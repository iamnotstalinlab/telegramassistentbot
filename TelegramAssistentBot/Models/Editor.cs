﻿//Модуль объекта последнего ответственного редактора (того кто последним правил основные настройки)
//

namespace TelegramAssistentBot.QiwiModels {
    public class Editor {

        public long Id { get; set; }
        public Editor(long id) {
            Id = id;
        }
        public Editor() { }

    }
}
