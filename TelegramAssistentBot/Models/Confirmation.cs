﻿//Модуль объекта подтверждения результата обращения
//

namespace TelegramAssistentBot {
    public class Confirmation {

        public int MessageId { get; set; }
        public string Text { get; set; } = "null";
        public int LastSolutionMessageId { get; set; }

        public Confirmation() {}
    }
}
