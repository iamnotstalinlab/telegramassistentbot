﻿//Модуль объекта с дополнительным уникальным номером чата. Дополнен свойством "Type" (тип),
//тк без типа сложно определить права администратора (права для чата и канала у представителей разные)
//

using Telegram.Bot.Types;
namespace TelegramAssistentBot {
    public class ExternalChatId :ChatId {

        public string Type { get; set; } = "null";   //Тип чата (Супергруппа или канал)
        public string Link {get;set;} ="null";

        public string ArchiveChatLink {get;set;} ="null";
        public ExternalChatId(long chatId): base(chatId) {}
        public ExternalChatId(string username) : base(username) { }
    }
}
