﻿using System;
namespace TelegramAssistentBot.Models{

    public class JoinRequest {
        public DateTime JoinEndDateTime { get; set; }
        public long UserId { get; set; }
        public string FirstName { get; set; } = "null";
        public long ChatId {get;set;}
        public int CorrectOptionId {get;set;}

        public JoinRequest(long userId){
            UserId = userId;
        }

        public JoinRequest() { }
    }
}

