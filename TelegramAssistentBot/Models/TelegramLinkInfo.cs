﻿using System;
namespace TelegramAssistentBot.Models{
    public class TelegramLinkInfo{

        public int MessageId { get; set; }
        public ExternalChatId ChatIdOrName { get; set; }

        public TelegramLinkInfo(){}
    }
}

