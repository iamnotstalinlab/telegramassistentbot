﻿//Модуль объекта с комиссией Qiwi-кошелька
//

namespace TelegramAssistentBot {
    public class WithdrawSum {
        public double amount { get; set; }
        public string currency { get; set; }
    }

    public class EnrollmentSum {
        public int amount { get; set; }
        public string currency { get; set; }
    }

    //В этом классе содержится информация о комисии
    public class QwCommission {
        public double amount { get; set; }
        public string currency { get; set; }
    }

    public class FundingSourceCommission {
        public int amount { get; set; }
        public string currency { get; set; }
    }

    public class QiwiCommissonResponse {
        public QiwiCommissonResponse() { }

        public int providerId { get; set; }
        public WithdrawSum withdrawSum { get; set; }
        public EnrollmentSum enrollmentSum { get; set; }
        public QwCommission qwCommission { get; set; }
        public FundingSourceCommission fundingSourceCommission { get; set; }
        public int withdrawToEnrollmentRate { get; set; }
    }


    public class QiwiCommissionRequest{
        public QiwiCommissionRequest() {
            account = string.Empty;
            paymentMethod = new PaymentMethod();
            purchaseTotals = new PurchaseTotals();
        }

        public string account { get; set; }
        public PaymentMethod paymentMethod { get; set; }
        public PurchaseTotals purchaseTotals { get; set; }
    }



}
