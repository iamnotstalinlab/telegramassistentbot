﻿//Модуль объекта с информацией Qiwi-платежа
//

namespace TelegramAssistentBot {

    public class QiwiPaymentInfo {
        public string id { get; set; }  // Уникальный номер платежа
        public string terms { get; set; }
        public Fields fields { get; set; }
        public Sum sum { get; set; }    //Сумма
        public Transaction transaction { get; set; }    //Объект денежного перевода
        public string source { get; set; }
        public string comment { get; set; }
    }

    public class Fields {
        public string account { get; set; }
    }

    public class Sum {
        public int amount { get; set; }
        public string currency { get; set; }
    }

    public class State {
        public string code { get; set; }
    }

    public class Transaction {
        public string id { get; set; }  //Уникальный номер денежного перевода
        public State state { get; set; }
    }

    public class QiwiPayment {
        public string id { get; set; }  //Уникальный номер платежа
        public Sum sum { get; set; }
        public PaymentMethod paymentMethod { get; set; }
        public string comment { get; set; }
        public Fields fields { get; set; }

       public QiwiPayment() {
            sum = new Sum();
            paymentMethod = new PaymentMethod();
            fields = new Fields();
        }
    }

    public class PaymentMethod {
        public string type { get; set; }
        public string accountId { get; set; }

        public PaymentMethod() { }
    }



    public class Total {
        public double amount { get; set; }
        public string currency { get; set; }
    }

    public class PurchaseTotals {
        public Total total { get; set; }

        public PurchaseTotals() {
            total = new Total();
        }
    }

    public class QiwiPay {
        public string account { get; set; }
        public PaymentMethod paymentMethod { get; set; }
        public PurchaseTotals purchaseTotals { get; set; }

        public QiwiPay() {
            paymentMethod = new PaymentMethod();
            purchaseTotals = new PurchaseTotals();
        }
    }


    //Класс объекта с ошибкой Qiwi-платежа
    public class QiwiPaymentError {
        public string code { get; set; }
        public string message { get; set; } //Сообщение об ошибки от серверов Qiwi
    }

}
