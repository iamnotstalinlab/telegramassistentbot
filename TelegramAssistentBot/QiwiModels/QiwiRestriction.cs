﻿//Модуль объекта с ограничениями Qiwi-кошелька
//

namespace TelegramAssistentBot {

    public class QiwiRestriction {
        public string restrictionCode { get; set; }
        public string restrictionDescription { get; set; }  //Описание ограничений
    }
}
