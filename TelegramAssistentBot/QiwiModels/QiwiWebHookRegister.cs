﻿using System;
namespace TelegramAssistentBot.QiwiModels {

    public class QiwiHookParameters {
        public string url { get; set; }
    }
    
    public class QiwiWebHook {
        public string hookId { get; set; }
        public QiwiHookParameters qiwiHookParameters { get; set; }
        public string hookType { get; set; }
        public string txnType { get; set; }
    }
}
