﻿//Модуль начальной панели для быстрого перехода к основным действиям и их исполнению.
//Включая денежный перевод, архив и переход к панели системы.
//

using System;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.AppealsModels;
using Message = Telegram.Bot.Types.Message;

namespace TelegramAssistentBot {
    public class StartPanel :SystemPannel {

        public string BackCallback { get; set; }

        //Создаём экземпляр начальной панели
        
        public StartPanel() :base()  {
            
            OnBaseCreated += BaseSettings_OnBaseCreated;
            OnVotingIsStopped += StartPanel_OnVotingIsStopped;
            CheckAndLoadActiveVoting();
        }


        #region EVENTS

        public EventHandler OnBasePanelCreated;    //Событие при создании основных настроек
        private void BaseSettings_OnBaseCreated(object sender, EventArgs e) {

            OnBasePanelCreated?.Invoke(this, new EventArgs());
            ShowStartPage(idCurrentUser);
        }

        //Выполняем при успешной остановке голосования
        private void StartPanel_OnVotingIsStopped(object sender, VotingEventArgs e) {
            bot.AnswerCallbackQueryAsync(e.CallbackId, "⚠️ Обращение остановлено");
            ShowStartPage(e.StoperId);
        }

        #endregion

        

        //Принимаем и обрабатываем команду
        public void AcceptCommand(Telegram.Bot.Types.Message message) {
            DeleteMessageFromArrestedAdmin(message);

            if (message != null
                && !string.IsNullOrEmpty(message.Text)) {

                if (spammers != null) {
                    if (spammers.ContainsKey(message.From.Id)) {
                        AutoConfiscateMessage(message);
                        return;
                    }
                }

                if (message.Text.Contains("/mark")) {
                    ExecuteGetIdCommand(message);
                    AddAuthor(message.From);
                    MakeFastOrder(message.From.Id, message, "ShowOrderPageCallback");
                }
            }
        }

        //Принимаем и обрабатываем сообщение в чате
        public void AcceptMessageToBaseChat(Telegram.Bot.Types.Message message) {
            if (message == null)
                return;

            if (message.SenderChat != null) {
                BanSenderChat(message);
                return;
            }

            DeleteMessageFromArrestedAdmin(message);
            if (spammers.Count <= 0)
                return;

            if(!(message.Type == Telegram.Bot.Types.Enums.MessageType.LeftChatMember
            || message.Type == Telegram.Bot.Types.Enums.MessageType.NewChatMembers)) {

                if (spammers.ContainsKey(message.From.Id)) {
                    AutoConfiscateMessage(message);
                }
            }
        }

        //Получаем ID указанного пользователя
        PersonInfo pInfo;
        protected void ExecuteGetIdCommand(Message message) {
            if (message.Text == "/mark" || message.Text == $"/mark@{BotProfile.Username}") {

                string answer = string.Empty;
                if (message.ReplyToMessage != null) {

                    pInfo = LoadPersonInfo(message.ReplyToMessage.From.Id);
                    if(pInfo == null) {
                        ApprehendPerson(message.From.Id, message.ReplyToMessage.From.Id, new(message.Chat.Id));
                    } else {
                        if(pInfo.ArrestEndDateTime < DateTime.Now
                            && pInfo.Verification.VK_Id <=0) {
                            ApprehendPerson(message.From.Id, message.ReplyToMessage.From.Id, new(message.Chat.Id));
                        }
                    }

                    if (message.ReplyToMessage.Type == Telegram.Bot.Types.Enums.MessageType.NewChatMembers) {

                        answer += $"\n\n <code>{message.ReplyToMessage.NewChatMembers[0].Id}</code>" +
                            $"\n<i>Гость" +
                            $" <a href=\"tg://user?id={message.ReplyToMessage.NewChatMembers[0].Id}\"" +
                            $">{message.ReplyToMessage.NewChatMembers[0].FirstName.Replace('<','*').Replace('>','*')}</a></i> <b>задержан</b> на 1 минуту." +

                            $"\n\n<i>Участника пригласил {GetCorrectName(message.ReplyToMessage.From.FirstName)}" +
                            $"<a href=\"tg://user?id={message.ReplyToMessage.From.Id}\"" +
                            $">{message.ReplyToMessage.From.Id}</a></i> | " +
                            $"\n<code>{message.ReplyToMessage.From.Id}</code>\n";
                    } else {
                        answer += $"\n\n<a href=\"tg://user?id={message.ReplyToMessage.From.Id}\"" +
                            $">Задержан(а)</a>: {GetCorrectName(message.ReplyToMessage.From.FirstName)} [ <code>{message.ReplyToMessage.From.Id}</code> ]";
                    }
                }

                answer += $"\nВыполнил: <u><a href=\"tg://user?id={(long) bot.BotId}\">БОТ-СЕКРЕТАРЬ</a></u>" +
                    $" [ <code>{(long) bot.BotId}</code> ]" +
                    $"\n\n<i>*Задержание на 1 минуту.</i>";
                SendMessage(message.Chat, answer, null, message.MessageId);
            }
        }


        //Принимаем сообщения
        Object spamLocker = new object();
        public void AcceptMessageToBot(Telegram.Bot.Types.Message message) {


            if(message != null) {
                lock (spamLocker) {
                    if (spammers.ContainsKey(message.From.Id)
                        || joinRequests.ContainsKey(message.From.Id)) {

                        if (joinRequests.ContainsKey(message.From.Id)
                        || spammers[message.From.Id].SpamCount > 1) {
                            CheckAsHumanOrBot(message.From.Id, message.From.FirstName
                            , joinRequests.ContainsKey(message.From.Id) ? joinRequests[message.From.Id].ChatId
                            : BaseChatId);
                        }
                    } else {
                        if (HasAccess(message.From.Id)) {
                            AcceptStartPanelMessage(message);
                        }
                    }
                }
            }
        }

        private void AutoHandledAuth(long userId) {
            string text = $"Здравствуйте. Для удостоверения личности, приглашаю вас получить удостверение(предъявление документов/верификацию) в ВК ID." +
                $"После этого, можно будет провести удостоврение через меня." +
                $"" +
                $"\n\nДля получения достоверности ВКонтакте, можно использовать достоверный профиль в ГосУслугах, Сбер ID, Alfa ID, Tinkoff ID.";

            //InlineKeyboardButton linkButton = InlineKeyboardButton.WithUrl("Войти через VK ID", GetVkontakteIDAuthorizationUri(userId));
            //keyboard = new InlineKeyboardMarkup(new[]{linkButton});
            SendMessage(userId, text, keyboard);
        }




        //Принимаем обратные вызовы
        public void AcceptCallbackToBot(CallbackQuery callback) {
            if (callback != null) {
                if (callback.From.Id == AdministratorId || HasAccess(callback.From.Id)) {
                    AcceptStartPanelCallback(callback, "ShowStartPageCallback");
                }
            }            
        }

        




        //Принимаем основное сообщение в панель
        protected void AcceptStartPanelMessage(Message message) {
            long idUser = message.From.Id;

            if(IsBaseEmpty) {
                AcceptBasePannelMessage(message, "NULL");
            } else {
                AddAuthor(message.From);
                if(!string.IsNullOrEmpty(authors[idUser].CallbackData)) {
                    if(authors[idUser].CallbackData.Contains("Create")) {
                        CreateStartAppeal(idUser, null, message, "ShowStartPageCallback");
                    } else {
                        if(authors[idUser].CallbackData.Contains("Set")) {
                            if(idUser == AdministratorId
                                || (HasAdminRights(idUser) && AllowControl)
                                && voting == null) {

                                AcceptBasePannelMessage(message, "ShowSystemPageCallback");
                            }
                        } else {
                            if(authors[idUser].CallbackData.Contains("Order")) {
                                AcceptOrderCallback(idUser, message, "ShowStartPageCallback");
                            } else {
                                authors[idUser].IdEditMessage = 0;
                                authors[idUser].CallbackData = "ShowStartPageCallback";
                                ShowStartPage(idUser);
                            }
                        }
                    }
                } else {
                    authors[idUser].IdEditMessage = 0;
                    authors[idUser].CallbackData = "ShowStartPageCallback";
                    ShowStartPage(idUser);
                }
            }

        }



        //Создаём обращения (основной метод, используется когда не доступен обратный вызов)
        protected void CreateStartAppeal(long idUser, CallbackQuery callback, Message message, string cancelCallback) {
            if(CanCreateAppeal(callback)) {
                if(authors[idUser].CallbackData.Contains("Remittance")) {
                    CreateRemittanceAppeal(idUser, message, cancelCallback);
                } else {
                    CreateSystemAppeal(idUser, message, cancelCallback);
                }
            }
        }



        //Принимаем обратный вызов основной панели (этот модуль)
        protected void AcceptStartPanelCallback(CallbackQuery callback, string cancelCallback) {

            AddAuthor(callback.From);
            long idUser = callback.From.Id;
            authors[idUser].CallbackData = callback.Data;

            if(IsBaseEmpty) {
                AcceptBasePannelCallback(callback, "NULL");
            } else {
                if(callback.Data == "StopAppealCallback"
                    || callback.Data == "SignAppealCallback") {
                    if(voting != null) {
                        if(callback.Data == "StopAppealCallback") {
                            StopRemittanceAppeal(callback);
                            return;
                        } else {
                            SignAppealByAdmin(callback);
                            ShowStartPage(callback.From.Id);
                            return;
                        }
                    } else {
                        ShowStartPage(callback.From.Id);
                    }
                }

                if(callback.Message.MessageId == authors[idUser].IdEditMessage) {
                    if(callback.Data.Contains("Show")) {
                        ShowPage(callback, cancelCallback);
                    } else {
                        if (callback.Data.Contains("MakeOrder")) {
                            AcceptOrderCallback(idUser, null, cancelCallback);
                        } else {
                            if(callback.Data.Contains("Create")) {
                                if (CanCreateAppeal(callback)) {
                                    CreateStartAppeal(idUser, callback, null, "ShowStartPageCallback");
                                }
                            } else {
                                if (callback.Data.Contains("Send")) {
                                    if (CanCreateAppeal(callback)) {
                                        FormAndSendAppeal(idUser);
                                    }
                                } else {
                                    if (idUser == AdministratorId || (HasAdminRights(idUser) && AllowControl)) {
                                        AcceptBasePannelCallback(callback, "ShowSystemPageCallback");
                                    } else {
                                        try {
                                            bot.AnswerCallbackQueryAsync(callback.Id, "⚠️  Только для основателя и представителей.");
                                        } catch { }
                                        ShowStartPage(idUser);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    ShowStartPage(idUser);
                }
            }
        }



        //Проверяем может ли пользователь создать сейчас обращение
        private bool CanCreateAppeal(CallbackQuery callback) {
            return false;

            if(voting != null) {
                try {
                    bot.AnswerCallbackQueryAsync(callback.Id, "⚠️  Сейчас решается вопрос. Обратитесь позже.");
                } catch { }
                return false;
            } else {
                DateTime now = DateTime.Now;

                if (callback != null) {
                    if((now.TimeOfDay < StartTime.TimeOfDay || now.TimeOfDay > EndTime.TimeOfDay)
                        && (callback.Data.Contains("Create"))){
                        try {
                            bot.AnswerCallbackQueryAsync(callback.Id,$"⚠️  Обращения" +
                                $" принимаются с {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}");
                        } catch { }
                        return false;
                    } else {
                        PersonInfo info = LoadPersonInfo(callback.From.Id);
                        if(info != null &&  info.Verification != null && info.Verification.VK_Id >0)
                            return true;
                        else{
                            bot.AnswerCallbackQueryAsync(callback.Id, "⚠️ Оформите пожалуйста удостоверение для обращений.");
                            return false;
                        }
                    }
                } else {
                    PersonInfo info = LoadPersonInfo(callback.From.Id);
                    if(info != null &&  info.Verification != null && info.Verification.VK_Id >0)
                        return true;
                    else{
                        bot.AnswerCallbackQueryAsync(callback.Id, "⚠️ Оформите пожалуйста удостоверение для обращений.");
                        return false;
                    }
                }
            }
        }


        #region SHOW PAGE
        //Показываем определённую страницу
        private void ShowPage(CallbackQuery callback, string cancelCallback) {
            if(callback.Data == "ShowStartPageCallback") {

                authors[callback.From.Id].IdEditMessage = 0;
                authors[callback.From.Id].CallbackData = null;
                ResetOrder(callback.From.Id);
                ResetStartAppeal(callback.From.Id);
                ShowStartPage(callback.From.Id);
            } else {
                if (callback.Data == "ShowOrderPageCallback") {
                    ShowOrderPages(callback, cancelCallback);
                } else {
                    ShowSystemPages(callback, cancelCallback);
                }
            }
        }

        //Сбрасываем начальное обращение
        protected void ResetStartAppeal(long idUser) {
            ResetRemittanceAppeal(idUser);
        }

        //Показываем страницу обращений
        private async void ShowStartPage(long idUser) {

            string donationUrl = GetDonateUrl(idUser);
            string votingText =string.Empty;


            InlineKeyboardButton topButton =
                InlineKeyboardButton.WithCallbackData("Денежный перевод"
                , "CreateRemittanceAppealCallback"); //Верхняя кнопка начального экрана
            //InlineKeyboardButton topButton = InlineKeyboardButton.WithUrl("Войти через VK ID", GetVkontakteIDAuthorizationUri(idUser));

            InlineKeyboardButton leftKey = InlineKeyboardButton.WithCallbackData("Порядок"
                , "ShowOrderPageCallback"); //Средняя кнопка начального экрана;

            InlineKeyboardButton rightButton =
                InlineKeyboardButton.WithCallbackData("Система"
                , "ShowSystemPageCallback"); //фВерхняя кнопка начального экрана


            votingText = GetVotingText();

            if(voting != null) {                
                int index =0;

                if (voting.Type == "Remittance"
                    || voting.Type =="Common") {

                    if (voting.SignerId == 0) {

                        index = votingText.IndexOf("вопрос</a>") + "вопрос</a>".Length;
                        votingText = votingText.Insert(index, " (Без подписи)");
                    } else {
                        index = votingText.IndexOf("вопрос</a>");
                        index += +"вопрос</a>".Length;
                        votingText = votingText.Insert(index, " (Подписан)");
                    }


                    if (HasAdminRights(idUser) || AdministratorId == idUser){

                        topButton = InlineKeyboardButton.WithCallbackData("Остановить", "StopAppealCallback");
                        leftKey = InlineKeyboardButton.WithCallbackData("Порядок", "ShowOrderPageCallback");

                        if (voting.SignerId == 0){
                            rightButton = InlineKeyboardButton.WithCallbackData("Подписать", "SignAppealCallback");
                        }
                    }
                }
            }
            string notify =string.Empty;
            double tokenDaysLeft = (WalletTokenDate.AddDays(180) - DateTime.Now).TotalDays;
            if(tokenDaysLeft < 14) {
                notify = $"\n<i>*В течение {(int)tokenDaysLeft} дней(я) стоит заменить Qiwi-токен.</i>";
            }

            string text = $"\n\n{votingText}" +
                "\n\n<b>Пояснения:</b>" +
                $"\n🕰  Приём с {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}" +
                $"\n⏳  Голосования по {PollDurationMins} минут(ы/е)" +
                $"\n🪖  Арест {ArrestDurationMins} мин." +
                $"\n💬  <a href=\"{BaseChatLink}\">Основной чат</a>" +
                $"\n🎓  <a href=\"{ArchiveChatLink}\">Архив чата</a>" +
                $"\n\n<a href =\"{donationUrl}\">🎁  Поддержать систему</a>" +
                $"\n<i>Союз ведёт <a href=\"tg://user?id={AdministratorId}\">{AdministratorId}</a> [ <code>{AdministratorId}</code> ]</i>"+
                $"\n<i>ИО ЧС <a href=\"tg://user?id={ReserveAdminId}\">{ReserveAdminId}</a> [ <code>{ReserveAdminId}</code> ]</i>";
            
            text += $"{notify}";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{topButton },
                new []{leftKey, rightButton}
            });

            if (authors[idUser].IdEditMessage != 0) {
                EditMessage(idUser, text);
            } else {
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        #endregion


       
    }
}
