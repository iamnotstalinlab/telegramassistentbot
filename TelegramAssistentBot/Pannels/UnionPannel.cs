﻿//Модуль для главной работы со всеми союзными обращениями включая: арест и восстановление участников.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.Appealsandorders;
using TelegramAssistentBot.Models;
using static System.Net.Mime.MediaTypeNames;
using Message = Telegram.Bot.Types.Message;

namespace TelegramAssistentBot.AppealsModels {
    public class UnionPanel :GroupPannel {

        #region INITIALIZE
        public PersonInfo AdministratorInfo { get; set; }
        public PersonInfo ReserveAdminInfo { get; set; }

        //ОШИБКА В ТЕЛЕГРАМ: Больше 25-и участников ломает текст - падают шрифты и ссылки.
        //Ставим 8, при условии чтобы админов было 10. Чтобы не получилось захвата власти в руки банды где
        //один будет покрывать другого и заблокирует других за один раз
        int maxPersons =0; //Максимальное число лиц которых можно добавить в список за раз
        int maxSpamCount = 4;    //Максимальное число подозрений в спаме до того, чтобы убрали из группы

        Dictionary<long, UnionAppeal> unionAppeals = new Dictionary<long, UnionAppeal>();
        public Dictionary<long, PersonInfo> prisoners;
        public Dictionary<long, PersonInfo> spammers;

        public event EventHandler<ArrestEventArgs> OnArrestingIsExecuted;     //Событие при исполнении ареста
        public event EventHandler<ArrestEventArgs> OnReleasingIsExecuted;
        public event EventHandler<ArrestEventArgs> OnRemovingIsExecuted;
        public class ArrestEventArgs {

            public Message Message { get; set; }

            public long ActiveUserId { get; set; }
            public long PassiveUserId { get; set; }

            public string ReportLink { get; set; }
            public int ReportMessageId { get; set; }
            public string Text { get; set; }

            public bool IsAuto { get; set; }
        }
        
        //Создаём панель объединений
        public UnionPanel() : base() {
            CreateTablePersons(connection);
            CreateTableJoins(connection);

            OnVotingIsLoaded += UnionPanel_OnVotingIsLoaded;
            OnVotingIsStarted += UnionPanel_OnAppealVotingIsStarted;
            OnVotingIsStopped += UnionPanel_OnVotingIsStopped;
            OnVotingIsFinished += UnionPanel_OnVotingIsFinished;

            OnNewUniteAdded += UnionPanel_OnNewUniteAdded;
            OnRebuild += UnionPanel_OnRebuild;
            LoadAdministatorsInfos();

            joinTimer.Elapsed += JoinTimer_Elapsed;
            prisonerTimer.Elapsed += PrisonersTimer_Elapsed;

            permissions = LoadArrestPerimssions();
            PrisonersAndTimerUpdate();
            LoadSpammersGlobally();

            if(verification != null) {
                verification.OnVerificationIsPassed += Verification_OnVerificationIsPassed;
            }
        }

        private async void UnionPanel_OnRebuild(object sender, RebuildEventArgs e){

            if(e.Status == "ResetAdministrator2") {
                string text = "Не получилось.! " +
                $"\n<i>Права ИО ЧС для " +
                $" <a href=\"tg://user?id={ReserveAdminId}\">{ReserveAdminId}</a> </i> [ <code>{ReserveAdminId}</code> ] <b>не</b> выданы." +
                $"\n\n<i>*Возможно оформление удостоверения поможет получить права</i>";

                //Снимаем права с прошлого запасного админа
                if (ReserveAdminInfo !=null && ReserveAdminInfo.Verification != null
                && ReserveAdminInfo.Verification.VK_Id > 0) { 
                    await PromoteAndSetAliasToVerifiedUser(ReserveAdminInfo);
                } else {
                    UnrestructUnverifiedUser(e.PreviousReserveAdminId);
                }

                ReserveAdminInfo = LoadPersonInfo(ReserveAdminId);
                CheckAndPromoteReserveAdmin(ReserveAdminId);

                text = "Выполнено! " +
                    $"\n<i>Права ИО ЧС для " +
                    $" <a href=\"tg://user?id={ReserveAdminId}\">{ReserveAdminId}</a> </i> [ <code>{ReserveAdminId}</code> ] выданы.";

                SendSettingsMessage(AdministratorId, text, null);
            }
        }

        //Загружаем информацию про админов
        private void LoadAdministatorsInfos() {
            AdministratorInfo = LoadPersonInfo(AdministratorId);
            if (ReserveAdminId != 0)
                ReserveAdminInfo = LoadPersonInfo(ReserveAdminId);
        }

        private async void Verification_OnVerificationIsPassed(object sender, VerificationServer.VerificationEventArgs e){
            if(e != null) {
                if(e.Verification == null) {
                        EditMessageToUser(e.TelegramUserId, "Для получения удостоверения, приглашаю вас пройти верификацию VK ID.", null);
                    return;
                }
                Console.WriteLine("Хэш строка в полученном событии = " +e.Verification.HashString);
                if(IsVerifictationReady(e.TelegramUserId, e.Verification)) {
                    PersonInfo personInfo = LoadPersonInfo(e.TelegramUserId);
                    if(personInfo == null) {
                        personInfo = new PersonInfo(e.TelegramUserId, GetUserName(e.TelegramUserId));
                    }

                    personInfo.Verification = e.Verification;
                    personInfo.Verification.Alias = "Достоверный";
                    await PromoteAndSetAliasToVerifiedUser(personInfo);

                    SavePersonsInfo(new List<PersonInfo> { personInfo });
                    EditMessageToUser(e.TelegramUserId, "☀️ УДОСТВЕРЕНИЕ ВЫДАНО!" +
                        "\n\nПоздравляю вас с получением удостоверения на имя: " +
                        $"\n{e.Verification.FirstName}" +
                        $"\n{e.Verification.LastName}" +
                        $"\n{e.Verification.BirthDay}", null);


                    SendMessage(BaseChatId, "☀️ <b>+1</b> УДОСТВЕРЕНИЕ ВЫДАНО!" +
                        $"\n\nПоздравляю <a href=\"tg://user?id={e.TelegramUserId}\">"+
                        $" {GetUserName(e.TelegramUserId)}</a> с получением удостоверения!"+
                        $"\nДостоверных участников уже <b>{GetVerifyedPersonsCount()}</b>."
                        , null);

                    Console.WriteLine("Хэш строка после сохранения в бд = " +e.Verification.HashString);
                }

            }
        }

        private bool IsVerifictationReady(long idUser, VkVerification information) {
            string errorMessage = string.Empty;
            bool isReady = true;

            if (information == null) {
                errorMessage = "Для получения удостоверения, приглашаю вас пройти верификацию VK ID." +
                    "Перед оформлением удостоверения, следует включить подтверждение данных (по рекомендации ВК https://vk.com/faq20572)" +
                    "\n\nКак включить подтверждение данных?" +
                    "\nПерейти к подтверждению можно двумя способами: " +
                    "\n\n1. Управление VK ID → Личные данные (https://id.vk.com/account/#/verifications) → Подтверждение аккаунта. " +
                    "\n2. Управление VK ID → Безопасность и вход  (https://id.vk.com/account/#/security)→ Способы входа. " +
                    "\n\nВ обоих вариантах нужно ввести логин и пароль от Госуслуг, Сбер ID, Tinkoff ID, Alfa ID, указать пароль или номер от аккаунта VK ID и подтвердить, что данные в аккаунте будут изменены на те, что указаны в выбранном стороннем сервисе. " +
                    "\n\nЕсли ранее вы прошли идентификацию VK Pay, нажмите на кнопку Подтвердить аккаунт.";
                isReady = false;
            }

            if(string.IsNullOrEmpty(information.FirstName)
                || string.IsNullOrEmpty(information.LastName)
                || information.Sex == 0
                || string.IsNullOrEmpty(information.BirthDay)) {

                isReady = false;
                int i = 0;
                errorMessage = "☁️ Не получилось." +
                    "\n\nДля оформления удостоверения вам необходимо <a href=\"https://vk.com/faq20572\">подключить сервисы подтверждения VK ID</a>." +
                    " Эти сервисы предоставят ещё:\n";
                if (string.IsNullOrEmpty(information.FirstName)) {
                    ++i;
                    errorMessage += i+ ") Имя\n";
                }
                if (string.IsNullOrEmpty(information.LastName)) {
                    ++i;
                    errorMessage += i+ ") Фамилию\n";
                }
                if (information.Sex == 0) {
                    ++i;
                    errorMessage += i + ") Пол\n";
                }
                if (string.IsNullOrEmpty(information.BirthDay)) {
                    ++i;
                    errorMessage += i + ") Дату рождения\n";
                }
            }

            if (IsExistedVerificationHash(information.HashString)) {
                errorMessage = "Данный профиль уже имеет удостоверение в системе.";
                isReady = false;
            }

            EditMessageToUser(idUser, errorMessage, null);
            return isReady;

        }

        



        protected string GetVerificationUrl(long idUser) {
            return (verification !=null) ? verification.GetVkontakteIDAuthorizationUri(idUser) : null;
        }


        //Получаем ограниечения по аресту
        ChatPermissions permissions = new ChatPermissions(); //Ограничнения по аресту
        protected ChatPermissions LoadArrestPerimssions() {

            bool f = false;
            permissions.CanAddWebPagePreviews = f;
            permissions.CanChangeInfo = f;
            permissions.CanInviteUsers = f;
            permissions.CanManageTopics = f;
            permissions.CanPinMessages = f;
            permissions.CanSendAudios = f;
            permissions.CanSendDocuments = f;
            permissions.CanSendMessages = f;
            permissions.CanSendOtherMessages = f;
            permissions.CanSendPhotos = f;
            permissions.CanSendPolls = f;
            permissions.CanSendVideos = f;
            permissions.CanSendVideoNotes = f;
            permissions.CanSendVoiceNotes = f;
            return permissions;
        }




        #endregion

        #region SHOW AND CREATE UINON APPEAL
        protected void ShowUnionPages(CallbackQuery callback, string cancelCallback) {
            if(callback.Data == "ShowPersonInfoPageCallback"
                || callback.Data == "ShowPublicPageCallback") {

                if(callback.Data == "ShowPublicPageCallback") {
                    ResetUnionAppeal(callback.From.Id);
                    ShowUnionPage(callback.From.Id, cancelCallback);
                } else {
                    authors[callback.From.Id].CallbackData = "GetPersonInfoPageCallback";
                    ResetUnionAppeal(callback.From.Id);
                    ShowPersonPage(callback.From.Id, false, null, cancelCallback);
                }
            } else {
                ShowGroupPages(callback, "ShowPublicPageCallback");
            }
        }


        //Показываем страницу союза
        private void ShowUnionPage(long idUser, string cancelCallback) {

            string donationUrl = GetDonateUrl(idUser);
            
            string text = $"{GetVotingText()}" +
                "\n\n<i>У представителей нет прав на удаление</i>" +
                "\n\n<b>Пояснения:</b>" +
                $"\n🕰  Приём с {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}" +
                $"\n⏳  Голосования по {PollDurationMins} минут(ы/е)" +
                $"\n🪖  Арест {ArrestDurationMins} мин." +
                $"\n\n<a href =\"{donationUrl}\">🎁  Поддержать систему</a>" +
                $"\n<i>Союз ведёт <a href=\"tg://user?id={editor.Id}\">ID {editor.Id}</a></i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                //new []{

                    //InlineKeyboardButton.WithCallbackData("Восстановить", "CreateUnbanAppelCallback"),
                    //InlineKeyboardButton.WithCallbackData("Арестовать", "CreateKickAppelCallback"),

                //},
                new []{
                    InlineKeyboardButton.WithCallbackData("Назад", cancelCallback),
                    InlineKeyboardButton.WithCallbackData("Группа", "ShowAssociationPageCallback")
                }
            });

            if(authors[idUser].IdEditMessage != 0) {
                EditMessage(idUser, text);
            } else {
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Создаём союзное обращение
        protected void CreateUnionAppeal(long idUser, Message message, string cancelCallback) {
            if(IsCreateUnionAppeal(idUser)) {
                PrepareUnionAppeal(idUser);

                if(unionAppeals[idUser].IsPersonsFull == false) {
                    SetPersons(idUser, message, "ShowPublicPageCallback");
                } else {
                    CreateAppeal(idUser, message, unionAppeals[idUser], "ShowPublicPageCallback");
                }
            } else {
                CreateGroupAppeal(idUser, message, cancelCallback);
            }
        }

        //Проверяем действительно ли заявка на создание общественного обращения
        private bool IsCreateUnionAppeal(long idUser) {
            if(authors[idUser].CallbackData == "CreateUnbanAppelCallback"
                || authors[idUser].CallbackData == "CreateKickAppelCallback"
                || authors[idUser].CallbackData == "CreateFullPersonsListCallback") {

                return true;
            } else {
                return false;
            }
        }

        //Подготавливаем союзное обращение 
        private void PrepareUnionAppeal(long idUser) {
            if(!unionAppeals.ContainsKey(idUser)) {
                UnionAppeal un = new UnionAppeal();
                un.AuthorId = authors[idUser].Id;

                unionAppeals.Add(idUser, un);
                SetUnionAppealType(idUser);
            }

            if(authors[idUser].CallbackData == "CreateFullPersonsListCallback") {
                unionAppeals[idUser].IsPersonsFull = true;
            } 
        }

        //Устанавливаем типа союзного обращения
        private void SetUnionAppealType(long idUser) {
            switch(authors[idUser].CallbackData) {

                case "CreateUnbanAppelCallback":
                    unionAppeals[idUser].Type = "Unban";
                    break;

                case "CreateKickAppelCallback":
                    unionAppeals[idUser].Type = "Kick";
                    break;
            }
        }


        //Устанавливаем личности по обращению
        private async void SetPersons(long idUser, Message message, string cancelCallback) {

            bool isList = IsListOfPersonsInAppeal(idUser);
            Dictionary<String, PersonInfo> dictPersonInfo = await GetPersonsFromMessage(message);
            PersonInfo pPersonInfo = dictPersonInfo["PassivePerson"];

            
            if(pPersonInfo !=null) {

                if ((pPersonInfo ==null) || (pPersonInfo != null && pPersonInfo.ArrestEndDateTime < DateTime.Now)) {


                    if (!unionAppeals[idUser].Persons.Contains(pPersonInfo)) {

                        if (string.IsNullOrEmpty(unionAppeals[idUser].Title)) {
                            SetUnionAppealTitleAndServiceInfo(idUser);
                        }

                        AddPersonToAppealList(idUser, pPersonInfo, pPersonInfo);
                        CreateUnionAppeal(idUser, null, cancelCallback);

                    } else {
                        string note = $"<i>{GetUserName(pPersonInfo.Id)}" +
                            $" <a href=\"tg://user?id={pPersonInfo.Id}\">[ID {pPersonInfo.Id}]</a>" +
                            $" уже присутствует в списке.</i>";
                        ShowPersonPage(idUser, isList, note, cancelCallback);
                    }
                } else {
                    string note = $"<i>{GetUserName(pPersonInfo.Id)}" +
                            $" <a href=\"tg://user?id={pPersonInfo.Id}\">[ID {pPersonInfo.Id}]</a>" +
                            $" уже находится под арестом до {pPersonInfo.ArrestEndDateTime.ToString("HH:mm:ss dd/MM/yyyy")}.</i>";
                    ShowPersonPage(idUser, isList, note, cancelCallback);
                }
            } else {
                ShowPersonPage(idUser, isList, null, cancelCallback);
            }
        }

        //Получаем личность из сообщения
        PersonInfo pf;
        protected async Task<Dictionary<String, PersonInfo>> GetPersonsFromMessage(Message message, bool isOrderRequest =false) {

            long personId = 0;
            Dictionary<String, PersonInfo> dictPersons = null;

            if (message != null) {
                if(long.TryParse(message.Text, out personId)) {
                    try {
                        Chat chat = await bot.GetChatAsync(personId);
                        dictPersons = LoadPersonsInfoDictionary(message.From.Id, chat.Id);

                        if (dictPersons != null) {
                            if (!dictPersons.ContainsKey("PassivePerson")) {
                                dictPersons.Add("PassivePerson", new PersonInfo(chat.Id, chat.FirstName));
                            }
                            if (!dictPersons.ContainsKey("ActivePerson")) {
                                dictPersons.Add("ActivePerson", new PersonInfo(message.From.Id, message.From.FirstName));
                            }

                            dictPersons["PassivePerson"].FirstName = chat.FirstName;
                            dictPersons["ActivePerson"].FirstName = message.From.FirstName;
                        } else {
                            dictPersons = new Dictionary<string, PersonInfo>{
                                { "PassivePerson", new PersonInfo(chat.Id, chat.FirstName) },
                                { "ActivePerson", new PersonInfo(message.From.Id, message.From.FirstName) }
                            };

                        }
                    } catch (Exception ex) {

                        //Если это запрос на информацию из базы данных, то проверяем АйДи в любом случае
                        //Иначе - работаем только с обнаруженными пользователями.
                        if (isOrderRequest) {
                            dictPersons = LoadPersonsInfoDictionary(message.From.Id, personId);
                            if (dictPersons == null) {
                                return null;
                            } else {
                                return dictPersons;
                            }
                        } else {
                            return null;
                        }

                    }       
                }
            }
            return dictPersons;
        }

        //Получаем личностей из пользователей
        protected Dictionary<String, PersonInfo> GetPersonsFromUsers(User uActive, User uPassive) {

            Dictionary<String, PersonInfo> dictPersons = LoadPersonsInfoDictionary(uActive.Id, uPassive.Id);

            if (dictPersons == null) {
                dictPersons = new Dictionary<string, PersonInfo>{
                    { "PassivePerson", new PersonInfo(uPassive.Id , uPassive.FirstName) },
                    { "ActivePerson", new PersonInfo(uActive.Id, uActive.FirstName) }
                };
            }

            //Обновляем имя пользователя
            if (!dictPersons.ContainsKey("PassivePerson")) {
                dictPersons.Add("PassivePerson", new PersonInfo(uPassive.Id, uPassive.FirstName));
            }
            if (!dictPersons.ContainsKey("ActivePerson")) {
                dictPersons.Add("ActivePerson", new PersonInfo(uActive.Id, uActive.FirstName));
            }
            
            dictPersons["PassivePerson"].FirstName = uPassive.FirstName;
            dictPersons["ActivePerson"].FirstName = uActive.FirstName;
    
            return dictPersons;
        }




        //Проверяем будет ли перечисление лиц
        private bool IsListOfPersonsInAppeal(long idUser) {
            if(authors[idUser].CallbackData == "CreateKickAppelCallback"
                || authors[idUser].CallbackData == "CreateRevokeAppealCallback") {
                return true;
            } else {
                return false;
            }

        }

        //Добавляем личность в список обращения
        private void AddPersonToAppealList(long idUser, User user, PersonInfo pInfo) {

            if(unionAppeals[idUser].Persons.Count < maxPersons) {

                unionAppeals[idUser].Persons.Add(new PersonInfo(user.Id, user.FirstName));
                if(unionAppeals[idUser].Persons.Count > 1) {
                    unionAppeals[idUser].Title += ",";
                }

                string link = string.Empty;
                if(pInfo != null) {
                    if (!string.IsNullOrEmpty(pInfo.LastSolutionLink)) {
                        link = $" | <a href=\"{pInfo.LastSolutionLink}\">Справка</a>" +
                        $" | <a href=\"{pInfo.ArrestReportLink}\">Аресты</a>";
                    }
                }

                unionAppeals[idUser].Title +=
                    $" {GetUserName(unionAppeals[idUser].Persons[unionAppeals[idUser].Persons.Count - 1].Id)}" +
                    $" <i>[<a href=\"tg://user?id={unionAppeals[idUser].Persons[unionAppeals[idUser].Persons.Count - 1].Id}" +
                    $"\">ID {unionAppeals[idUser].Persons[unionAppeals[idUser].Persons.Count - 1].Id}</a>{link}]</i>";
            } else {

                if(unionAppeals[idUser].Persons.Count > maxPersons) {
                    if(unionAppeals[idUser].Persons.Count > maxPersons) {
                        unionAppeals[idUser].Persons.RemoveRange(maxPersons - 1, unionAppeals[idUser].Persons.Count - maxPersons);
                    }
                }
            }
        }

        //Собираем информационный текст о личности
        protected string FormPersonInfoText(User user) {

            string result = null;
            if(user != null) {

                result = $"{GetUserName(user.Id)} <a href=\"tg://user?id={user.Id}\">[ID {user.Id}]</a>";
                result += ", пока, <b>не отметился</b> в архиве.";
            }
            return result;
        }

        //Собираем информационный текст о личности
        protected string FormPersonInfoText(PersonInfo pInfo) {

            string result = null;
            string link = null;
            if(pInfo != null) {

                result = $"<b><a href=\"tg://user?id={pInfo.Id}\">Профиль</a></b>:" +
                $"\n{GetUserName(pInfo.Id)} [ ID <code>{pInfo.Id}</code> ]";

                if(pInfo.Verification != null) {
                    result += $"\n<b>VK_ID</b>: <code>{pInfo.Verification.VK_Id}</code>";
                }

                if (!string.IsNullOrEmpty(pInfo.ArrestReportLink)) {
                    link = pInfo.ArrestReportLink;

                    if (pInfo.ArrestEndDateTime < DateTime.Now) {
                        result += $"\n<a href=\"{link}\">Освобождён от ареста</a> {pInfo.ArrestEndDateTime.ToString("dd/MM/yyyy")} " +
                            $"в {pInfo.ArrestEndDateTime.ToString("HH:mm:ss")}";
                    } else {
                        result += $"\n<a href=\"{link}\">Будет освобождён от ареста</a> {pInfo.ArrestEndDateTime.ToString("dd/MM/yyyy")} " +
                            $"после {pInfo.ArrestEndDateTime.ToString("HH:mm:ss")}.";
                    }
                }

                if (!string.IsNullOrEmpty(pInfo.RemovingReportLink)) {
                    link = pInfo.RemovingReportLink;
                    result += $"\n<a href=\"{link}\">Отчёт об удалении сообщения.</a>";
                }

                if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                    result += $"\n<i>*Ссылки доступны только участникам <a href=\"{BaseChatLink}\">чата</a>.</i>";
                }
                

            } 
            return result;
        }

        //Устанавливаем заголовок союзного обращения
        private void SetUnionAppealTitleAndServiceInfo(long idUser) {
            switch(authors[idUser].CallbackData) {

                case "CreateUnbanAppelCallback":
                    unionAppeals[idUser].IsPersonsFull = true;
                    unionAppeals[idUser].Title = "<b>Восстановить</b> в системе";
                    unionAppeals[idUser].ServiceInformation = $"<i>Чтобы остранить, создайте обращение через" +
                        $" <a href=\"tg://user?id={bot.BotId}\">помощника</a></i>.";
                    break;

                case "CreateKickAppelCallback":
                    unionAppeals[idUser].Title = $"<b>Арестовать</b> в системе на " +
                        $"<b>{ArrestDurationMins}</b> минут(у/ы) ";
                    unionAppeals[idUser].ServiceInformation = $"<i>Чтобы восстановить, создайте обращение через" +
                        $" <a href=\"tg://user?id={bot.BotId}\">помощника</a></i>.";
                    break;
            }
            unionAppeals[idUser].ServiceInformation += $"\n<i>Голосования по {PollDurationMins} минут(ы/е).</i> ";
        }

        //Показываем страницу определения личностей
        protected void ShowPersonPage(long idUser, bool isList, string note, string cancelCallback) {

            Appeal appeal = unionAppeals[idUser];
            string text = $"<b>ОБРАЩЕНИЕ</b>" +
                $"\nОт: {GetUserName(appeal.AuthorId)}" +
                $" <i><a href=\"tg://user?id={appeal.AuthorId}\">[ID {appeal.AuthorId}]</a></i>" +
                $"\n\n<pre>🎯  Цель обращения:</pre>\n{appeal.Title}";

            if(!string.IsNullOrEmpty(appeal.Title)) {
                text += $"\n\n";
            }

            maxPersons = (int)Math.Ceiling(bot.GetChatAdministratorsAsync(BaseChatId).Result.ToList().Count() / 4.0);
            if (isList && unionAppeals[idUser].Persons.Count >0) {
                text += $"*Перешлите сообщения пользователей или их ID" +
                    $" ({unionAppeals[idUser].Persons.Count}/<b>{maxPersons}</b>)";
            } else {
                text += $"*Перешлите сообщение пользователя, контакт или ID.";
            }

            if(!string.IsNullOrEmpty(note)) {
                text += $"\n\n{note}\n";
            }

            if(isList && unionAppeals[idUser].Persons.Count > 0) {
                keyboard = new InlineKeyboardMarkup(new[] {
                    InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback),
                    InlineKeyboardButton.WithCallbackData("Закончить перечисление", "CreateFullPersonsListCallback")

                });
            } else {
                keyboard = new InlineKeyboardMarkup(new[] {
                    InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
                });
            }

            //Отправляем с ожиданием, чтобы сообщения приходили очередью
            authors[idUser].IdEditMessage =SendMessageWithId(idUser, text, keyboard);  
        }



        #endregion

        #region UNION VOTING
        //Событие при запуске голосования
        private void UnionPanel_OnAppealVotingIsStarted(object sender, EventArgs e) {

            if(unionAppeals.ContainsKey(voting.AuthorId)) {
                long idAuthor = voting.AuthorId;
                SaveVotingUnionInfo(idAuthor, unionAppeals[idAuthor].Type, unionAppeals[idAuthor].Persons);

                if (unionAppeals[idAuthor].Type == "Kick") {
                    // KickPersons(unionAppeals[idAuthor].Persons, false);
                }
                
            }
        }

        //Событие при загрузке данных голосования
        private void UnionPanel_OnVotingIsLoaded(object sender, EventArgs e) {
            LoadVotingInfo(voting.AuthorId);
        }


        //Событие при остановке голосования
        private void UnionPanel_OnVotingIsStopped(object sender, VotingEventArgs e) {
            if(unionAppeals.ContainsKey(e.AuthorId)) {

                StopSolutionText(e.StoperId);
                unionAppeals.Remove(e.AuthorId);
                DeleteMessage(voting.PollMessageId);
                ResetVoting();
            }
        }


        //Событие при завершении голосования
        private void UnionPanel_OnVotingIsFinished(object sender, VotingEventArgs e) {
            if(unionAppeals.ContainsKey(e.AuthorId)) {

                if(e.IsApproved) {
                    UnionAppeal pAppeal = unionAppeals[e.AuthorId];
                    Confirmation conf = AcceptSolutionText(voting.AppealMessageId);

                    SavePersonsInfo(pAppeal.Persons);
                    ExecutePersonSolution(pAppeal.Type, pAppeal.Persons);
                    SignatureConfirmation(conf);
                } else {
                    RejectSolutionText();
                }
                unionAppeals.Remove(e.AuthorId);
                ResetVoting();
            }
        }


        //Выполняем решение по личному вопросу
        private void ExecutePersonSolution(string type, List<PersonInfo> persons) {
            switch(type) {
                case "Kick":
                    // KickPersons(persons);
                    break;
                case "Unban":
                    Unban_Person_In_Union_Chats(persons[0]);
                    break;
            }
        }


        //Восстанавливаем участника
        private void Unban_Person_In_Union_Chats(User person) {
            if (person.Id == ReserveAdminId)
                return;

            for (int i = 0; i < unionChats.Count; ++i)
                try { bot.UnbanChatMemberAsync(unionChats.Keys.ElementAt(i), person.Id); } catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        //Отсраняем участников от системы (Окончательное решение всегда положительное, другое он не вызывает функцию пинка)
        // private void KickPersons(List<PersonInfo> persons, bool isFinished =true) {
        //     DateTime arrestEndDateTime = voting.PollEndTime.AddMinutes(ArrestDurationMins);
        //     if (!isFinished) {
        //         arrestEndDateTime = voting.PollEndTime;
        //     }

        //     try {
        //         for(int i = 0; i < unionChats.Count; ++i) {
        //             for(int j = 0; j < persons.Count; ++j) {
        //                     bot.RestrictChatMemberAsync(unionChats.Keys.ElementAt(i), persons[j].Id, permissions, untilDate: arrestEndDateTime);
        //             }
        //         }
        //     } catch {}

        //     foreach(PersonInfo pInfo in persons) {
        //         if (isFinished) {
        //             pInfo.Status = "Kick";
        //         } else {
        //             pInfo.Status = null;
        //         }
        //         // pInfo.ArrestReportMessageId = voting.AppealMessageId;
        //         // pInfo.LastSolutionMessageId = voting.AppealMessageId;
        //         pInfo.ArrestEndDateTime = arrestEndDateTime;
        //     }

        //     SavePersonsInfo(persons);
        // }



        

        //Задерживаем участника
        protected void ApprehendPerson(long idUser, long idPrisoner, ExternalChatId exChatId) {
            bot.RestrictChatMemberAsync(exChatId, idPrisoner, permissions, untilDate: DateTime.Now.AddMinutes(1));
        }

        //Бессрочный арест для тех кто уже там
        protected void LongArrestPerson(long idPrisoner, ChatId chatId) {
            bot.RestrictChatMemberAsync(chatId, idPrisoner, permissions);
        }


        //Ограничить участника во всех чатах союза
        private async void Restrict_person_in_uion_chats(long passivePersonId, DateTime arrestEndTime) {
            if (passivePersonId == ReserveAdminId || passivePersonId ==AdministratorId)
                return;
            
            for (int i = 0; i < unionChats.Count; ++i) {
                try {
                    if (arrestEndTime > DateTime.Now)
                        await bot.RestrictChatMemberAsync(unionChats.Keys.ElementAt(i), passivePersonId, permissions, untilDate: arrestEndTime);
                } catch (Exception ex){
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.Data);
                    Console.WriteLine(ex.HResult);
                    Console.WriteLine(ex.StackTrace);
                    Console.WriteLine(ex.Source);
                 }
            }
        }



        // Арестовываем участника на выходе даём ID сообщения с отчётом
        protected async Task<Message> ArrestPerson(Order order, string arrestText, bool IsAuto =false, Message report =null) {
            Restrict_person_in_uion_chats(order.PassivePerson.Id, order.ArrestEndTime);
            order.PassivePerson.ArrestEndDateTime = order.ArrestEndTime;
            
            if (order.PassivePerson.SpamCount >= maxSpamCount)
                Unban_Person_In_Union_Chats(order.PassivePerson);


            long chatId = (order.TargetMessage != null) ? order.TargetMessage.Chat.Id : BaseChatId;
            if (report == null)
                report = await SendMessageWithResult(chatId, arrestText
                ,(order.TargetMessage !=null) ? order.TargetMessage.MessageId : 0);
            else
                EditMessage(chatId, report.MessageId, arrestText);

            string link = string.Empty;
            if (report != null) {
                link =GetLinkFromMessage(report.Chat.Id, report.MessageId);
                SetAdministratorsArrestInfo(ref order, report.MessageId);
                order.PassivePerson.ArrestReportLink = GetLinkFromMessage(chatId, report.MessageId);
            }

            SavePersonsInfo(new List<PersonInfo>() {order.PassivePerson});
            PrisonersAndTimerUpdate();
            LoadSpammersGlobally();

            if(report != null) {
                OnArrestingIsExecuted?.Invoke(this, new ArrestEventArgs {
                    Message = report
                    , ActiveUserId = order.ActivePerson.Id
                    , PassiveUserId = order.PassivePerson.Id
                    , ReportLink = link
                    , ReportMessageId = report.MessageId
                    , Text =arrestText
                    , IsAuto =IsAuto
                }) ;
            }
            return report;
        }

        //Устанавливаем информацию об аресте администратора
        private void SetAdministratorsArrestInfo(ref Order order, int reportMessageId) {
            if (AdministratorId != order.PassivePerson.Id && ReserveAdminId != order.PassivePerson.Id)
                return;

            if (AdministratorId == order.PassivePerson.Id) {
                AdministratorInfo.ArrestEndDateTime = order.ArrestEndTime;
                if(order.TargetMessage != null)
                    AdministratorInfo.ArrestReportLink = GetLinkFromMessage(order.TargetMessage.Chat.Id, reportMessageId);
                else 
                    AdministratorInfo.ArrestReportLink = GetLinkFromMessage(BaseChatId, reportMessageId);
                return;
            }

            if (ReserveAdminId == order.PassivePerson.Id) {
                ReserveAdminInfo.ArrestEndDateTime = order.ArrestEndTime;
                if(order.TargetMessage != null)
                    AdministratorInfo.ArrestReportLink = GetLinkFromMessage(order.TargetMessage.Chat.Id, reportMessageId);
                else 
                    AdministratorInfo.ArrestReportLink = GetLinkFromMessage(BaseChatId, reportMessageId);
                return;
            }
        }


        //Освобождаем неудостоверенного участника
        private async void UnrestructUnverifiedUser(long idUser) {
            if (idUser == AdministratorId || idUser == ReserveAdminId)
                return;

            for (int i = 0; i < unionChats.Count; ++i) {
                try {
                    await bot.PromoteChatMemberAsync(unionChats.Keys.ElementAt(i), idUser); //Здесь просто снимаем ограничения, а не ставим админа
                } catch {}
            }
        }

        // Арестовываем участника
        protected async Task<Message> ReleasePerson(Order order, string arrestText, bool IsAuto = false, Message report = null) {

            long chatId = (order.TargetMessage != null) ? order.TargetMessage.Chat.Id : BaseChatId;
            if(order.PassivePerson.Verification !=null
            && order.PassivePerson.Verification.VK_Id > 0) {
                await PromoteAndSetAliasToVerifiedUser(order.PassivePerson);
            } else {
                UnrestructUnverifiedUser(order.PassivePerson.Id);
            }

            CheckAndPromoteReserveAdmin(order.PassivePerson.Id);
            if(!IsAuto){
                if (report == null) {
                    report = await SendMessageWithResult(chatId, arrestText
                    , (order.TargetMessage !=null) ? order.TargetMessage.MessageId :0);
                } else {
                    EditMessage(chatId, report.MessageId, arrestText);
                }
            }

            string link = string.Empty;
            if (report != null) {
                link = GetLinkFromMessage(report.Chat.Id, report.MessageId);
                order.PassivePerson.ArrestReportLink = GetLinkFromMessage(chatId, report.MessageId);

                SetAdministratorsArrestInfo(ref order, report.MessageId);
            }

            order.PassivePerson.ArrestEndDateTime = order.ArrestEndTime;
            order.PassivePerson.SpamCount = 0;

            SavePersonsInfo(new List<PersonInfo>() { order.PassivePerson });
            PrisonersAndTimerUpdate();
            LoadSpammersGlobally();

            if (report != null) {
                OnReleasingIsExecuted?.Invoke(this, new ArrestEventArgs {
                    Message = report
                    , ActiveUserId = order.ActivePerson.Id
                    , PassiveUserId = order.PassivePerson.Id
                    , ReportLink = link
                    , ReportMessageId = report.MessageId
                    , Text = arrestText
                    , IsAuto = IsAuto
                });
            }
            return report;
        }


        //Удаляем сообщение
        protected async Task<Message> RemoveMessage(Order order, string removingText, bool IsAuto = false, Message report = null) {
            
            long chatId = (order.TargetMessage != null) ? order.TargetMessage.Chat.Id : BaseChatId;
            if (report == null)
                report = await SendMessageWithResult(chatId, removingText
                , order.TargetMessage.MessageId);
            else
                EditMessage(chatId, report.MessageId, removingText);

            try {
                bot.DeleteMessageAsync(chatId, order.TargetMessage.MessageId);
            } catch { }

            string link = GetLinkFromMessage(report.Chat.Id, report.MessageId);
            order.ActivePerson.RemovingReportLink = GetLinkFromMessage(chatId, report.MessageId);
            SavePersonsInfo(new List<PersonInfo>() { order.ActivePerson});

            OnRemovingIsExecuted?.Invoke(this, new ArrestEventArgs {
                Message = report
                , ActiveUserId = order.ActivePerson.Id
                , PassiveUserId =order.PassivePerson.Id
                , ReportLink = link
                , ReportMessageId = report.MessageId
                , Text = removingText
                , IsAuto = IsAuto
            });

            return report;
        }

        protected async Task<Message> RemoveMultipleMessage(Order order, string removingText, bool IsAuto = false, Message report = null) {
            if (report == null)
                report = await SendMessageWithResult(order.TargetMessage.Chat.Id, removingText
                , order.TargetMessage.MessageId);
            else
                EditMessage(order.TargetMessage.Chat.Id, report.MessageId, removingText);

            if (order.IdsSelectedMessages != null) 
                try { bot.DeleteMessagesAsync(order.TargetMessage.Chat.Id, order.IdsSelectedMessages); } catch { }


            string link = GetLinkFromMessage(report.Chat.Id, report.MessageId);
            order.ActivePerson.RemovingReportLink =GetLinkFromMessage(order.TargetMessage.Chat.Id, report.MessageId);
            SavePersonsInfo(new List<PersonInfo>() { order.ActivePerson });

            OnRemovingIsExecuted?.Invoke(this, new ArrestEventArgs {
                Message = report
                , ActiveUserId = order.ActivePerson.Id
                , PassiveUserId = order.PassivePerson.Id
                , ReportLink = link
                , ReportMessageId = report.MessageId
                , Text = removingText
                , IsAuto = IsAuto
            });

            return report;
        }



        //Изымаем сообщение с арестом автора
        protected async void ConfiscateMessage(Order order, string text, bool IsAuto =false) {
            Message report = await ArrestPerson(order, text, IsAuto);
            await RemoveMessage(order, text, IsAuto, report);
        }


        //Устанавливаем псевдоним подтверждённому пользователю
        protected async Task<bool> PromoteAndSetAliasToVerifiedUser(PersonInfo person) {
            if (person == null) return false;

            if (person.Verification.VK_Id > 0) {
                if(person.Id != AdministratorId
                && person.Id != ReserveAdminId) {

                    for (int i = 0; i < unionChats.Count; ++i) {
                        try {
                            await bot.PromoteChatMemberAsync(unionChats.ElementAt(i).Value.Identifier, person.Id
                                , isAnonymous: false, canManageChat: true, canPostMessages: false
                                , canEditMessages: false, canDeleteMessages: false, canManageVideoChats: false
                                , canInviteUsers: false, canPromoteMembers: false, canChangeInfo: false
                                , canRestrictMembers: false, canPinMessages: false, canManageTopics: true);
                            await bot.SetChatAdministratorCustomTitleAsync(unionChats.ElementAt(i).Value.Identifier, person.Id, person.Verification.Alias);
                        } catch (Exception ex) {
                            if (ex.Message.Contains("ADMIN_RANK_EMOJI_NOT_ALLOWED"))
                                return false;
                        }
                    }
                }
            }

            SavePersonsInfo(new List<PersonInfo> { person });
            person = LoadPersonInfo(person.Id);
            return true;
        }


        //Проверяем и восстанавливаем запасного админа
        private async void CheckAndPromoteReserveAdmin(long id) {
            if (ReserveAdminId != id)
                return;
            if (ReserveAdminId == AdministratorId)
                return;

            for (int i = 0; i < unionChats.Count; ++i) {
                try {
                    await bot.PromoteChatMemberAsync(BaseChatId, ReserveAdminId, isAnonymous: false, canManageChat: true, canPostMessages: true
                    , canEditMessages: true, canDeleteMessages: true, canManageVideoChats: true, canInviteUsers: true
                    , canPromoteMembers: true, canChangeInfo: true, canRestrictMembers: true, canPinMessages: true, canManageTopics: true);
                } catch { }
                        
            }
        }


        //Создаём авто-заявку
        protected Order CreateAutoOrder(long passiveId, string passiveFirstName, Message message, bool isArrest) {

            Order autoOrder = new Order();
            if (spammers.ContainsKey(passiveId)) {
                autoOrder.PassivePerson = spammers[passiveId];
                autoOrder.PassivePerson.FirstName = passiveFirstName;   //Не взял из message так как иногда сообщение бывает пустым (если из опроса)
            } else {
                autoOrder.PassivePerson = LoadPersonInfo(passiveId);
                if(autoOrder.PassivePerson == null) {
                    autoOrder.PassivePerson = new PersonInfo(passiveId, passiveFirstName);
                }
            }

            if (isArrest) {
                autoOrder.ArrestEndTime = GetArrestEndTime();
            }
            autoOrder.ActivePerson = new PersonInfo((long) bot.BotId);
            autoOrder.ActivePerson.FirstName = bot.GetMyNameAsync().ToString();
            autoOrder.TargetMessage = message;
            autoOrder.MessageArchiveNumber = GetMessageArchiveNumber(autoOrder.TargetMessage);

            return autoOrder;
        }


        //Получаем текущее время окончания ареста
        protected DateTime GetArrestEndTime() {

            if (DateTime.Now.TimeOfDay < StartTime.TimeOfDay
                || DateTime.Now.TimeOfDay > EndTime.TimeOfDay
                || DateTime.Now.AddMinutes(ArrestDurationMins).TimeOfDay > EndTime.TimeOfDay
                || DateTime.Now.AddMinutes(ArrestDurationMins).TimeOfDay < StartTime.TimeOfDay) {

                //Если арест происходит или перекрывается с ночным периодом, то он продливается до утра.
                if (DateTime.Now.TimeOfDay < StartTime.TimeOfDay) {

                    //Если арест просиходит ночью, после полуночи, то до утра текущего дня.
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, StartTime.Hour, StartTime.Minute, StartTime.Second);
                } else {

                    //Если арест просиходит ночью, до полуночи, то до утра следующего дня.
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, StartTime.Hour, StartTime.Minute, StartTime.Second).AddDays(1);
                }
            } else {

                //Если арест происходит днём не перекрывается с ночным периодом, то происходит арест на определённое время.
                return DateTime.Now.AddMinutes(ArrestDurationMins);
            }
        }




        //Арестовываем того, кто сейчас под арестом
        private async void MakeReArrest(PersonInfo prisonerInfo) {
            if (ReserveAdminId == prisonerInfo.Id || AdministratorId == prisonerInfo.Id)
                return;

            for (int i = 0; i < unionChats.Count; ++i) {
                try {
                    await bot.RestrictChatMemberAsync(unionChats.Keys.ElementAt(i), prisonerInfo.Id, permissions
                    , untilDate: (DateTime.Now.AddMinutes(1) <prisonerInfo.ArrestEndDateTime) ? prisonerInfo.ArrestEndDateTime : DateTime.Now.AddMinutes(1));
                } catch {}
            }
        }



        //Удаляем сообщение от арестованного администратора
        protected void DeleteMessageFromArrestedAdmin(Message message) {

            if (AdministratorId != message.From.Id && ReserveAdminId != message.From.Id)
                return;


            if (AdministratorId == message.From.Id && (AdministratorInfo == null || DateTime.Now > AdministratorInfo.ArrestEndDateTime))
                return;

            if(ReserveAdminId ==message.From.Id && (ReserveAdminInfo == null || DateTime.Now > ReserveAdminInfo.ArrestEndDateTime))
                return;
                    
            DeleteMessage(BaseChatId, message.MessageId);
            string text = $"Сообщение {GetLinkFromMessage(message.Chat.Id, message.MessageId)} удалено." +
                $"\n<code>{GetMessageArchiveNumber(message)}</code> [<a href=\"{ArchiveChatLink}\">Архив</a>]" +
                $"\n\n*{GetCorrectName(message.From.FirstName)} " +
                $"[ <code>{message.From.Id}</code> ] арестован <a href=\"" +
                $"{(AdministratorId ==message.From.Id ? AdministratorInfo : ReserveAdminInfo).ArrestReportLink} \"" +
                $">до {(AdministratorId ==message.From.Id ? AdministratorInfo : ReserveAdminInfo).ArrestEndDateTime.ToString("HH:mm:ss dd/MM/yyyy")}</a>.";

            SendMessage(BaseChatId, text, null);
        }

        

        protected string BuildPersonsReportLinks(PersonInfo person, bool arrestInfo, bool removingInfo, bool solutionInfo) {
            string links = null;
            if (solutionInfo
                && !string.IsNullOrEmpty(person.LastSolutionLink)) {
                links += $" | <a href=\"{person.LastSolutionLink}\">Справка</a>";
            }
            if (arrestInfo 
                && !string.IsNullOrEmpty(person.ArrestReportLink)) {
                links += $" | <a href=\"{person.ArrestReportLink}\">Арест</a>";
            };

            if (removingInfo
                && !string.IsNullOrEmpty(person.RemovingReportLink)) {
                links += $" | <a href=\"{person.RemovingReportLink}\">Удаление</a>";
            }

            if (person.SpamCount > 0) {
                links += $" {person.SpamCount}/{maxSpamCount}";
            }

            return links;
        }


        //Блокируем анонимных (чаты и каналы) авторов
        protected void BanSenderChat(Message message) {
            try {
                bot.BanChatSenderChatAsync(BaseChatId, message.SenderChat.Id);
            } catch { };

            string text = $"\n\n<pre>{message.SenderChat.Id}</pre>" +
                $"\nЧат или канал <i>{message.SenderChat.Title}</i> <b>исключён</b>." +
                $"\n<i>В пользу личного общения между участниками.</i>\n";
            SendMessage(message.Chat, text, null, message.MessageId);
        }

        

        


        

       


        Timer prisonerTimer = new Timer();
        private void PrisonersTimer_Elapsed(object sender, ElapsedEventArgs e) {
            PrisonersAndTimerUpdate();
        }


        //Обновление общего списка арестованных и таймера
        double totalMseconds;
        private async void PrisonersAndTimerUpdate() {
            prisonerTimer.Stop();

            if (prisoners != null) {
                if(prisoners.Count > 0) {
                    PersonInfo personInfo = prisoners.Values.MinBy(a => a.ArrestEndDateTime);

                    while (prisoners.Count >0
                        && (prisoners[personInfo.Id].ArrestEndDateTime -DateTime.Now).TotalMilliseconds < 0) {

                        await PromoteAndSetAliasToVerifiedUser(personInfo);
                        prisoners.Remove(personInfo.Id);
                        if(prisoners.Count > 0) personInfo = prisoners.Values.MinBy(a => a.ArrestEndDateTime);
                    }
                }
            }

            LoadPrisonersGlobally();
            if(prisoners != null
                && prisoners.Count >0) {

                prisonerTimer.Interval = (prisoners.Values.Min(a => a.ArrestEndDateTime) - DateTime.Now).TotalMilliseconds;
                prisonerTimer.Start();
            }
        }


        public void AcceptChatJoinRequest(ChatJoinRequest cjR) {
            CheckAsHumanOrBot(cjR.From.Id, cjR.From.FirstName, cjR.Chat.Id);
        }




        Timer joinTimer = new Timer();
        protected Dictionary<long, JoinRequest> joinRequests = new Dictionary<long, JoinRequest>();

        private void JoinTimer_Elapsed(object sender, ElapsedEventArgs e) {
            joinTimer.Stop();
            if (joinRequests.Count() > 0) {

                long id = joinRequests.Values.MinBy(a => a.JoinEndDateTime).UserId;
                double interval = (joinRequests[id].JoinEndDateTime - DateTime.Now).TotalMilliseconds;

                while (interval < 0 && joinRequests.Count >0) {
                    try {
                        bot.ApproveChatJoinRequest(BaseChatId, joinRequests[id].UserId);
                    } catch { }

                    Order autoOrder = CreateAutoOrder(joinRequests[id].UserId, joinRequests[id].FirstName, null, false);
                    if(autoOrder.PassivePerson.SpamCount == 0) {
                        autoOrder.PassivePerson.SpamCount = 1;
                    }

                    ArrestPerson(autoOrder, null, true, null);
                    joinRequests.Remove(id);

                    if(joinRequests.Count > 0) {
                        id = joinRequests.Values.MinBy(a => a.JoinEndDateTime).UserId;
                        interval = (joinRequests[id].JoinEndDateTime - DateTime.Now).TotalMilliseconds;
                    }
                }

                if(interval > 0) {
                    joinTimer.Interval = interval;
                    joinTimer.Start();
                }
            }
        }


        Random random = new Random();
        int pollDurationSec = 300;
        protected void CheckAsHumanOrBot(long idUser, string firstName, long chatId) {

            if (!prisoners.ContainsKey(idUser)
                || prisoners[idUser].SpamCount>1) {

                JoinRequest join = new JoinRequest();
                join.UserId = idUser;
                join.FirstName = firstName;
                join.ChatId =chatId;
                join.JoinEndDateTime = DateTime.Now.AddSeconds(pollDurationSec);


                List<InputPollOption> daysNames = new List<InputPollOption>();
                join.CorrectOptionId = random.Next(3);
                for (int i = 0; i < 3; i++) {
                    daysNames.Add(CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat.GetDayName(join.JoinEndDateTime.AddDays(-1).AddDays(i).DayOfWeek).ToUpper());
                }
                string buff = daysNames[join.CorrectOptionId].Text;
                daysNames[join.CorrectOptionId] = daysNames[1];
                daysNames[1] = buff;
                
                string chatTitle = (BaseChatId == chatId) ? GetBaseChatTitle() : bot.GetChatAsync(chatId).Result.Title; 
                bot.SendPollAsync(idUser
                    , question: $"Здравствуйте, я хочу дать доступ в \"{chatTitle}\". \n\nДля этого просто выберите сегодняшний день недели:"
                    , options: daysNames
                    , messageThreadId: null
                    , isAnonymous: false
                    , type: PollType.Quiz
                    , correctOptionId: join.CorrectOptionId
                    , openPeriod: pollDurationSec);

                daysNames.Clear();

                if (joinRequests.ContainsKey(idUser)) {
                    joinRequests[idUser] = join;
                } else {
                    joinRequests.Add(idUser, join);
                }

                joinTimer.Interval = (join.JoinEndDateTime.AddSeconds(5) -DateTime.Now).TotalMilliseconds;
                joinTimer.Start();
            } else {

                SendMessage(idUser, $"Приглашаю вас вступить в \"{BaseChatTittle}\", после освобождения от ареста.", null); ;
            }
        }



        //Примнимаем или отказываем участнику
        public async void ApproveOrNotToBaseChat(PollAnswer pollAnswer) {

            if (pollAnswer == null)
            return;

            if (prisoners == null)
                LoadPrisonersGlobally();

            if (prisoners.ContainsKey(pollAnswer.User.Id))
                MakeReArrest(prisoners[pollAnswer.User.Id]);

            //Подтверждаем вступление
            string wellcome = null;
            string text = string.Empty;

            if(!joinRequests.ContainsKey(pollAnswer.User.Id))
                return;
            
            long userId =pollAnswer.User.Id; 

            if (pollAnswer.OptionIds[0] == joinRequests[userId].CorrectOptionId) {
                try {
                    await bot.ApproveChatJoinRequest(joinRequests[userId].ChatId, userId);
                } catch{ }
                
                joinRequests.Remove(userId);
                
                wellcome = $"Верно! Добро пожаловать в {BaseChatUsername}";
                if (spammers.ContainsKey(userId)) {
                    if (spammers[userId].SpamCount > 1) {       //Если изъятие было
                        Order autoOrder = CreateAutoOrder(userId, pollAnswer.User.FirstName, null, false);
                        await ReleasePerson(autoOrder, text, true, null);
                    } else {
                        spammers[userId].SpamCount =0;
                        SavePersonsInfo(new List<PersonInfo>() { spammers[userId] });
                        LoadSpammersGlobally();
                    }
                }
                
                await PromoteAndSetAliasToVerifiedUser(LoadPersonInfo(userId));
                CheckAndPromoteReserveAdmin(userId);
                SendMessage(userId, $"Верно! Подозрения сняты.", null);
            } else {

                //Отклоняем вступление
                //bot.DeclineChatJoinRequest(BaseChatId, pollAnswer.User.Id);
                joinRequests.Remove(userId);   //Удаляем по одной пременной индекса, чтобы не занимать лишней памяти

                if (spammers.ContainsKey(userId)) {
                    spammers[userId].SpamCount ++;
                    SavePersonsInfo(new List<PersonInfo>() { spammers[userId] });
                } else {
                    spammers.Add(userId, new PersonInfo(userId, pollAnswer.User.FirstName));
                }

                SendMessage(userId
                    , $"К сожалению, вы ошиблись. Давайте, позже, попоробуем снова подать заявку в чат.", null);
            }
        }

        string description;
        protected void DeleteMessageAsSpam(Message message, PersonInfo spammer) {
            if(message != null) {
                DeleteMessage(message.MessageId);

                if(spammer != null) {
                    if ( !string.IsNullOrEmpty(spammer.ArrestReportLink)) {
                        description = $" | <a href=\"{spammer.LastSolutionLink}\">Справка</a>" +
                            $" | <a href=\"{spammer.ArrestReportLink}\">Арест</a>";
                    };
                }

                string text = $"<code>{GetMessageArchiveNumber(message)}</code> [ <a href=\"{ArchiveChatLink}\">Архив</a> ]" +
                    $"\n\nСообщение N{message.MessageId} | Тема N{message.MessageThreadId} удалено." +
                    $"\n{GetCorrectName(message.From.FirstName)} [ ID <code>{message.From.Id}</code>{description} ]" +
                    $" <b>под {spammer.SpamCount}м подозрением</b> в авто-рассылке.";

                description = string.Empty;
                spammer.SpamCount++;
                SavePersonsInfo(new List<PersonInfo>() { spammer });
                SendMessage(BaseChatId, text, null);
            }
        }



        //Событие при добавлении нового объединения
        private void UnionPanel_OnNewUniteAdded(object sender, GroupEventArgs e) {
            Confirmation conf = AcceptSolutionText(e.AppealMessageId);
            SignatureConfirmation(conf);
        }

        


        //Останавливаем союзное обращение
        protected void StopUnionAppeal(CallbackQuery callback) {
            if(voting != null) {
                if(unionAppeals.ContainsKey(voting.AuthorId)) {

                    ChatMember stopper = bot.GetChatMemberAsync(callback.From.Id, callback.From.Id).Result;
                    User person = unionAppeals[voting.AuthorId].Persons.Find(item => item.Id == callback.From.Id);

                    if(person != null) {
                        if((unionAppeals[voting.AuthorId].Type == "Kick" || unionAppeals[voting.AuthorId].Type == "Revoke")) {
                            bot.AnswerCallbackQueryAsync(callback.Id, "⚠️  Вы не можете остановить это обращение.");
                            return;
                        }
                    }
                }
                StopGropAppeal(callback);
            }
        }

        //Сбрасываем союзное обращение
        protected void ResetUnionAppeal(long idUser) {
            if((voting == null) || (voting.AuthorId != idUser)) {

                if(unionAppeals.ContainsKey(idUser)) {

                    unionAppeals.Remove(idUser);
                    authors[idUser].IdEditMessage = 0;
                    ResetAppeal(idUser);

                } else {
                    ResetGroupAppeal(idUser);
                }
            }
        }


        #endregion

        #region DATA BASE

        //Создаём таблицу личностей
        private void CreateTablePersons(SqliteConnection cnct) {

            OpenDatabase();
            string sCommand = @"CREATE TABLE IF NOT EXISTS Persons (
                PersonId   TEXT NOT NULL UNIQUE,
                FirstName   TEXT NOT NULL,
                Status     TEXT,
                LastSolutionLink   TEXT,
                ArrestReportLink TEXT,
                ArrestEndDateTime TEXT,
                ArrestEndDateTimeNumeric INTEGER,
                RemovingReportLink TEXT,
                SpamCount INTEGER NOT NULL DEFAULT 0,

                VerificationVK_Id INTEGER NOT NULL DEFAULT 0,
                VerificationFirstName TEXT NULL,
                VerificationLastName TEXT NULL,
                VerificationSex INTEGER DEFAULT 0,
                VerificationBirhtDay TEXT NULL,
                VerificationAlias TEXT NULL,
                VerificationHash TEXT NULL);";

            SqliteCommand command = new SqliteCommand();

            command.Connection = cnct;
            command.CommandText = sCommand;
            command.ExecuteNonQuery();

            CloseDatabase();
        }

        //Создаём таблицу запросов на вступление
        private void CreateTableJoins(SqliteConnection cnct) {

            OpenDatabase();
            string sCommand = @"CREATE TABLE IF NOT EXISTS JoinRequests (
                PersonId   TEXT NOT NULL UNIQUE,
                FirstName   TEXT NOT NULL,
                JoinEndDateTime TEXT,
                JoinEndDateTimeNumeric INTEGER);";

            SqliteCommand command = new SqliteCommand();

            command.Connection = cnct;
            command.CommandText = sCommand;
            command.ExecuteNonQuery();

            CloseDatabase();
        }



        //Проверяем имеет ли доступ данное лицо
        protected bool HasAccess(long idPerson) {

            bool hasAccess = true;
            DateTime arrestEndTime = default;

            if (idPerson == AdministratorId
                && AdministratorInfo !=null) {
                    arrestEndTime = AdministratorInfo.ArrestEndDateTime;
            } else {
                lock(dbLocker) {
                    OpenDatabase();
                    string sCommand = "SELECT * FROM Persons WHERE PersonId =@personId";

                    SqliteCommand command = new SqliteCommand();
                    command.CommandText = sCommand;
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@personId", idPerson);

                    dataReader = command.ExecuteReader();
                    if(dataReader.HasRows) {
                        while (dataReader.Read()) {
                            arrestEndTime = DateTime.Parse(dataReader["ArrestEndDateTime"].ToString());
                        }
                    }
                    dataReader.Close();
                    CloseDatabase();
                 
                }
            }

            if (arrestEndTime < DateTime.Now) {
                hasAccess = true;
            } else {
                hasAccess = false;
            }
            return hasAccess;
        }

        private int GetVerifyedPersonsCount(){
            int count =0;
            lock(dbLocker) {
                OpenDatabase();
                string sCommand = "SELECT * FROM Persons WHERE VerificationVK_Id >0";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while (dataReader.Read()) {
                        ++count;
                    }
                }
                dataReader.Close();
                CloseDatabase();
                
            }
            return count;
        }


        //Сохраняем отчёт о голосовании
        private void SaveVotingUnionInfo(long authorId, string type, List<PersonInfo> persons) {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Votings SET Type =@type" +
                    ", SetPersonsJson =@setPersonsJson WHERE AuthorId =@authorId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@authorId", authorId);
                command.Parameters.AddWithValue("@type", type);

                string setPersonsJson = JsonConvert.SerializeObject(persons);
                command.Parameters.AddWithValue("@setPersonsJson", setPersonsJson);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Загружаем данные по голосованию из базы
        private void LoadVotingInfo(long authorId) {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Votings WHERE AuthorId =@authorId" +
                    " AND (Type ='Unban' OR Type ='Kick' OR Type ='Promote' OR Type ='Revoke')";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@authorId", authorId);

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {

                        UnionAppeal persAppeal = new UnionAppeal();
                        persAppeal.AuthorId = long.Parse(dataReader["AuthorId"].ToString());
                        persAppeal.Type = dataReader["Type"].ToString();

                        persAppeal.Persons = JsonConvert.DeserializeObject<List<PersonInfo>>(dataReader["SetPersonsJson"].ToString());
                        
                        if(!unionAppeals.ContainsKey(persAppeal.AuthorId)) {
                            unionAppeals.Add(persAppeal.AuthorId, persAppeal);
                        }
                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        

        //Сохраняем резальтаты обращения
        private void SavePersonsInfo(List<PersonInfo> persons) {

            SqliteCommand command = new SqliteCommand();
            lock (dbLocker) {
                OpenDatabase();
                string sCommand = string.Empty;

                for (int i = 0; i < persons.Count; ++i) {

                    //Внимание! Текстовые данные SQLite в строке принимает только в одинарных кавычках (Пример: 'Дом')
                    sCommand += "INSERT INTO Persons (PersonId, FirstName, Status, LastSolutionLink, ArrestReportLink, ArrestEndDateTime, " +
                        $"ArrestEndDateTimeNumeric, RemovingReportLink, SpamCount" +
                        $", VerificationVK_Id, VerificationFirstName, VerificationLastName, VerificationSex, VerificationBirhtDay, VerificationAlias, VerificationHash" +
                        $") VALUES (" +
                        $"{persons[i].Id}, @firstName{i}, @status{i}, @lastSolutionLink{i}, @arrestReportLink{i}, @arrestEndDateTime{i}" +
                        $", @arrestEndDateTimeNumeric{i}, @removingReportLink{i}, @spamCount{i}" +
                        $", @verificationVK_Id{i}, @verificationFirstName{i}, @verificationLastName{i}, @verificationSex{i}, @verificationBirhtDay{i}" +
                        $",  @verificationAlias{i}, @verificationHash{i}" +
                        $") ON CONFLICT (PersonId) DO UPDATE SET PersonId ={persons[i].Id}" +
                        $", FirstName =@firstName{i}, Status =@status{i}, LastSolutionLink =@lastSolutionLink{i}" +
                        $", ArrestReportLink =@arrestReportLink{i}, ArrestEndDateTime =@arrestEndDateTime{i}" +
                        $", ArrestEndDateTimeNumeric =@arrestEndDateTimeNumeric{i}"+
                        $", RemovingReportLink =@removingReportLink{i}, SpamCount =@spamCount{i}" +
                        $", VerificationVK_Id =@verificationVK_Id{i}, VerificationFirstName =@verificationFirstName{i}" +
                        $", VerificationLastName =@verificationLastName{i}, VerificationSex =@verificationSex{i}, VerificationBirhtDay =@verificationBirhtDay{i}" +
                        $", VerificationAlias =@verificationAlias{i}, VerificationHash =@verificationHash{i};";

                    //Ставим тут параметр с первым именем, потому что там могут быть системные символы, которые SQL принимает как команду (прим: ' или ;)
                    command.Parameters.AddWithValue($"@firstName{i}", string.IsNullOrEmpty(persons[i].FirstName) ? DBNull.Value : persons[i].FirstName) ;
                    command.Parameters.AddWithValue($"@status{i}", string.IsNullOrEmpty(persons[i].Status) ? DBNull.Value : persons[i].Status);
                    command.Parameters.AddWithValue($"@lastSolutionLink{i}"
                    , string.IsNullOrEmpty(persons[i].LastSolutionLink) ? DBNull.Value : persons[i].LastSolutionLink);
                    command.Parameters.AddWithValue($"@arrestReportLink{i}"
                    , string.IsNullOrEmpty(persons[i].ArrestReportLink) ? DBNull.Value : persons[i].ArrestReportLink);
                    command.Parameters.AddWithValue($"@arrestEndDateTime{i}", persons[i].ArrestEndDateTime);
                    command.Parameters.AddWithValue($"@arrestEndDateTimeNumeric{i}", persons[i].ArrestEndDateTime.ToString("yyyyMMddHHmmss"));
                    command.Parameters.AddWithValue($"@removingReportLink{i}"
                    , string.IsNullOrEmpty(persons[i].RemovingReportLink) ? DBNull.Value : persons[i].RemovingReportLink);
                    command.Parameters.AddWithValue($"@spamCount{i}", persons[i].SpamCount);

                    //Остановился тут пишу базу для удостоверений
                    command.Parameters.AddWithValue($"@verificationVK_Id{i}", persons[i].Verification.VK_Id);
                    command.Parameters.AddWithValue($"@verificationFirstName{i}", string.IsNullOrEmpty(persons[i].Verification.FirstName)
                        ? DBNull.Value : persons[i].Verification.FirstName);
                    command.Parameters.AddWithValue($"@verificationLastName{i}", string.IsNullOrEmpty(persons[i].Verification.LastName)
                        ? DBNull.Value : persons[i].Verification.LastName);
                    command.Parameters.AddWithValue($"@verificationSex{i}", persons[i].Verification.Sex);
                    command.Parameters.AddWithValue($"@verificationBirhtDay{i}", (string.IsNullOrEmpty(persons[i].Verification.BirthDay))
                        ? DBNull.Value : persons[i].Verification.BirthDay);
                    command.Parameters.AddWithValue($"@verificationAlias{i}", string.IsNullOrEmpty(persons[i].Verification.Alias)
                        ? DBNull.Value : persons[i].Verification.Alias);
                    command.Parameters.AddWithValue($"@verificationHash{i}", string.IsNullOrEmpty(persons[i].Verification.HashString)
                        ? DBNull.Value : persons[i].Verification.HashString);

                }
                //new SqliteCommand();

                command.Connection = connection;
                command.CommandText = sCommand;

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Загружаем информацию о личности из базы данных
        //Входной pInfo для того, чтобы можно было изменять входящий параметр
        protected PersonInfo LoadPersonInfo(long idPerson1, PersonInfo pInfo =null) {
            lock(dbLocker) {
                OpenDatabase();
                string sCommand = "SELECT * FROM Persons WHERE PersonId =@personId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@personId", idPerson1);

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {

                    pInfo = new PersonInfo(idPerson1);
                    while(dataReader.Read()) {

                        pInfo.FirstName = dataReader["FirstName"].ToString();
                        pInfo.Status = dataReader["Status"].ToString();
                        pInfo.LastSolutionLink = dataReader["LastSolutionLink"].ToString();
                        pInfo.ArrestReportLink = dataReader["ArrestReportLink"].ToString();
                        pInfo.ArrestEndDateTime = DateTime.Parse(dataReader["ArrestEndDateTime"].ToString());
                        pInfo.ArrestEndDateTimeNumeric = long.Parse(dataReader["ArrestEndDateTimeNumeric"].ToString());
                        pInfo.RemovingReportLink = dataReader["RemovingReportLink"].ToString();
                        pInfo.SpamCount = int.Parse(dataReader["SpamCount"].ToString());

                        pInfo.Verification.VK_Id = int.Parse(dataReader["VerificationVK_Id"].ToString());
                        pInfo.Verification.FirstName = dataReader["VerificationFirstName"].ToString();
                        pInfo.Verification.LastName = dataReader["VerificationLastName"].ToString();
                        pInfo.Verification.Sex = int.Parse(dataReader["VerificationSex"].ToString());
                        pInfo.Verification.BirthDay = dataReader["VerificationBirhtDay"].ToString();
                        pInfo.Verification.HashString = dataReader["VerificationHash"].ToString();
                        pInfo.Verification.Alias = dataReader["VerificationAlias"].ToString();
                    }
                }
                dataReader.Close();
                CloseDatabase();
                return pInfo;
            }
        }

        protected Dictionary<String, PersonInfo> LoadPersonsInfoDictionary(long activePersonId, long passivePersonId) {
            lock (dbLocker) {
                OpenDatabase();
                string sCommand = "SELECT * FROM Persons WHERE PersonId =@personId1 OR PersonId =@personId2";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@personId1", activePersonId);
                command.Parameters.AddWithValue("@personId2", passivePersonId);

                dataReader = command.ExecuteReader();

                Dictionary<String, PersonInfo> dictPersons = null;
                if (dataReader.HasRows) {
                    dictPersons = new Dictionary<string, PersonInfo>();
                    string personRole = string.Empty;

                    while (dataReader.Read()) {

                        if(long.Parse(dataReader["PersonId"].ToString()) == activePersonId){
                            personRole = "ActivePerson";
                            dictPersons.Add("ActivePerson", new PersonInfo(activePersonId));
                        } else {
                            personRole = "PassivePerson";
                            dictPersons.Add("PassivePerson", new PersonInfo(passivePersonId));
                        }

                        dictPersons[personRole].Status = dataReader["FirstName"].ToString();
                        dictPersons[personRole].Status = dataReader["Status"].ToString();
                        dictPersons[personRole].LastSolutionLink = dataReader["LastSolutionLink"].ToString();
                        dictPersons[personRole].ArrestReportLink = dataReader["ArrestReportLink"].ToString();
                        dictPersons[personRole].ArrestEndDateTime = DateTime.Parse(dataReader["ArrestEndDateTime"].ToString());
                        dictPersons[personRole].ArrestEndDateTimeNumeric = long.Parse(dataReader["ArrestEndDateTimeNumeric"].ToString());
                        dictPersons[personRole].RemovingReportLink = dataReader["RemovingReportLink"].ToString();
                        dictPersons[personRole].SpamCount = int.Parse(dataReader["SpamCount"].ToString());

                        dictPersons[personRole].Verification.VK_Id = int.Parse(dataReader["VerificationVK_Id"].ToString());
                        dictPersons[personRole].Verification.FirstName = dataReader["VerificationFirstName"].ToString();
                        dictPersons[personRole].Verification.LastName = dataReader["VerificationLastName"].ToString();
                        dictPersons[personRole].Verification.Sex = int.Parse(dataReader["VerificationSex"].ToString());
                        dictPersons[personRole].Verification.BirthDay = dataReader["VerificationBirhtDay"].ToString();
                        dictPersons[personRole].Verification.HashString = dataReader["VerificationHash"].ToString();
                        dictPersons[personRole].Verification.Alias = dataReader["VerificationAlias"].ToString();

                    }
                }
                dataReader.Close();
                CloseDatabase();

                if(activePersonId == passivePersonId
                    &&dictPersons !=null && dictPersons["ActivePerson"] !=null) {
                    dictPersons["PassivePerson"] = dictPersons["ActivePerson"];
                }
                return dictPersons;
            }
        }

        protected void LoadPrisonersGlobally() {

            lock (dbLocker) {
                OpenDatabase();
                string sCommand = "SELECT * FROM Persons WHERE ArrestEndDateTimeNumeric >@dateTimeNowNumeric";
            
                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                //string dateTimeNow = DateTime.Now.ToString();
                command.Parameters.AddWithValue("@dateTimeNowNumeric", DateTime.Now.ToString("yyyyMMddHHmmss"));

                
                if(prisoners == null) {
                    prisoners =new Dictionary<long, PersonInfo>();
                } else {
                    prisoners.Clear();
                }

                dataReader = command.ExecuteReader();
                if (dataReader.HasRows) {
                    while (dataReader.Read()) {

                        long id = long.Parse(dataReader["PersonId"].ToString());
                        prisoners.Add(id, new PersonInfo(id));
                        prisoners[id].FirstName = dataReader["FirstName"].ToString();
                        prisoners[id].Status = dataReader["Status"].ToString();
                        prisoners[id].LastSolutionLink = dataReader["LastSolutionLink"].ToString();
                        prisoners[id].ArrestReportLink = dataReader["ArrestReportLink"].ToString();
                        prisoners[id].ArrestEndDateTime = DateTime.Parse(dataReader["ArrestEndDateTime"].ToString());
                        prisoners[id].ArrestEndDateTimeNumeric = long.Parse(dataReader["ArrestEndDateTimeNumeric"].ToString());
                        prisoners[id].RemovingReportLink = dataReader["RemovingReportLink"].ToString();
                        prisoners[id].SpamCount = int.Parse(dataReader["SpamCount"].ToString());

                        prisoners[id].Verification.VK_Id = int.Parse(dataReader["VerificationVK_Id"].ToString());
                        prisoners[id].Verification.FirstName = dataReader["VerificationFirstName"].ToString();
                        prisoners[id].Verification.LastName = dataReader["VerificationLastName"].ToString();
                        prisoners[id].Verification.Sex = int.Parse(dataReader["VerificationSex"].ToString());
                        prisoners[id].Verification.BirthDay = dataReader["VerificationBirhtDay"].ToString();
                        prisoners[id].Verification.HashString = dataReader["VerificationHash"].ToString();
                        prisoners[id].Verification.Alias = dataReader["VerificationAlias"].ToString();

                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        protected void LoadSpammersGlobally() {

            lock (dbLocker) {
                OpenDatabase();
                string sCommand = "SELECT * FROM Persons WHERE SpamCount >0";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();

                if(spammers == null) {
                    spammers = new Dictionary<long, PersonInfo>();
                } else {
                    spammers.Clear();
                }

                if (dataReader.HasRows) {

                    long id = 0;
                    while (dataReader.Read()) {

                        id = long.Parse(dataReader["PersonId"].ToString());
                        spammers.Add(id, new PersonInfo(long.Parse(dataReader["PersonId"].ToString())));
                        spammers[id].FirstName = dataReader["FirstName"].ToString();
                        spammers[id].Status = dataReader["Status"].ToString();
                        spammers[id].LastSolutionLink = dataReader["LastSolutionLink"].ToString();
                        spammers[id].ArrestReportLink = dataReader["ArrestReportLink"].ToString();
                        spammers[id].ArrestEndDateTime = DateTime.Parse(dataReader["ArrestEndDateTime"].ToString());
                        spammers[id].ArrestEndDateTimeNumeric = long.Parse(dataReader["ArrestEndDateTimeNumeric"].ToString());
                        spammers[id].RemovingReportLink = dataReader["RemovingReportLink"].ToString();
                        spammers[id].SpamCount = int.Parse(dataReader["SpamCount"].ToString());

                        spammers[id].Verification.VK_Id = int.Parse(dataReader["VerificationVK_Id"].ToString());
                        spammers[id].Verification.FirstName = dataReader["VerificationFirstName"].ToString();
                        spammers[id].Verification.LastName = dataReader["VerificationLastName"].ToString();
                        spammers[id].Verification.Sex = int.Parse(dataReader["VerificationSex"].ToString());
                        spammers[id].Verification.BirthDay = dataReader["VerificationBirhtDay"].ToString();
                        spammers[id].Verification.HashString = dataReader["VerificationHash"].ToString();
                        spammers[id].Verification.Alias = dataReader["VerificationAlias"].ToString();

                    }
                }
                dataReader.Close();
                CloseDatabase();
            }

        }



       

        //Получаем список представителей союза
        private List<User> GetUnionAdmins() {
            lock(dbLocker) {
                OpenDatabase();

                List<User> unionAdmins = null;
                string sCommand = "SELECT * FROM Persons WHERE Status ='Promote'";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {

                    unionAdmins = new List<User>();
                    while(dataReader.Read()) {

                        User person = new User();
                        person.Id = long.Parse(dataReader["PersonId"].ToString());
                        unionAdmins.Add(person);
                    }
                }
                dataReader.Close();
                CloseDatabase();
                return unionAdmins;
            }
        }

        private bool IsExistedVerificationHash(string verificationHash) {
            lock (dbLocker) {
                OpenDatabase();
                bool isExist = false;

                string sCommand = "SELECT * FROM Persons WHERE VerificationHash =@verificationHash";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@verificationHash", verificationHash);
                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                    isExist =true;

                dataReader.Close();
                CloseDatabase();
                return isExist;
            }
        }

        #endregion

    }
}
