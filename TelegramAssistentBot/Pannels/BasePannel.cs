﻿//Модуль основной панели для создания всей системы и управления важнейшими настройками
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.QiwiModels;
using Message = Telegram.Bot.Types.Message;

namespace TelegramAssistentBot {
    public class BasePannel : DataBase {

        #region INITIALIZE

        public ITelegramBotClient bot;   //Объект бота-исполнителя
        public ITelegramBotClient archBot;
        public User BotProfile;    //Объект профиля бота со всеми данными
        protected Dictionary<long?, Author> authors; //Словарь авторов обращения турнира
        protected string CancelCallback;

        protected InlineKeyboardMarkup keyboard = null;
        protected long idCurrentUser;  //Уникальный идентификатор последнего пользователя

        public long AdministratorId { get; protected set; }
        public long ReserveAdminId { get; protected set; }

        private string BaseBotToken { get; set; }
        public int VerificationAppId { get; private set; }
        public string VerificationAppServiceKey { get; private set; }

        public long BaseChatId { get; protected set; }
        public string BaseChatLink { get; protected set; }
        public string BaseChatUsername { get; protected set; }
        public string BaseChatTittle { get; set; }
        public string ArchiveChatLink { get; protected set; }
        public bool AllowControl { get; protected set; } //Разрешить администраторам основного чата упралять системой
        public bool IsBaseEmpty { get; protected set; } //Проверка на пустую основу
        protected Editor editor { get; private set; }   //Последний редактор основных настроек

        string tempNumber;  //Вспомогательная переменная при установке номера
        public string WalletNumber { get; private set; }    //Номер Qiwi-кошелька

        /// <summary>
        /// Сделать ВаллетТокен в протектед (защищённый)
        protected string WalletToken { get; private set; }  //Токен Qiwi-кошелька
        /// </summary>

        //protected string WalletToken { get; private set; }  //Токен Qiwi-кошелька
        protected DateTime WalletTokenDate { get; private set; }  //Дата обновления токена

        protected double walletBalance;
        protected double nonExistentAmount = -0.004; //Не существующая сумма (Qiwi не считает тысячные от рубля)

        protected int ArrestDurationMins;

        public EventHandler OnBaseCreated;    //Событие при изменении основных настроек
        
        public event EventHandler<RebuildEventArgs> OnRebuild; //Событие при нажатии на кнопку "перегруппировка"
        public class RebuildEventArgs {
            public long PreviousReserveAdminId { get; set; }
            public string Status { get; set; }
            public CallbackQuery Callback { get; set; }
        }

        protected VerificationServer verification; //Объект организующий удостоверения
        protected static LinkPreviewOptions DisableLinks = new LinkPreviewOptions { IsDisabled = true };
        
        //Создаём экземпляр основной панели
        public BasePannel(): base() {
            
            IsBaseEmpty = true;
            CreateDatabase();

            if (!isDatabaseExists) {
                Console.WriteLine("Введите токен бота:");
                BaseBotToken = Console.ReadLine();
                CreateDatabase();
            }

            authors = new Dictionary<long?, Author>();
            editor = new Editor();

            CreateTableBases(connection);
            RebuildBaseSystem();
            RestartVerificationServer();

            bot = new TelegramBotClient(BaseBotToken);
            BotProfile = bot.GetMeAsync().Result;
        }

        //Перестраиваем систему
        private void RebuildBaseSystem() {
            LoadBaseSettings();
            GetBaseChatUsername();
        }


        //Перезапускаем сервер с удостоверениями
        protected void RestartVerificationServer() {

            if (VerificationAppId != 0
                && !string.IsNullOrEmpty(VerificationAppServiceKey)) {

                if(verification == null) {

                    verification = new VerificationServer(VerificationAppId, VerificationAppServiceKey);
                    verification.StartVerificationServer();
                } else {
                    verification.ResetAppIdAndServiceKey(VerificationAppId, VerificationAppServiceKey);
                }
            }
        }

        
        public async void SendTest() {

            HttpClient client1 = new HttpClient();
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string url1 = $"https://edge.qiwi.com//payment-notifier/v1/hooks/test";
            HttpResponseMessage response1 = client1.GetAsync(url1).Result;
            string result1 = await response1.Content.ReadAsStringAsync();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(result1);
            Console.WriteLine();
            Console.WriteLine();
        }


        protected async Task<string> GetPaymentSecretKey(string hookId) {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string url = $"https://edge.qiwi.com//payment-notifier/v1/hooks/{hookId}/key";
            HttpResponseMessage response = client.GetAsync(url).Result;
            string key = string.Empty;
            if (response.StatusCode == System.Net.HttpStatusCode.OK) {
                string result = await response.Content.ReadAsStringAsync();
                QiwiSecretKey qiwiSecretKey = JsonConvert.DeserializeObject<QiwiSecretKey>(result);
                key = qiwiSecretKey.key;
            }
            return key;
        }



        #endregion

        #region ACCEPT ANY MESSAGES AND CALLBACKS

        //Принимаем и обрабатываем сообщение основной настройки
        protected void AcceptBasePannelMessage(Message message, string cancelCallback) {
            long idUser = message.From.Id;
            AddAuthor(message.From);

            if (IsBaseEmpty) {
                CreateBaseSettings(idUser, message.Text);
            } else {
                if (!string.IsNullOrEmpty(authors[idUser].CallbackData)) {
                    AcceptSetMessage(message, cancelCallback);
                }
            }


        }

        //Добавляем автора в список
        protected void AddAuthor(User user) {
            if (!authors.ContainsKey(user.Id)) {
                authors.Add(user.Id, new Author(user)); ;
            }
        }

        //Принимаем обратный вызов для основной настройки
        protected void AcceptBasePannelCallback(CallbackQuery callback, string cancelCallback) {
            long idUser = callback.From.Id;
            if (IsBaseEmpty) {
                AcceptBaseCreateCallback(idUser, cancelCallback);
            } else {
                AcceptBaseManageCallback(idUser, callback, cancelCallback);
            }
        }

        #endregion

        #region CREATE BASE SETTINGS

        //Принимаем обратный вызов для создания основных настроек
        private void AcceptBaseCreateCallback(long idUser, string cancelCallback) {

            string text = string.Empty;
            switch (authors[idUser].CallbackData) {
                case "YesIamFounderCallback":

                    AdministratorId = idUser;
                    text = "Замечательно. Рад с вами познакомиться." +
                        "\nДавайте продолжим настройку.";

                    SendSettingsMessage(idUser, text, null);
                    CreateBaseSettings(idUser, null);
                    break;
                case "NoIamnotFounderCallback":

                    text = "Тогда приглашаю основателя." +
                        "\nПусть напишет и мы проведём настройку.";
                    AdministratorId = 0;
                    authors[idUser].IdEditMessage = 0;
                    idCurrentUser = idUser;

                    EditMessageToUser(idUser, text, null);
                    break;

                case "SkipWalletStepCallback":
                    WalletNumber = "NULL";
                    WalletToken = "NULL";

                    CreateBaseSettings(idUser, null);
                    break;
                default:
                    CreateAndSaveBaseSettingsFromCallback(idUser);
                    break;
            }
        }

        //Создаём основные настройки
        private void CreateBaseSettings(long idUser, string mText) {
            if (AdministratorId == 0) {
                CreateAdministratorId(idUser);
            } else {
                if (BaseChatId == 0) {
                    CreateBaseChatIdAndLink(idUser, mText);
                } else {
                    if (string.IsNullOrEmpty(WalletToken)) {
                        CreateWalletNumberAndToken(idUser, mText);
                    } else {
                        CreateAllowControl(idUser);
                    }
                }
            }
        }

        //Создаём уникальный номер администратора системы
        private void CreateAdministratorId(long idUser) {
            string text = string.Empty;
            text = "<i>🥇  Шаг <b>1</b>/5.</i> [Основная настройка]" +
                    "\nЗдравствуйте." +
                    "\nДавайте установим основные настройки союза." +
                    "\n\nВы ответственный руководитель?";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Да, я руководитель", "YesIamFounderCallback")
                },

                new []{
                    InlineKeyboardButton.WithCallbackData("Нет, я только помогаю", "NoIamnotFounderCallback"),
                }});
            SendSettingsMessage(idUser, text, keyboard);
        }

        //Создаём адрес основного чата
        private async void CreateBaseChatIdAndLink(long idUser, string mText) {
            keyboard = null;
            string text = string.Empty;

            if (!string.IsNullOrEmpty(mText)) {
                text = "<i>🥇  Шаг <b>2</b>/5.</i> [Основная настройка]" +
                            "\nНе смог найти такую <b>супергруппу</b>." +
                            "\n\n<i>(Проверьте адрес и пришлите снова)</i>" +
                            "\n<i>Пример: @telegramgamechat</i>";

                if (mText.Contains("@")) {

                    await Task.Run(() => {
                        try {
                            Chat chat = bot.GetChatAsync(mText).Result;
                            if (chat.Type == ChatType.Supergroup) {
                                BaseChatId = chat.Id;
                                BaseChatTittle =chat.Title;
                                BaseChatLink = CreateChatLink(chat.Id);
                                BaseChatUsername = chat.Username;

                                text = "Замечательно. Общую <b>супергруппу</b> установили." +
                                    "\nПродолжаем настройку.";
                                SendSettingsMessage(idUser, text, null);
                                CreateBaseSettings(idUser, null);
                            } else {
                                SendSettingsMessage(idUser, text, keyboard);
                            }

                        } catch {
                            SendSettingsMessage(idUser, text, keyboard);
                        }
                    });
                } else {
                    SendSettingsMessage(idUser, text, keyboard);
                }
            } else {
                text = "<i>🥇  Шаг <b>2</b>/5</i> [Основная настройка]" +
                        "\nУстанавливаем <b>общую супергруппу</b> совета участников." +
                        "\n\n<i>(Пришлите адрес и сделайте меня администратором)</i>" +
                        "\n<i>Пример: @telegramgamechat</i>";

                SendSettingsMessage(idUser, text, null);
            }
        }


        //Создаём ссылку с подтверждением входа
        private string CreateChatLink(long idChat) {

            ChatInviteLink link = bot.CreateChatInviteLinkAsync(idChat
                , name: "Bot's main link with approwing."
                , createsJoinRequest: true).Result;

            return link.InviteLink;
        }

        //Создаём номер и токен Qiwi-кошелька
        private void CreateWalletNumberAndToken(long idUser, string mText) {
            string text = string.Empty;

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Продолжить без кошелька", "SkipWalletStepCallback")
                    }
                });

            if (!string.IsNullOrEmpty(mText)) {
                if (string.IsNullOrEmpty(WalletNumber)) {
                    CreateWalletNumber(idUser, mText);
                } else {
                    if (string.IsNullOrEmpty(WalletToken)) {
                        CreateWalletToken(idUser, mText);
                    }
                }
            } else {
                text = "<i>🥇  Шаг <b>3</b>/5.</i> [Основная настройка]" +
                        "\nТеперь пришлите <b>номер</b> Qiwi-кошелька." +
                        "\n\n<i>(Отправьте номер без знака \"+\")</i>" +
                        "\n<i>Пример: 79778140892</i>";

                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Создаём номер Qiwi-кошелька
        private void CreateWalletNumber(long idUser, string walletNumber) {
            double wNumber;
            keyboard = null;
            string text = string.Empty;

            if (double.TryParse(walletNumber, out wNumber)) {
                WalletNumber = wNumber.ToString();

                text = "<i>🥇  Шаг <b>4</b>/5.</i> [Основная настройка]" +
                            "\nТеперь пришлите <b>токен</b> Qiwi-кошелька <b>со всеми</b> разрешениями." +
                            "\n\n<i>(<a href=\"https://qiwi.com/api\">" +
                            "Получить токен Qiwi-кошелька</a>)</i>" +
                            "\n<i>Пример: U1QtOTkwMTAyLWNud3FpdWhmbzg3M</i>";

                SendSettingsMessage(idUser, text, keyboard);
            } else {
                text = "<i>Попробуем снова!</i> [Основная настройка]" +
                            "\nНе получилось связаться с Qiwi-кошельком." +
                            "\n\n<i>Проверьте номер кошелька и пришлите по примеру:</i>" +
                            "\n<i>Пример: 79778140892</i>";
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Создаём токен Qiwi-кошелька
        private void CreateWalletToken(long idUser, string mText) {
            string text = string.Empty;

            if (!HasPaymentRestrictions(WalletNumber, mText)) {
                WalletToken = mText;
                CreateBaseSettings(idUser, null);
            } else {
                WalletToken = null;
                WalletNumber = null;

                text = "<i>Попробуем снова!</i> [Основная настройка]" +
                    "\nНе получилось связаться с Qiwi-кошельком." +
                    "\n\n<i>Проверьте номер кошелька и пришлите по примеру:</i>" +
                    "\n<i>Пример: 79778140892</i>";

                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Проверяем есть ли ограничения на данном кошельке
        protected bool HasPaymentRestrictions(string walletNumber, string walletToken) {
            try {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + walletToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string url = $"https://edge.qiwi.com/person-profile/v1/persons/{walletNumber}/status/restrictions";

                HttpResponseMessage response = client.GetAsync(url).Result;
                if (response.StatusCode == HttpStatusCode.OK) {

                    string result = response.Content.ReadAsStringAsync().Result;
                    if (result == "[]") {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            } catch {
                return true;
            }
        }

        //Получаем текущий баланс 
        protected async Task<double> GetWalletBalanceAsync(string walletNumber) {

            double balance = nonExistentAmount;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string url = $"https://edge.qiwi.com/funding-sources/v2/persons/{walletNumber}/accounts";
            HttpResponseMessage response = client.GetAsync(url).Result;
            if (response.StatusCode == System.Net.HttpStatusCode.OK) {
                string result = await response.Content.ReadAsStringAsync();
                QiwiBalance qiwiBalance = JsonConvert.DeserializeObject<QiwiBalance>(result);
                balance = qiwiBalance.accounts[0].balance.amount;
            }
            return balance;
        }

        //Создаём разрешение управлять всем представителям союза
        private void CreateAllowControl(long idUser) {
            string text = string.Empty;

            text = "<i>🥇  Шаг <b>5</b>/5</i>. [Системная настройка]" +
                "\nУстанавливаем права для помощников." +
                "\n\nРазрешаем администраторам <b>общего совета</b> управлять системой?";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Да, пусть тоже управляют", "AllowControlCallback")
                },

                new []{
                    InlineKeyboardButton.WithCallbackData("Нет, управляю только я", "DisallowControlCallback")
                }});
            SendSettingsMessage(idUser, text, keyboard);
        }


        //Создаём и сохраняем основные настройки из обратного вызова
        private void CreateAndSaveBaseSettingsFromCallback(long idUser) {
            if (authors[idUser].CallbackData == "AllowControlCallback"
                    || authors[idUser].CallbackData == "DisallowControlCallback") {

                switch (authors[idUser].CallbackData) {
                    case "AllowControlCallback":
                        AllowControl = true;
                        break;
                    case "DisallowControlCallback":
                        AllowControl = false;
                        break;
                }

                WalletTokenDate = DateTime.Now;
                editor.Id = authors[idUser].Id;

                //Создаём основную таблицу
                CreateTableBases(connection);
                InitializeAndSaveBaseSettings();

                IsBaseEmpty = false;

                string text = string.Empty;

                text = "Великолепно!" +
                    "\nМы выполнили <b>основную</b> настройку!";
                SendSettingsMessage(idUser, text, null);

                authors[idUser].IdEditMessage = 0;
                OnBaseCreated?.Invoke(this, new EventArgs());
            }
        }

        #endregion

        #region SHOW AND SET BASE SETTINGS

        //Принимаем обратный вызов управления основными настройками
        private void AcceptBaseManageCallback(long idUser, CallbackQuery callback, string cancelCallback) {
            switch (authors[idUser].CallbackData) {

                case "ShowBasePageCallback":
                    ShowBasePage(idUser, cancelCallback);
                    break;

                case "ShowBaseWalletPageCallback":
                    ShowBaseWalletPage(idUser, "ShowBasePageCallback");
                    break;

                default:
                    AcceptSetCallback(idUser, callback, cancelCallback);
                    break;
            }
        }

        //Показываем страницу основных настроек
        private void ShowBasePage(long idUser, string cancelCallback) {
            string sAllowControl = string.Empty;
            string text = string.Empty;

            authors[idUser].IdEditMessage = 0;
            authors[idUser].CallbackData = null;
            tempAppId = 0;

            if (AllowControl) {
                sAllowControl = "🥁  Управляют представители.";
            } else {
                sAllowControl = "👑  Вы управляете системой.";
            }
            

            text = $"\n\n<b>Пояснения:</b>" +
                $"\n🎓 <a href=\"{BaseChatLink}\">Основной чат</a>" +
                $"\n🏺 <a href=\"{ArchiveChatLink}\">Архивный чат</a>" +
                $"\n🧲  Номер кошелька: <b>{WalletNumber}</b>" +
                $"\n{sAllowControl}" +
                $"\n\n<i>Союз ведёт <a href=\"tg://user?id={editor.Id}\">ID {editor.Id}</a></i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Сервис удостоверений","SetVerificationAppIdCallback")
                    , InlineKeyboardButton.WithCallbackData("Заместитель (адм)","SetReserveAdminCallbask")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Чат","SetBaseChatAddressCallback")
                    , InlineKeyboardButton.WithCallbackData("Кошелёк","ShowBaseWalletPageCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Назад", cancelCallback)
                    , InlineKeyboardButton.WithCallbackData("Управление", "SetAllowControlCallback")
                }
            });

            if (authors[idUser].IdEditMessage != 0) {
                EditMessage(idUser, text);
            } else {
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Показываем страницу настроек Qiwi-кошелька
        private void ShowBaseWalletPage(long idUser, string cancelCallback) {
            string text = string.Empty;

            text = $"<i>Настраиваем кошелёк</i>" +
                $"\n\n<b>Сведения о Qiwi-кошельке:</b>" +
                $"\n🧲  Номер: {WalletNumber}" +
                $"\n<i>Токен до {WalletTokenDate.AddDays(180).ToString("dd/MM/yyyy HH:mm")}</i>" +
                $"\n\n<i>Что следует изменить?</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{

                    InlineKeyboardButton.WithCallbackData("Номер","SetWalletNumberCallback"),
                    InlineKeyboardButton.WithCallbackData("Токен","SetWalletTokenCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Назад",cancelCallback),
                    //InlineKeyboardButton.WithCallbackData("Уведомления","SetWalletNotificationCallback")
                }
            });

            if (authors[idUser].IdEditMessage != 0) {
                EditMessage(idUser, text);
            } else {
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Принимаем обратный вызов с установкой настроек
        private void AcceptSetCallback(long idUser, CallbackQuery callback, string cancelCallback) {
            switch (authors[idUser].CallbackData) {
                case "SetBaseChatAddressCallback":
                    ShowBaseChatIdPage(idUser, "ShowBasePageCallback");
                    break;
                case "SetArchiveChatAddressCallback":
                    ShowArchiveChatPage(idUser, "ShowBasePageCallback");
                    break;

                case "SetReserveAdminCallbask":
                    ShowReserveAdminPage(idUser, "ShowBasePageCallback");
                    break;

                case "SetVerificationAppIdCallback":
                    ShowVerificationAppIdPage(idUser, "ShowBasePageCallback");
                    break;
                case "SetVerificationAppServiceKeyCallback":
                    ShowVerificationAppSeriviceKeyPage(idUser, "ShowBasePageCallback");
                    break;

                case "SetAllowControlCallback":
                    if (idUser == AdministratorId) {
                        SetAllowControl(idUser, cancelCallback);
                    } else {
                        try {
                            bot.AnswerCallbackQueryAsync(callback.Id, "⚠️  Доступно только основателю.");
                        } catch { }
                    }
                    break;

                case "SetWalletNumberCallback":
                    ShowWalletNumberPage(idUser, "ShowBaseWalletPageCallback");
                    break;
                case "SetWalletTokenCallback":
                    ShowWalletTokenPage(idUser, "ShowBaseWalletPageCallback");
                    break;

                case "SetWalletNotificationCallback":
                    ShowWalletNotificationPage(idUser, "ShowBaseWalletPageCallback");
                    break;
                default:
                    RebuildBaseSystemWithEvent(callback);
                    break;
            }
        }

        //Показываем страницу с установкой участника с должностью ИО ЧС 
        private void ShowReserveAdminPage(long idUser, string cancelCallback) {
            string text = string.Empty;

            text = "Пришлите Телеграм ID участника исполняющего обязанности администратора в чрезвычайных ситуациях (ИО ЧС)" +
                "\n\n<i>Пример:</i>" +
                "\n<b>6802950856</b>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessage(idUser, text);
        }


        //Показываем страницу с установкой номера основного чата
        private void ShowBaseChatIdPage(long idUser, string cancelCallback) {
            string text = string.Empty;

            text = "Хорошо.\n" +
                "Теперь, пришлите адрес <b> основного</b> чата." +
                "\n\n<i>Пример:</i>" +
                "\n<b>@wearegames</b>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessage(idUser, text);
        }

        private void ShowArchiveChatPage(long idUser, string cancelCallback) {
            string text = "Хорошо.\n" +
                "Теперь, пришлите <b>ссылку</b> на архивный чат." +
                "\n\n<i>Пример:</i>" +
                "\n<b>https://t.me/archive_chat</b>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        //Устанавливаем права управления системой для представителей
        private void SetAllowControl(long idUser, string cancelCallback) {
            if (AllowControl == false) {
                AllowControl = true;
            } else {
                AllowControl = false;
            }

            editor.Id = authors[idUser].Id;

            authors[idUser].CallbackData = null;
            SaveAllowControl();
            LoadBaseSettings();

            if (IsBaseEmpty) {
                IsBaseEmpty = false;
                OnBaseCreated?.Invoke(this, new EventArgs());
            }
            ShowBasePage(idUser, cancelCallback);
        }

        //Показываем страницу установки номера Qiwi-кошелька
        private void ShowWalletNumberPage(long idUser, string cancelCallback) {
            string text = "Хорошо.\n" +
                "\nПришлите <b>номер</b> Qiwi-кошелька." +
                        "\n\n<i>(Номер без \"+\")</i>" +
                        "\n<i>Пример: 79778140892</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessage(idUser, text);
        }

        //Показываем страницу установки токена для Qiwi-кошелька
        private void ShowWalletTokenPage(long idUser, string cancelCallback) {
            string text = "Пришлите <b>токен</b> Qiwi-кошелька <b>со всеми</b> разрешениями:" +
                 "\n\n<i>(<a href=\"https://qiwi.com/api\">" +
                 "Получить токен Qiwi-кошелька</a>)</i>" +
                 "\n<i>Пример: U1QtOTkwMTAyLWNud3FpdWhmbzg3M</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            SendSettingsMessage(idUser, text, keyboard);
        }

        private void ShowWalletNotificationPage(long idUser, string cancelCallback) {
            string text = "Пришлите <b>URL</b> сервера с длиной оригинального (не URL-encoded)" +
                " адреса обработчика не более 100 символов.\n" +
                 "\n\n<i>Пример: https://dd26-94-25-171-65.eu.ngrok.io</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            SendSettingsMessage(idUser, text, keyboard);
        }

        //Пересобираем систему с отправкой события
        private void RebuildBaseSystemWithEvent(CallbackQuery callback) {
            LoadBaseSettings();
            GetBaseChatUsername();
            OnRebuild?.Invoke(this, new RebuildEventArgs { Callback = callback });
        }

//Получаем пользовательское-имя чата
    protected string GetChatUsername(long chatId){
        if(chatId ==0)
            return null;
        try {
            Chat chat = bot.GetChatAsync(chatId).Result;
            return chat.Username;
        } catch { }
        return null;
    }

        //Получаем адрес основного чата
        protected string GetBaseChatUsername() {
            if (BaseChatId != 0) {
                try {
                    Chat chat = bot.GetChatAsync(BaseChatId).Result;
                    BaseChatUsername = chat.Username;
                    return BaseChatUsername;
                } catch { }
            }
            return null;
        }

        //Получаем заголовок чата
        protected string GetBaseChatTitle() {
            if(!string.IsNullOrEmpty(BaseChatTittle))
                return BaseChatTittle;
            
            if (BaseChatId != 0) {
                try {
                    Chat chat = bot.GetChatAsync(BaseChatId).Result;
                    BaseChatTittle = chat.Title;
                    return BaseChatTittle;
                } catch { }
            }
            return null;
        }

        private Task<ChatFullInfo> GetChat(long chatId) {
            return bot.GetChatAsync(chatId);
        }


        //Принимаем сообщение с установкой параметра
        private void AcceptSetMessage(Message message, string cancelCallback) {
            long idUser = message.From.Id;
            string text = message.Text;
            switch (authors[idUser].CallbackData) {
                case "SetVerificationAppIdCallback":
                    SetVerificationAppId(idUser, message.Text, cancelCallback);
                    break;
                case "SetVerificationAppServiceKeyCallback":
                    if(tempAppId == 0) {
                        SetVerificationAppServiceKey(idUser, VerificationAppId, text, cancelCallback);
                    } else {
                        SetVerificationAppServiceKey(idUser, tempAppId, text, cancelCallback);
                    }
                    break;
                case "SetBaseChatAddressCallback":
                    SetBaseChatIdsAndLink(idUser, text, cancelCallback);
                    break;
                case "SetArchiveChatAddressCallback":
                    SetBaseArvhiveLink(idUser, text, cancelCallback);
                    break;

                case "SetReserveAdminCallbask":
                    SetReserveAdminCallbask(idUser, text, cancelCallback);
                    break;
                case "SetWalletNumberCallback":
                    SetWalletNumber(idUser, text, cancelCallback);
                    break;
                case "SetWalletTokenCallback":
                    if (string.IsNullOrEmpty(tempNumber)) {
                        SetWalletToken(idUser, WalletNumber, text, cancelCallback);
                    } else {
                        SetWalletToken(idUser, tempNumber, text, cancelCallback);
                    }
                    break;
            }
        }

        //Назначаем ИО ЧС (Второго админа)
        private async void SetReserveAdminCallbask(long idUser, string textIdSecondAdmin, string cancelCallback) {
            long id = 0;
            
            string text = "Не получилось. Давайте попробуем снова.\n" +
                "Пришлите Телеграм ID участника исполняющего обязанности администратора в чрезвычайных ситуациях (ИО ЧС)" +
                "\n\n<i>Пример:</i>" +
                "\n<b>6802950856</b>";

            keyboard = new InlineKeyboardMarkup(new[] { InlineKeyboardButton.WithCallbackData("Отмена", "ShowBasePageCallback") });

            if (long.TryParse(textIdSecondAdmin, out id)) {
                try {
                    ChatMember admin2 = await bot.GetChatMemberAsync(BaseChatId, id);

                    id = ReserveAdminId; //Сохраняю предыдущего запасного админа для того, чтобы снять с него права
                    ReserveAdminId = admin2.User.Id;
                    SaveReserveAdminId();
                    OnRebuild?.Invoke(this, new RebuildEventArgs { PreviousReserveAdminId = id, Status = "ResetAdministrator2" });
                    text = "Выполнено! " +
                        $"\n<i>Теперь" +
                        $" <a href=\"tg://user?id={ReserveAdminId}\">{admin2.User.FirstName}</a>" +
                        $" </i> [ <code>{ReserveAdminId}</code> ] внесён в базу данных как ИО ЧС.";
                    SendSettingsMessage(idUser, text, null);

                    authors[idUser].IdEditMessage = 0;
                    authors[idUser].CallbackData = null;
                } catch {
                    SendSettingsMessage(idUser, text, keyboard);
                }
            }
        }

        private void ShowVerificationAppIdPage(long idUser, string cancelCallback) {
            string text = "Пришлите номер приложения";

            keyboard = new InlineKeyboardMarkup(new[] { InlineKeyboardButton.WithCallbackData("Отмена", "ShowBasePageCallback") });
            SendSettingsMessage(idUser, text, keyboard);
        }

        int tempAppId = 0;
        private async void SetVerificationAppId(long idUser, string sAppId, string cancelCallback) {
            int appId = 0;
            if (!string.IsNullOrEmpty(sAppId)
                && int.TryParse(sAppId, out appId)) {

                tempAppId = appId;
                authors[idUser].CallbackData = "SetVerificationAppServiceKeyCallback";
                ShowVerificationAppSeriviceKeyPage(idUser, "ShowBasePageCallback");
            }
        }

        private void ShowVerificationAppSeriviceKeyPage(long idUser, string cancelCallback) {
            string text = "Теперь пришлите <b>сервисный ключ доступа</b> верификации.";

            keyboard = new InlineKeyboardMarkup(new[] { InlineKeyboardButton.WithCallbackData("Отмена", "ShowBasePageCallback") });
            SendSettingsMessage(idUser, text, keyboard);
        }


        private void SetVerificationAppServiceKey(long idUser, int appId, string secretKey, string cancelCallback) {
            string text = "Не получилось.";

            keyboard = new InlineKeyboardMarkup(new[] { InlineKeyboardButton.WithCallbackData("Отмена", "ShowSystemPageCallback") });

            if (!string.IsNullOrEmpty(secretKey)) {
                VerificationAppId = appId;
                VerificationAppServiceKey = secretKey;

                SaveVerificationIdAndKey();
                LoadBaseSettings();
                RestartVerificationServer();

                authors[idUser].IdEditMessage = 0;
                authors[idUser].CallbackData = null;
                tempAppId = 0;

                text = "Поздравляю! Ключ установлен.";
                keyboard = new InlineKeyboardMarkup(new[] {
                    new[] { InlineKeyboardButton.WithUrl("Проверить", verification.GetVkontakteIDAuthorizationUri(idUser)) }
                });
            }
            SendSettingsMessage(idUser, text, keyboard);
        }


        //Устанавливаем адрес основного чата
        private async void SetBaseChatIdsAndLink(long idUser, string command, string cancelCallback) {

            string text = "<i>Стоп.</i> Не получается." +
                $"\nДавайте проверим адрес <b>чата</b> и повторим." +
                "\n\n<i>Пример:</i> <b>@wearegames</b>";

            keyboard = new InlineKeyboardMarkup(new[]{

                InlineKeyboardButton.WithCallbackData("Отмена", "ShowSystemPageCallback")
                });

            await Task.Run(() => {
                try {

                    Chat chat1 = bot.GetChatAsync(command).Result;
                    if (!string.IsNullOrEmpty(command)
                    && command.Contains("@")) {
                        ChatFullInfo chat = bot.GetChatAsync(command).Result;

                        if (chat.Type == ChatType.Supergroup) {
                            editor.Id = authors[idUser].Id;

                            BaseChatId = chat.Id;
                            BaseChatUsername = chat.Username;
                            BaseChatLink = chat.InviteLink;

                            SaveBaseChatId(BaseChatId);
                            SaveBaseChatLink(BaseChatLink);

                            authors[idUser].CallbackData = "SetArchiveChatAddressCallback";
                            ShowArchiveChatPage(idUser, cancelCallback);
                        } else {
                            SendSettingsMessage(idUser, text, keyboard);
                        }
                    } else {
                        SendSettingsMessage(idUser, text, keyboard);
                    }
                } catch (Exception ex) {
                    Console.Write(ex.Message);
                    SendSettingsMessage(idUser, text, keyboard);
                }
            });
        }




        private async void SetBaseArvhiveLink(long idUser, string inLink, string cancelCallback) {

            string text = "<i>Стоп.</i> Не получается." +
                $"\nДавайте проверим ссылку на адрес <b>чата</b> и повторим." +
                "\n\n<i>Пример:</i> <b>https://t.me/archive_chat</b>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", "ShowSystemPageCallback")
            });

            if (!string.IsNullOrEmpty(inLink) && !inLink.Contains("https://t.me/")){
                SendSettingsMessage(idUser, text, keyboard);
                return;
            }

            editor.Id = authors[idUser].Id;
            
            SaveBaseArvhiveLink(inLink);
            LoadBaseSettings();

            authors[idUser].IdEditMessage = 0;
            authors[idUser].CallbackData = null;

            OnBaseCreated?.Invoke(this, new EventArgs());
            ShowBasePage(idUser, cancelCallback);
        }

        //Устанавливаем номер Qiwi-кошелька
        private void SetWalletNumber(long idUser, string number, string cancelCallback) {
            string text = "<i>Стоп.</i> Не получается." +
                $"\nДавайте проверим <b>номер</b> кошелька и повторим." +
                "\n\n<i>Пример:</i> <b>79778140892</b>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)});

            double num;
            if(double.TryParse(number, out num)) {
                tempNumber = number;
                authors[idUser].CallbackData = "SetWalletTokenCallback";
                ShowWalletTokenPage(idUser, "ShowBaseWalletPageCallback");
            } else {
                tempNumber = string.Empty;
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Устанавливаем токен кошелька
        private void SetWalletToken(long idUser, string number, string token, string cancelCallback) {
            string text = string.Empty;

            if (!HasPaymentRestrictions(number, token)) {
                WalletNumber = number;
                WalletToken = token;
                WalletTokenDate = DateTime.Now;

                editor.Id = authors[idUser].Id;
                
                SaveBaseWalletNumberAndToken();
                OnBaseCreated?.Invoke(this, new EventArgs());

                authors[idUser].IdEditMessage = 0;
                authors[idUser].CallbackData = null;
                tempNumber = string.Empty;

                ShowBaseWalletPage(idUser, cancelCallback);
            } else {
                if(string.IsNullOrEmpty(tempNumber)) {

                    text = "<i>Стоп.</i> Не получается." +
                        $"\nПроверим <b>токен</b> кошелька <b>со всеми</b> разрешениями и повторим." +
                        "\n\n<i>(<a href=\"https://qiwi.com/api\">" +
                        "Получить токен Qiwi-кошелька</a>)</i>" +
                        "\n<i>Пример: U1QtOTkwMTAyLWNud3FpdWhmbzg3M</i>";
                } else {
                    tempNumber = string.Empty;
                    authors[idUser].CallbackData = "SetWalletNumberCallback";

                    text = "<i>Попробуем снова!</i>" +
                        "\nНе получилось связаться с Qiwi-кошельком." +
                        "\n\n<i>Проверьте номер кошелька и пришлите по примеру:</i>" +
                        "\n<i>Пример: 79778140892</i>";
                }

                keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", "ShowBaseWalletPageCallback")});
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        private async Task<QiwiWebHook> GetActiveHooks() {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string url1 = $"https://edge.qiwi.com/payment-notifier/v1/hooks/active";
            HttpResponseMessage response = await client.GetAsync(url1);
            string result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<QiwiWebHook>(result);
        }

        #endregion

        #region EXTERNAL FUNCTIONS

        //Получаем строку с финансовой помощью
        protected string GetDonateUrl(long idUser) {

            return "https://qiwi.com/payment/form/99" +
                $"?extra['account']={WalletNumber}&amountInteger=200&amountFraction=0" +
                $"&blocked[0]=account" +
                $"&extra['comment']=Помощь от {GetUserName(idUser).Replace("\"", "'")} [ID {idUser}]";
        }

        //Получаем ссылку на сообщение из уникального номера
        protected string GetBaseLinkFromMessageId(int messageId, int? messageThreadId =null, bool isTopic =false) {

            if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                if (messageThreadId == null) {
                    if (isTopic) {
                        return $"https://t.me/c/{BaseChatId.ToString().Substring(4)}/1/{messageId}";
                    } else {
                        //return $"https://t.me/c/{BaseChatId.ToString().Substring(4)}/{messageId}";
                        return "";
                    }
                } else {
                    return $"https://t.me/c/{BaseChatId.ToString().Substring(4)}/{messageThreadId}/{messageId}";
                }
            } else {
                if (messageThreadId == null) {
                    if (isTopic) {
                        return $"https://t.me/{BaseChatUsername.Trim('@')}/1/{messageId}";
                    } else {
                        return $"https://t.me/{BaseChatUsername.Trim('@')}/{messageId}";
                    }
                } else {
                    return $"https://t.me/{BaseChatUsername.Trim('@')}/{messageThreadId}/{messageId}";
                }
            }
        }

//Получаем ссылку по сообщению
        protected string GetLinkFromMessage(long chatId, int messageId, int? messageThreadId =null, bool isTopic =false) {
            string chatUsername = GetChatUsername(chatId);
            if (string.IsNullOrEmpty(chatUsername)) {
                if (messageThreadId == null) {
                    if (isTopic) {
                        return $"https://t.me/c/{chatId.ToString().Substring(4)}/1/{messageId}";
                    } else {
                        return "";
                    }
                } else {
                    return $"https://t.me/c/{chatId.ToString().Substring(4)}/{messageThreadId}/{messageId}";
                }
            } else {
                if (messageThreadId == null) {
                    if (isTopic) {
                        return $"https://t.me/{chatUsername.Trim('@')}/1/{messageId}";
                    } else {
                        return $"https://t.me/{chatUsername.Trim('@')}/{messageId}";
                    }
                } else {
                    return $"https://t.me/{chatUsername.Trim('@')}/{messageThreadId}/{messageId}";
                }
            }
        }

        protected int ForwardMessage(Message message, ChatId chatId) {
            try {
                return bot.ForwardMessageAsync(chatId
                    , message.Chat.Id
                    , message.MessageId
                    , disableNotification: true
                    , protectContent: false).Result.MessageId;
            } catch {
                return 0;
            }
        }


        //Получаем поисковой номер для сообщения в архиве
        protected string GetMessageArchiveNumber(Message message) {
            if(message != null)
                return $"N{message.MessageId} | T{message.MessageThreadId}" +
                $" | {message.Date.AddHours(3).ToString("dd/MM/yyyy HH:mm:ss")} (+3 МСК)";else 
            return null;
        }

        //Получаем поисковой номер для пересланного сообщения в архиве
        protected string GetForwardedMessageArchiveNumber(int msgId, int? msgThreadId, DateTime? msgDateTime) {
            if (msgDateTime != null)
                return $"N{msgId} | T{msgThreadId}"+
                $" | {((DateTime)msgDateTime).AddHours(3).ToString("dd/MM/yyyy HH:mm:ss")} (+3 МСК)";
                else
            return null;
        }



        #endregion

        #region TELEGRAM METHODS



        //Отправляем сообщение текущему пользователю
        public void SendSettingsMessage(long idUser, string title, InlineKeyboardMarkup keyboard) {

            try {
                if(keyboard != null) {
                    authors[idUser].IdEditMessage = bot.SendTextMessageAsync(
                    chatId: idUser, text: title, replyMarkup: keyboard, linkPreviewOptions: DisableLinks
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html).Result.MessageId;
                } else {
                    authors[idUser].IdEditMessage = bot.SendTextMessageAsync(
                    chatId: idUser, text: title, linkPreviewOptions: DisableLinks
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html).Result.MessageId;
                }
                idCurrentUser = idUser;
            } catch { }
        }

        //Отправляем сообщение
        protected void EditMessageToUser(long idUser, string text, InlineKeyboardMarkup keyboard, int repliedMessageId =0) {
            if (authors.ContainsKey(idUser)) {
                try {
                  ReplyParameters replyParam = new ReplyParameters { MessageId = repliedMessageId };
                    if (keyboard != null){

                    authors[idUser].IdEditMessage =bot.SendTextMessageAsync(
                        chatId: idUser
                        , text: text
                        , replyParameters: replyParam
                        , replyMarkup: keyboard
                        , linkPreviewOptions: DisableLinks
                        , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html).Result.MessageId;
                    } else {
                        authors[idUser].IdEditMessage =bot.SendTextMessageAsync(
                        chatId: idUser
                        , text: text
                        , replyParameters: replyParam
                        , linkPreviewOptions: DisableLinks
                        , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html).Result.MessageId;
                    }

                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.Source);
                    Console.WriteLine(ex.HResult);
                }
            }
        }

        protected async Task<Message> SendTecnicMessage(ChatId srcChatId, string inText
        , InlineKeyboardMarkup keyboard, ChatId replyChatId =null, int replyMessageId =0, int? messageThreadId =null){
             
             if(replyChatId ==null || replyMessageId ==0) return null;

             return await bot.SendMessage(
                chatId: srcChatId
                , text: inText
                , messageThreadId: messageThreadId
                , replyParameters: new ReplyParameters{ ChatId =replyChatId
                    , MessageId = replyMessageId});
        }

        protected void SendMessage(ChatId idUser, string text
        , InlineKeyboardMarkup keyboard, int messageReplyId =0)  {

            try {
            if(keyboard != null) {
                bot.SendTextMessageAsync(
                    chatId: idUser
                    , text: text
                    , replyParameters: new ReplyParameters { MessageId = messageReplyId }
                    , replyMarkup: keyboard
                    , linkPreviewOptions: DisableLinks
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
                } else {
                    bot.SendTextMessageAsync(
                    chatId: idUser
                    , text: text
                    , replyParameters: new ReplyParameters { MessageId =messageReplyId}
                    , linkPreviewOptions: DisableLinks
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
                }
            } catch { }
        }

        //Отправляем сообщение с параметром отмены
        protected void SendMessageWithCancel(ChatId idUser, string text, string cancelCallback) {

            try {
                keyboard = new InlineKeyboardMarkup(new[]{
                    InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)}) ;

                bot.SendTextMessageAsync(
                        chatId: idUser
                        , text: text
                        , replyMarkup: keyboard
                        , linkPreviewOptions: DisableLinks
                        , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
                authors[idUser.Identifier].IdEditMessage = 0;
            } catch { }
        }



        //Отправляем изображение в чат
        public int SendPhoto(ChatId chatId, Stream streamPhoto, string caption) {
            int messageId =0;

            try {
                //messageId = bot.SendPhotoAsync(chatId: BaseChatId
                //    , streamPhoto
                //    , caption
                //    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                //    , disableNotification: false).Result.MessageId;
            } catch (Exception ex){
                Console.WriteLine(ex.Message);
            }
            return messageId;
        }

        //Редактируем сообщение
        protected async void EditMessage(long idUser, string text) {
            await Task.Run(() => {
                try {
                    bot.EditMessageTextAsync(idUser
                       , authors[idUser].IdEditMessage
                       , text: text
                       , replyMarkup: keyboard
                       , linkPreviewOptions: DisableLinks
                       , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
                    idCurrentUser = idUser;
                } catch { }
            });
        }

        //Редактируем общее сообщение
        protected async void EditMessage(ChatId chatId, int messageId, string eText) {
            await Task.Run(() => {
                try {

                    if (!string.IsNullOrEmpty(eText)) {
                        bot.EditMessageTextAsync(chatId: chatId
                            , messageId: messageId
                            , text: eText
                            , linkPreviewOptions: DisableLinks
                            , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
                    }
                } catch (Exception ex){
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.Data);
                    Console.WriteLine(ex.HResult);
                    Console.WriteLine(ex.StackTrace);
                    Console.WriteLine(ex.Source);
                }
            });
        }

        //Открепляем сообщение
        protected async void UnpinMessage(ChatId chatId, int unpinMessageId) {
            await Task.Run(() => {
                try {
                    bot.UnpinChatMessageAsync(chatId: chatId,
                        messageId: unpinMessageId);
                } catch { }
            });
        }

        //Получаем имя пользователя по уникальному номеру
        protected string GetUserName(long idUser) {
            try {
                return GetCorrectName(bot.GetChatMemberAsync(idUser, idUser).Result.User.FirstName);
            } catch {
                return "<s>Пользователь</s>";
            }
        }

        //Получаем последнее имя пользователя по уникальному номеру
        protected string GetUserLastName(long idUser) {
            string lName = bot.GetChatMemberAsync(idUser, idUser).Result.User.LastName;
            if(!string.IsNullOrEmpty(lName)) {
                return GetCorrectName(lName);
            } else {
                return "<s>Пользователь</s> ";
            }
        }

        protected string GetCorrectName(string name) {
            if (!string.IsNullOrEmpty(name)) {
                if(name.Length > 40) {
                    name = name.Remove(40) + "...";
                }
                return name.Replace("<", "*").Replace(">", "*").Replace("/", "*");
            } else {
                return name;
            }
        }


        //Удаляем сообщение
        protected async void DeleteMessage(int idMessage) {
            try {
                await bot.DeleteMessageAsync(BaseChatId, idMessage);
            } catch { }
        }

        //Удаляем сообщение
        protected async void DeleteMessage(ChatId chatId, int idMessage) {
            try {
                await bot.DeleteMessageAsync(BaseChatId, idMessage);
            } catch { }
        }

        #endregion

        #region DATA BASE

        //Создаём основную таблицу
        private void CreateTableBases(SqliteConnection cnct) {
            
            OpenDatabase();
            string sCommand = @"CREATE TABLE IF NOT EXISTS Bases(
                BaseBotToken TEXT NOT NULL UNIQUE,
                VerificationAppId INTEGER,
                VerificationAppServiceKey TEXT,
                AdministratorId   INTEGER NOT NULL UNIQUE,
                ReserveAdminId   INTEGER,
                EditorId INTEGER NOT NULL,
                BaseChatId   TEXT NOT NULL,
                BaseChatLink    TEXT NOT NULL,
                ArchiveChatLink  TEXT DEFAULT NULL,
                AllowControl   TEXT NOT NULL,
	            WalletNumber    TEXT,
                WalletToken    TEXT,
                WalletTokenDate    TEXT,
                PollDurationMins   INTEGER,
                ArrestDurationMins INTEGER,
                StartTime   TEXT,
                EndTime  TEXT,
                LastSolutionMessageId INTEGER);";

            SqliteCommand command = new SqliteCommand();

            command.Connection = cnct;
            command.CommandText = sCommand;
            command.ExecuteNonQuery();

            CloseDatabase();
        }

        //Сохраняем системные настройки
        private void InitializeAndSaveBaseSettings() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "INSERT INTO Bases(" +
                    "BaseBotToken, VerificationAppId, VerificationAppServiceKey, AdministratorId, ReserveAdminId" +
                    ", EditorId, BaseChatId, BaseChatLink, ArchiveChatLink, AllowControl, WalletNumber"+
                    ", WalletToken, WalletTokenDate, PollDurationMins, ArrestDurationMins, StartTime, EndTime"+
                    ", LastSolutionMessageId" +
                    ") VALUES (" +
                    "@baseBotToken, @verificationAppId" +
                    ", @verificationAppServiceKey, @administratorId, @reserveAdminId" +
                    ", @editorId, @baseChatId, @baseChatLink, @archiveChatLink, @allowControl"+
                    ", @walletNumber, @walletToken, @walletTokenDate" +
                    ", @pollDurationMins, @arrestDurationMins, @startTime, @endTime, @lastSolutionMessageId);";



                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@administratorId", AdministratorId);
                command.Parameters.AddWithValue("@reserveAdminId", ReserveAdminId);
                command.Parameters.AddWithValue("@baseBotToken", BaseBotToken);
                command.Parameters.AddWithValue("@verificationAppId", VerificationAppId);
                command.Parameters.AddWithValue("@verificationAppServiceKey", DBNull.Value);

                command.Parameters.AddWithValue("@editorId", editor.Id);
                command.Parameters.AddWithValue("@baseChatId", BaseChatId.ToString());
                command.Parameters.AddWithValue("@baseChatLink", BaseChatLink);

                command.Parameters.AddWithValue("@archiveChatLink", DBNull.Value);
                command.Parameters.AddWithValue("@allowControl", AllowControl.ToString());
                command.Parameters.AddWithValue("@walletNumber", WalletNumber);
                command.Parameters.AddWithValue("@walletToken", WalletToken);
                command.Parameters.AddWithValue("@walletTokenDate", WalletTokenDate.ToString());
                command.Parameters.AddWithValue("@pollDurationMins", 1);

                command.Parameters.AddWithValue("@arrestDurationMins", 1);

                command.Parameters.AddWithValue("@startTime", "06:35");
                command.Parameters.AddWithValue("@endTime", "22:35");

                command.Parameters.AddWithValue("@lastSolutionMessageId", 0); 

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }



        //Загружаем базовые настройки
        private void LoadBaseSettings() {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Bases";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {
                        AdministratorId = long.Parse(dataReader["AdministratorId"].ToString());
                        ReserveAdminId = long.Parse(dataReader["ReserveAdminId"].ToString());

                        BaseBotToken = dataReader["BaseBotToken"].ToString();
                        
                        VerificationAppId = int.Parse(dataReader["VerificationAppId"].ToString());
                        VerificationAppServiceKey = dataReader["VerificationAppServiceKey"].ToString();

                        BaseChatId = long.Parse(dataReader["BaseChatId"].ToString());
                        BaseChatLink = dataReader["BaseChatLink"].ToString();
                        BaseChatLink = dataReader["BaseChatLink"].ToString();
                        
                        ArchiveChatLink = dataReader["ArchiveChatLink"].ToString();
                        AllowControl = bool.Parse(dataReader["AllowControl"].ToString());
                        
                        WalletNumber = dataReader["WalletNumber"].ToString();
                        WalletToken = dataReader["WalletToken"].ToString();
                        WalletTokenDate = DateTime.Parse(dataReader["WalletTokenDate"].ToString());

                        editor.Id = long.Parse(dataReader["EditorId"].ToString());
                        IsBaseEmpty = false;
                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        //Сохраняем уникальный номер основного чата
        private bool SaveBaseChatId(long idChat) {

            if(idChat != 0) {
                BaseChatId = idChat;
                lock(dbLocker) {
                    OpenDatabase();

                    string sCommand = "UPDATE Bases SET BaseChatId =@baseChatId, EditorId =@editorId";

                    SqliteCommand command = new SqliteCommand();
                    command.CommandText = sCommand;
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@baseChatId", BaseChatId);
                    command.Parameters.AddWithValue("@editorId", editor.Id);

                    command.ExecuteNonQuery();
                    CloseDatabase();
                    return true;
                }
            } else {
                return false;
            }
        }


        //Сохраняем уникальный номер архивного чата
        private bool SaveBaseArvhiveLink( string linkChat) {
            if(string.IsNullOrEmpty(linkChat))
                return false;

            ArchiveChatLink = linkChat;
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Bases SET ArchiveChatLink =@archiveChatLink, EditorId =@editorId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@archiveChatLink", string.IsNullOrEmpty(ArchiveChatLink) ? DBNull.Value : ArchiveChatLink);
                command.Parameters.AddWithValue("@editorId", editor.Id);

                command.ExecuteNonQuery();
                CloseDatabase();
                return true;
            }
        }

        //Сохраняем уникальный номер основного чата
        private bool SaveBaseChatLink(string link) {

            if(!string.IsNullOrEmpty(link)) {

                BaseChatLink = link;
                lock(dbLocker) {
                    OpenDatabase();

                    string sCommand = "UPDATE Bases SET BaseChatLink =@baseChatLink" +
                        ", EditorId =@editorId";

                    SqliteCommand command = new SqliteCommand();
                    command.CommandText = sCommand;
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@baseChatLink", link);
                    command.Parameters.AddWithValue("@editorId", editor.Id);

                    command.ExecuteNonQuery();
                    CloseDatabase();
                    return true;
                }
            } else {
                return false;
            }
        }


        //Сохраняем номер и токен Qiwi-кошелька
        private void SaveBaseWalletNumberAndToken() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Bases SET WalletNumber =@walletNumber, WalletToken =@walletToken" +
                    ", WalletTokenDate =@walletTokenDate, EditorId =@editorId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@walletNumber", WalletNumber);
                command.Parameters.AddWithValue("@walletToken", WalletToken);
                command.Parameters.AddWithValue("@walletTokenDate", WalletTokenDate);
                command.Parameters.AddWithValue("@editorId", editor.Id);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Проверяем есть ли у пользователя права администратора
        protected bool HasAdminRights(long idPerson) {
            lock (dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Persons WHERE PersonId =@personId AND Status ='Promote'";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@personId", idPerson);

                dataReader = command.ExecuteReader();

                bool hasAccess;
                if (dataReader.HasRows) {
                    hasAccess = true;
                } else {
                    hasAccess = false;
                }
                dataReader.Close();
                CloseDatabase();
                return hasAccess;
            }
        }

        //Устанавливаем разрешение управления для представителей союза
        private void SaveAllowControl() {

            lock(dbLocker) {
                OpenDatabase();
                string sCommand = "UPDATE Bases SET AllowControl =@allowControl";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@allowControl", AllowControl.ToString());

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Устанавливаем разрешение управления для представителей союза
        private void SaveVerificationIdAndKey() {

            lock (dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Bases SET VerificationAppId =@verificationAppId, VerificationAppServiceKey=@verificationAppServiceKey";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@verificationAppId", VerificationAppId);
                command.Parameters.AddWithValue("@verificationAppServiceKey", VerificationAppServiceKey);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Устанавливаем разрешение управления для представителей союза
        private void SaveReserveAdminId() {

            lock (dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Bases SET ReserveAdminId =@reserveAdminId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@reserveAdminId", ReserveAdminId);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        #endregion




        //Пишем логи при ошибке платежа
        public void WriteLogs(string log = null) {
            try {
                string path = "./LOGS";
                using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default)) {
                    sw.WriteLine("\n[" + DateTime.Now.ToString("dd/MMMM/yyyy HH:mm") + "]");
                    sw.Write(log);

                }

            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
