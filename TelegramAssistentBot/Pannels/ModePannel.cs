﻿//Модуль для создания режимных обращений по поводу графиков и голосований
//

using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.AppealsModels;

namespace TelegramAssistentBot.Pannels {
    public class ModePannel : UnionPanel {

        #region INITIALIZE

        int maxPollDurationMins;
        const int maxArrestDurationMins =480; //Число минут в сутках

        Dictionary<long, ModeAppeal> modeAppeals = new Dictionary<long, ModeAppeal>();

        //Создаём экземпляр панели режима
        public ModePannel() :base() {

            OnVotingIsLoaded += ModePannel_OnVotingIsLoaded;
            OnVotingIsStarted += ModePannel_OnVotingIsStarted;
            OnVotingIsStopped += ModePannel_OnVotingIsStopped;
            OnVotingIsFinished += ModePannel_OnVotingIsFinished;

            maxPollDurationMins = (int)(EndTime.TimeOfDay - StartTime.TimeOfDay).TotalMinutes / 10;
        }
        #endregion

        #region MODE VOTING

        //Выполняем как только голосование запускается
        private void ModePannel_OnVotingIsStarted(object sender, EventArgs e) {

            long idAuthor = voting.AuthorId;
            if(modeAppeals.ContainsKey(idAuthor)) {
                SaveModeVoting(idAuthor
                    , modeAppeals[idAuthor].Type
                    , modeAppeals[idAuthor].PollDurationMins
                    , modeAppeals[idAuthor].ArrestDurationMins
                    , modeAppeals[idAuthor].StartTime
                    , modeAppeals[idAuthor].EndTime);
            }
        }

        //Выполняем при загрузке действующего голосования из базы данных
        private void ModePannel_OnVotingIsLoaded(object sender, EventArgs e) {
            LoadModeVoting(voting.AuthorId);
        }

        //Останавливаем голосование
        public void StopModeAppeal(CallbackQuery callback) {
            StopUnionAppeal(callback);
        }

        //Выполняем при остановке режимного голосования
        private void ModePannel_OnVotingIsStopped(object sender, VotingEventArgs e) {
            if(modeAppeals.ContainsKey(e.AuthorId)) {
                long idAuthor = voting.AuthorId;
                StopSolutionText(e.StoperId);

                modeAppeals.Remove(idAuthor);
                DeleteMessage(voting.PollMessageId);
                ResetVoting();
            }
        }

        

        //Выполняем при завершении голосования
        private void ModePannel_OnVotingIsFinished(object sender, VotingEventArgs e) {

            if(modeAppeals.ContainsKey(e.AuthorId)) {
                long idAuthor = voting.AuthorId;

                if(e.IsApproved) {
                    Confirmation conf = AcceptSolutionText(voting.AppealMessageId);
                    SaveModeSolution(modeAppeals[idAuthor].Type
                        , modeAppeals[idAuthor].PollDurationMins
                        , modeAppeals[idAuthor].ArrestDurationMins

                        , modeAppeals[idAuthor].StartTime
                        , modeAppeals[idAuthor].EndTime
                        , conf.LastSolutionMessageId);

                    LoadModeSettings();
                    SignatureConfirmation(conf);
                } else {
                    RejectSolutionText();
                }

                modeAppeals.Remove(idAuthor);
                ResetVoting();
            }
        }



        //Сбрасываем обращение режима
        protected void ResetModeAppeal(long idUser) {
            if((voting == null) || (voting.AuthorId != idUser)) {
                if(modeAppeals.ContainsKey(idUser)) {

                    modeAppeals.Remove(idUser);
                    authors[idUser].IdEditMessage = 0;
                    ResetAppeal(idUser);
                } else {
                    ResetUnionAppeal(idUser);
                }
            }
        }



        #endregion

        #region SHOW AND CREATE MODE APPEAL

        //Показываем страницы режима
        protected void ShowModePages(CallbackQuery callback, string cancelCallback) {
            if(callback.Data == "ShowModePageCallback") {
                ResetModeAppeal(callback.From.Id);
                ShowModePage(callback.From.Id, cancelCallback);
            } else {
                ShowUnionPages(callback, cancelCallback);
            }
        }

        

        //Показываем страницу режима
        private void ShowModePage(long idUser, string cancelCallback) {

            string donationUrl = GetDonateUrl(idUser);
            string text = $"{GetVotingText()}" +
                "\n\n<b>Пояснения:</b>" +
                $"\n🕰  Приём с {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}" +
                $"\n⏳  Голосования по {PollDurationMins} минут(ы/е)" +
                $"\n🪖  Арест {ArrestDurationMins} мин." +
                $"\n\n<a href =\"{donationUrl}\">🎁  Поддержать систему</a>" +
                $"\n<i>Союз ведёт <a href=\"tg://user?id={editor.Id}\">ID {editor.Id}</a></i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Голосования", "CreatePollDurationCallback"),
                    InlineKeyboardButton.WithCallbackData("Приём", "CreatePollScheduleCallback")
                },


                new []{
                    InlineKeyboardButton.WithCallbackData("Назад", cancelCallback ),
                    InlineKeyboardButton.WithCallbackData("Арест", "CreateArrestDurationCallback") }
            });

            if(authors[idUser].IdEditMessage != 0) {
                EditMessage(idUser, text);
            } else {
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Создаём режимное обращение
        protected void CreateModeAppeal(long idUser, Message message, string cancelCallback) {

            if(authors[idUser].CallbackData == "CreatePollDurationCallback"
                || authors[idUser].CallbackData == "CreatePollScheduleCallback"
                || authors[idUser].CallbackData == "CreateArrestDurationCallback") {

                PrepareModeAppeal(idUser);
                switch (authors[idUser].CallbackData) {
                    case "CreatePollDurationCallback":

                        CreatePollDuration(idUser, message, cancelCallback);
                        break;

                    case "CreatePollScheduleCallback":
                        CreatePollSchedule(idUser, message, cancelCallback);
                        break;

                    case "CreateArrestDurationCallback":
                        CreateArrestDuration(idUser, message, cancelCallback);
                        break;
                }
            } else {
                CreateUnionAppeal(idUser, message, cancelCallback);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="message"></param>
        /// <param name="cancelCallback"></param>
        private void CreateArrestDuration(long idUser, Message message, string cancelCallback) {
            if (modeAppeals[idUser].ArrestDurationMins == 0) {
                SetDurationTime(idUser, message, "ShowModePageCallback");
            } else {
                CreateAppeal(idUser, message, modeAppeals[idUser], "ShowModePageCallback");
            }
        }

        private void CreatePollSchedule(long idUser, Message message, string cancelCallback) {
            if (authors[idUser].CallbackData == "CreatePollScheduleCallback") {
                if (modeAppeals[idUser].StartTime == DateTime.MinValue
                    || modeAppeals[idUser].EndTime == DateTime.MinValue) {

                    SetScheduleTime(idUser, message, "ShowModePageCallback");
                } else {
                    CreateAppeal(idUser, message, modeAppeals[idUser], "ShowModePageCallback");
                }
            }
        }

        private void CreatePollDuration(long idUser, Message message, string cancelCallback) {
            if (modeAppeals[idUser].PollDurationMins == 0) {
                SetDurationTime(idUser, message, "ShowModePageCallback");
            } else {
                CreateAppeal(idUser, message, modeAppeals[idUser], "ShowModePageCallback");
            }
        }

        //Подготавливам режимное обращение 
        private void PrepareModeAppeal(long idUser) {
            if(!modeAppeals.ContainsKey(idUser)) {
                ModeAppeal ord = new ModeAppeal();
                ord.AuthorId = authors[idUser].Id;

                modeAppeals.Add(idUser, ord);
                SetModeAppealType(idUser);
            }
        }


        //Устанавливаем тип режимного обращения
        private void SetModeAppealType(long idUser) {
            switch(authors[idUser].CallbackData) {

                case "CreatePollDurationCallback":
                    modeAppeals[idUser].Type = "PollDuration";
                    break;

                case "CreatePollScheduleCallback":
                    modeAppeals[idUser].Type = "PollSchedule";
                    break;

                case "CreateArrestDurationCallback":
                    modeAppeals[idUser].Type = "ArrestDuration";
                    break;
            }
        }

        //Устанавиливаем время опроса
        private void SetDurationTime(long idUser, Message message, string cancelCallback) {
            if(message != null
                && !string.IsNullOrEmpty(message.Text)) {

                int time;
                if (int.TryParse(message.Text, out time) && time > 0) {

                    if ((authors[idUser].CallbackData.Contains("Poll") && (time < maxPollDurationMins))
                        || (authors[idUser].CallbackData.Contains("Arrest") && (time < maxArrestDurationMins)
                        && authors[idUser].CallbackData.Contains("Arrest") && (time > maxArrestDurationMins /2))) {

                        switch (authors[idUser].CallbackData) {
                            case "CreatePollDurationCallback":
                                modeAppeals[idUser].PollDurationMins = time;
                                break;

                            case "CreateArrestDurationCallback":
                                    modeAppeals[idUser].ArrestDurationMins = time;
                                break;
                        }

                        if (string.IsNullOrEmpty(modeAppeals[idUser].Title)) {
                            SetModeAppealTitleAndServiceInfo(idUser);
                        }
                        CreateModeAppeal(idUser, null, cancelCallback);
                    } else {
                        ShowDurationPage(idUser, cancelCallback);
                    }
                }else {
                    ShowDurationPage(idUser, cancelCallback);
                }
            } else {
                ShowDurationPage(idUser, cancelCallback);
            }
        }

        //Устанавливаем заголовок режимного обращения
        private void SetModeAppealTitleAndServiceInfo(long idUser) {

            switch(authors[idUser].CallbackData) {

                case "CreatePollDurationCallback":

                    modeAppeals[idUser].Title = "Установить период голосования" +
                        $" в <b>{modeAppeals[idUser].PollDurationMins}</b> минут(ы/у).";

                    modeAppeals[idUser].ServiceInformation = $"<i>Чтобы изменить период создайте обращение через" +
                        $" <a href=\"tg://user?id={bot.BotId}\">помощника</a></i>." +
                        $"\n<i>Голосования по {PollDurationMins} минут(е/ы).</i>";

                    break;

                case "CreatePollScheduleCallback":

                    modeAppeals[idUser].Title = "Установить график приёма обращений" +
                        $" <b>с {modeAppeals[idUser].StartTime.ToString("HH:mm")}" +
                        $" до {modeAppeals[idUser].EndTime.ToString("HH:mm")}</b>.";

                    modeAppeals[idUser].ServiceInformation = $"<i>Чтобы изменить расписание создайте обращение через" +
                        $" <a href=\"tg://user?id={bot.BotId}\">помощника</a></i>." +
                        $"\n<i>Голосования по {PollDurationMins} минут(е/ы).</i>";

                    break;

                case "CreateArrestDurationCallback":

                    modeAppeals[idUser].Title = "Установить период <b>ареста</b>" +
                            $" в <b>{modeAppeals[idUser].ArrestDurationMins}</b> минут(ы/у)";

                    modeAppeals[idUser].ServiceInformation = $"<i>Чтобы изменить периоды арестов создайте обращение через" +
                        $" <a href=\"tg://user?id={bot.BotId}\">помощника</a></i>." +
                        $"\n<i>Голосования по {PollDurationMins} минут(е/ы).</i>";

                    break;
            }
        }

        //Показываем страницу времени опроса
        private void ShowDurationPage(long idUser, string cancelCallback) {

            Appeal appeal = modeAppeals[idUser];
            string text = $"<b>ОБРАЩЕНИЕ</b>\nОт: {GetUserName(appeal.AuthorId)}" +
                    $" <i><a href=\"tg://user?id={appeal.AuthorId}\">[ID {appeal.AuthorId}]</a></i>" +
                    $"\n\n<pre>🎯  Цель обращения:</pre>";

            maxPollDurationMins = (int)(EndTime.TimeOfDay - StartTime.TimeOfDay).TotalMinutes / 10;
            text += GetDurationAppealText(idUser);

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        private string GetDurationAppealText(long idUser) {
            switch (authors[idUser].CallbackData) {

                case "CreatePollDurationCallback":
                    return $"\n*Пришлите продолжительность " +
                        $"опроса от 1 до " +
                        $"{maxPollDurationMins} минут(ы).\n<i>Пример: 61</i>";

                case "CreateArrestDurationCallback":
                    if (modeAppeals[idUser].ArrestDurationMins == 0) {
                        return $"\n*Пришлите продолжительность " +
                            $"<b>ареста</b> от {maxArrestDurationMins /2} до " +
                            $"{maxArrestDurationMins} минут(ы).\n<i>Пример: 61</i>";
                    } else {
                        return $"\n*Пришлите продолжительность " +
                            $"<u>отстранения</u> от {maxArrestDurationMins /2} до " +
                            $"{maxArrestDurationMins} минут(ы).\n<i>Пример: 61</i>";
                    }
                default:
                    return null;
            }
        }

        //Устанавливаем расписание приёма заявок
        private void SetScheduleTime(long idUser, Message message, string cancelCallback) {
            
            if(message != null
            && !string.IsNullOrEmpty(message.Text)
            && DateTime.TryParse(message.Text, out DateTime time)) {

                if(modeAppeals[idUser].StartTime == DateTime.MinValue) {
                    modeAppeals[idUser].StartTime = time;
                } else {
                    if(modeAppeals[idUser].StartTime.AddMinutes(PollDurationMins *10).TimeOfDay < time.TimeOfDay
                        && modeAppeals[idUser].StartTime.TimeOfDay <time.TimeOfDay) {

                        modeAppeals[idUser].EndTime = time;
                        SetModeAppealTitleAndServiceInfo(idUser);
                    } 
                }
                CreateModeAppeal(idUser, null, cancelCallback);
            } else {
                if(modeAppeals[idUser].StartTime == DateTime.MinValue) {
                    ShowScheduleTimePage(idUser, true, cancelCallback);
                } else {
                    ShowScheduleTimePage(idUser, false, cancelCallback);
                }
            }
        }

        //Показываем страницу расписания приёма
        private void ShowScheduleTimePage(long idUser, bool isStartTime, string cancelCallback) {

            Appeal appeal = modeAppeals[idUser];
            string text = $"<b>ОБРАЩЕНИЕ</b>\nОт: {GetUserName(appeal.AuthorId)}" +
                $" <i><a href=\"tg://user?id={appeal.AuthorId}\">[ID {appeal.AuthorId}]</a></i>" +
                $"\n\n<pre>🎯  Цель обращения:</pre>";

            if(isStartTime) {
                text += "\n*Пришлите время <b>начала</b> приёма." +
                    "\n<i>Пример: 08:35</i>";

            } else {
                text += $"\n*Пришлите время <b>окончания</b> приёма" +
                    $" позже {modeAppeals[idUser].StartTime.AddMinutes(PollDurationMins *10).ToString("HH:mm")}." +
                    "\n<i>Пример: 20:35</i>";
            }

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        #endregion

        #region DATABASE


        //Сохраняем отчёт об обращении
        private void SaveModeVoting(long authorId, string type, int pollDurationMins,
            int arrestDurationMins, DateTime startTime, DateTime endTime) {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Votings SET Type =@type" +
                    ", SetPollDurationMins =@setPollDurationMins" +
                    ", SetArrestDurationMins =@setArrestDurationMins" +
                    ", SetStartTime =@setStartTime, SetEndTime =@setEndTime" +
                    " WHERE AuthorId =@authorId";


                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@authorId", authorId);
                command.Parameters.AddWithValue("@type", type);
                command.Parameters.AddWithValue("@setPollDurationMins", pollDurationMins);
                command.Parameters.AddWithValue("@setArrestDurationMins", arrestDurationMins);
                command.Parameters.AddWithValue("@setStartTime", startTime);
                command.Parameters.AddWithValue("@setEndTime", endTime);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Загружаем данные из базы
        private void LoadModeVoting(long authorId) {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Votings WHERE AuthorId =@authorId" +
                    " AND (Type ='PollDuration' OR Type ='PollSchedule' OR Type = 'ArrestDuration')";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@authorId", authorId);

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {

                        ModeAppeal modeAppeal = new ModeAppeal();

                        modeAppeal.AuthorId = long.Parse(dataReader["AuthorId"].ToString());
                        modeAppeal.Type = dataReader["Type"].ToString();

                        switch (modeAppeal.Type) {
                            case "PollDuration":
                                modeAppeal.PollDurationMins = int.Parse(dataReader["SetPollDurationMins"].ToString());
                                break;
                            case "PollSchedule":
                                modeAppeal.StartTime = DateTime.Parse(dataReader["SetStartTime"].ToString());
                                modeAppeal.EndTime = DateTime.Parse(dataReader["SetEndTime"].ToString());
                                break;
                            case "ArrestDuration":
                                modeAppeal.ArrestDurationMins = int.Parse(dataReader["SetArrestDurationMins"].ToString());
                                break;
                        }

                        

                        if(!modeAppeals.ContainsKey(modeAppeal.AuthorId)) {
                            modeAppeals.Add(modeAppeal.AuthorId, modeAppeal);
                        }
                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        //Сохраняем отчёт об обращении
        private void SaveModeSolution(string type, int pollDurationMins
            , int arrestDurationMins, DateTime startTime
            , DateTime endTime, int lastSolutionMessageId) {

            lock(dbLocker) {
                OpenDatabase();
                SqliteCommand command = new SqliteCommand();
                
                string sCommand;

                switch (type) {
                    
                    case "PollDuration":
                        sCommand = "UPDATE Bases SET PollDurationMins =@pollDurationMins" +
                            ", LastSolutionMessageId =@lastSolutionMessageId";
                        command.Parameters.AddWithValue("@pollDurationMins", pollDurationMins);
                        break;

                    case "PollSchedule":
                        sCommand = "UPDATE Bases SET StartTime =@startTime, EndTime =@endTime" +
                            ", LastSolutionMessageId =@lastSolutionMessageId";
                        command.Parameters.AddWithValue("@startTime", startTime);
                        command.Parameters.AddWithValue("@endTime", endTime);
                        break;

                    case "ArrestDuration":
                        sCommand = "UPDATE Bases SET ArrestDurationMins =@arrestDurationMins" +
                            ", LastSolutionMessageId =@lastSolutionMessageId";

                        command.Parameters.AddWithValue("@arrestDurationMins", arrestDurationMins);

                        break;

                    default:
                        sCommand = string.Empty;
                        break;
                }


                command.Parameters.AddWithValue("@lastSolutionMessageId", lastSolutionMessageId);
                command.CommandText = sCommand;
                command.Connection = connection;
                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }


        #endregion

    }
}
