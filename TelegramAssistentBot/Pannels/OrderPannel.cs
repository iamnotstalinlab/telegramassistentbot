﻿//Модуль архива. Для быстрого поиска информации в архиве по личности пользователя
//

using System;
using System.Collections.Generic;
using System.Linq;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.Appealsandorders;
using Message = Telegram.Bot.Types.Message;

namespace TelegramAssistentBot.Pannels {
    public class OrderPannel : RemittancePannel {

        #region INITIALIZE

        Dictionary<long, Order> orders = new Dictionary<long, Order>();
        public OrderPannel() : base() {
            OnArrestingIsExecuted += OrderPannel_OnArrestIsExecuted;
            OnReleasingIsExecuted += OrderPannel_OnReleasingIsExecuted;
            OnRemovingIsExecuted += OrderPannel_OnRemoveIsExecuted;
        }

        private void OrderPannel_OnReleasingIsExecuted(object sender, ArrestEventArgs e){
            if(!orders.ContainsKey(e.ActiveUserId))
                return;
            if (!spammers.ContainsKey(e.PassiveUserId)
                && !e.IsAuto) {

                int index = -1;
                string executionText = null;
                
                index = e.Text.IndexOf("<b>ОСВОБОЖДЕНИЕ</b>") + "<b>ОСВОБОЖДЕНИЕ".Length;
                executionText = e.Text.Insert(index, " ВЫПОЛНЕНО");
                
                EditMessage(e.Message.Chat.Id, e.ReportMessageId, executionText);
                bool isTargetMessage = (orders[e.ActiveUserId].TargetMessage !=null)? true :false;

                EditMessageToUser(e.ActiveUserId, $"Освобождение <a href=\"{e.ReportLink}\">выполнено</a>.", null);
                ResetOrder(e.ActiveUserId);
                ForwardReportToUnionChats(e.Message, isTargetMessage);
            }
        }



        //Событие когда выполнен арест
        private void OrderPannel_OnArrestIsExecuted(object sender, ArrestEventArgs e) {
            if (!orders.ContainsKey(e.ActiveUserId))
                return;

            if (spammers.ContainsKey(e.PassiveUserId) || e.IsAuto)
                return;

            int index = -1;
            string executionText = null;
            if (orders[e.ActiveUserId].ArrestEndTime > DateTime.Now) {

                index = e.Text.IndexOf("<b>АРЕСТ</b>") + "<b>АРЕСТ".Length;
                executionText = e.Text.Insert(index, " ВЫПОЛНЕН");
            } else {
                index = e.Text.IndexOf("<b>ОСВОБОЖДЕНИЕ</b>") + "<b>ОСВОБОЖДЕНИЕ".Length;
                executionText = e.Text.Insert(index, " ВЫПОЛНЕНО");
            }

            EditMessage(e.Message.Chat.Id, e.ReportMessageId, executionText);
            bool isTargetMessage = (orders[e.ActiveUserId].TargetMessage !=null)? true :false;

            EditMessageToUser(e.ActiveUserId, $"Арест <a href=\"{e.ReportLink}\">выполнен</a>.", null);
            ResetOrder(e.ActiveUserId);
            ForwardReportToUnionChats(e.Message, isTargetMessage);
        }



        //Событие когда выполнено удаление
        private void OrderPannel_OnRemoveIsExecuted(object sender, ArrestEventArgs e) {
            if(!orders.ContainsKey(e.ActiveUserId))
                return;
            int index;
            string executionText =e.Text;
            string action = string.Empty;

            if (spammers.ContainsKey(e.PassiveUserId)) {
                index = executionText.IndexOf("ИЗЪЯТИЕ") + "ИЗЪЯТИЕ".Length;
                action = "Изъятие";
            } else {
                index = executionText.IndexOf("УДАЛЕНИЕ") + "УДАЛЕНИЕ".Length;
                action = "Удаление";
            }
            executionText = executionText.Insert(index, " ВЫПОЛНЕНО");

            EditMessage(e.Message.Chat.Id, e.ReportMessageId, executionText);
            bool isTargetMessage = (orders[e.ActiveUserId].TargetMessage !=null)? true :false;
            
            EditMessageToUser(e.ActiveUserId, $"{action} <a href=\"{e.ReportLink}\">выполнено</a>.", null);

            if (e.IsAuto) {
                CheckAsHumanOrBot(orders[e.ActiveUserId].PassivePerson.Id
                ,orders[e.ActiveUserId].PassivePerson.FirstName, BaseChatId);
            }
            ResetOrder(e.ActiveUserId);
            ForwardReportToUnionChats(e.Message, isTargetMessage);
        }

        //Пересылаем сообщение во все чаты союза, чтобы все видели происходящее в союзе
        private void ForwardReportToUnionChats(Message message, bool isFastOrder){
            for(int i =0; i <unionChats.Count; ++i){
                if(isFastOrder && message.Chat.Id != unionChats.Values.ElementAt(i).Identifier)
                    ForwardMessage(message, unionChats.Values.ElementAt(i).Identifier);
            }
        }

        #endregion

        #region SHOW PAGES

        //Показываем страницы архива
        protected void ShowOrderPages(CallbackQuery callback, string cancelCallback) {

            switch (callback.Data) {
                case "ShowOrderPageCallback":
                    ResetOrder(callback.From.Id);
                    ShowOrderPage(callback.From.Id, cancelCallback);
                    break;
                default:
                    ShowModePages(callback, cancelCallback);
                    break;
            }

        }

        private void ShowMultipleRemovingTempPage(long idUser, string cancelCallback) {
            string text = "<b>МНОЖЕСТВЕННОЕ УДАЛЕНИЕ</b>";

            text += "\n\n Пожалуйста отметьте командой боту в чате по какое сообщение (включительно) следует выполнить удаление" +
                $"" +
                $"\n\n<i>*Схема удаления</i>:" +
                $"\n<b>По</b> это сообщение (и его тоже)." +
                $"\n...Будет удалено..." +
                $"\n...Будет удалено..." +
                $"\n...Будет удалено..." +
                $"\n<a href=\"{GetLinkFromMessage(orders[idUser].TargetMessage.Chat.Id
                ,orders[idUser].TargetMessage.MessageId
                , orders[idUser].TargetMessage.MessageThreadId)}\"><b>От</b> этого сообщения</a> (Образец)." +
                $"\n\n<i>*В указанном диапазоне удаляются сообщения похожего типа (если текст, то и при равной длине) от одного и того же автора c периодом 4 часа от первого сообщения.</i>";

            keyboard = new InlineKeyboardMarkup(new[] { new[]{InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)} });
            EditMessageToUser(idUser, text, keyboard);
        }



        //Добавляем в приказы арест
        protected void AddOrder(long idUser, PersonInfo activePerson, PersonInfo passivePerson, Message targetMessage =null, long idPrisoner =0
            , int arrestReportMessageId =0, int lastRemovedMessageId =0) {

            if (orders.ContainsKey(idUser)) {
                orders.Remove(idUser);
            }

            orders.Add(idUser, new Order());
            orders[idUser].ActivePerson = activePerson;
            orders[idUser].PassivePerson = passivePerson;

            orders[idUser].TargetMessage = targetMessage;
            orders[idUser].ArrestEndTime = GetArrestEndTime();
            orders[idUser].MessageArchiveNumber = GetMessageArchiveNumber(targetMessage);
        }

       

        //Сбрасываем обращения из архива
        protected void ResetOrder(long idUser) {
            if (orders.ContainsKey(idUser)) {
                orders.Remove(idUser);
                if (authors.ContainsKey(idUser)) {
                    authors[idUser].CallbackData = string.Empty;
                    authors[idUser].IdEditMessage = 0;
                }
            } else {
                ResetUnionAppeal(idUser);
            }
        }

        //Показываем страницу архива
        private void ShowOrderPage(long idUser, string cancelCallback) {
            string text = "<b>СЕЙЧАС ПОД АРЕСТОМ:</b>";
            
            if (prisoners != null) {
                for(int i =0; i < prisoners.Count; i++) {
                    
                    text += $"\n{i + 1}) {GetCorrectName(prisoners.ElementAt(i).Value.FirstName)} " +
                        $"[ <code>{prisoners.ElementAt(i).Value.Id}</code> ]" +
                        $" до <a href=\"{prisoners.ElementAt(i).Value.ArrestReportLink}\">" +
                        $"{prisoners.ElementAt(i).Value.ArrestEndDateTime.ToString("HH:mm:ss dd/MM/yyyy")}</a>";

                    if (prisoners.ElementAt(i).Value.Id == idUser) {
                        text += " ⚠️";
                    }
                }
            } else {
                text += "\nНикого нет.";
            }

            text += "\n\n🎭  Краткий отчёт:" +
                "\n\n<b>Для ареста</b>: пришлите ID (уникальный номер) пользователя.";
            keyboard = new InlineKeyboardMarkup(new[] {
                new[]{
                    InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)}
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        #endregion

        #region CREATE REQUEST

        //Создаём запрос в архив
        protected void AcceptOrderCallback(long idUser, Message message, string cancelCallback) {

            if (authors[idUser].CallbackData.Contains("MakeOrder")
            && orders.ContainsKey(idUser)) {

                if (authors[idUser].CallbackData == "MakeOrderVerificationCallback") 
                    PrepareVerification(idUser, message, cancelCallback);
                else {
                    if (authors[idUser].CallbackData == "MakeOrderMultipleRemovingCallback"
                    && orders[idUser] != null
                    && orders[idUser].IdsSelectedMessages == null) 
                        ShowMultipleRemovingTempPage(idUser, cancelCallback);
                    else
                        PrepareOrder(idUser, message, cancelCallback);
                }
                
            } else {
                GetInfo(idUser, message, cancelCallback);
            }
        }


        //Собираем уникальные номера сообщений для множественного удаления
        private void MakeArrayRemovingMessageIds(long idUser, Message lastMessage, string cancelCallback) {
            if (lastMessage.Chat.Id != orders[idUser].TargetMessage.Chat.Id
            || lastMessage.MessageThreadId != orders[idUser].TargetMessage.MessageThreadId) {

                ResetOrder(idUser);
                SendMessage(idUser, "Невозможно удалить сообщения из разных <b>подчатов</b> или <b>чатов</b>. Предлагаю попробовать снова.", null);
                return;
            }

            if(orders[idUser].TargetMessage.MessageId < lastMessage.MessageId) 
                PrepareArrayOfMessagesIds(idUser, orders[idUser].TargetMessage.MessageId, lastMessage.MessageId, cancelCallback);
            else 
                PrepareArrayOfMessagesIds(idUser, lastMessage.MessageId, orders[idUser].TargetMessage.MessageId, cancelCallback);
        }

        private async void PrepareArrayOfMessagesIds(long idUser, int firstId, int lastId, string cancelCallback) {
            Message msg =null;
            int count =0;
            for (int i = firstId; i <= lastId; ++i) { // -1 чтобы последнее указанное тоже мы замечали
                ++count;
                try {
                    msg = await bot.ForwardMessage(idUser, orders[idUser].TargetMessage.Chat.Id, i);
                    // msg =await bot.SendMessage(idUser
                    // , "Text", replyParameters: new ReplyParameters{ ChatId =orders[idUser].TargetMessage.Chat.Id, MessageId =i});
                    }catch { msg = null; }

                if (msg == null) continue;   //Если сообщения с таким ID не существует (уже удалено), то переходим к следующему сообщения (следующей повтору цикла)

                if (!IsValidForMultipleRemoving(idUser, ref msg))
                    await bot.DeleteMessageAsync(idUser, msg.MessageId);
                else {
                    if (orders[idUser].IdsSelectedMessages == null)
                        orders[idUser].IdsSelectedMessages = new List<int>();

                    if (orders[idUser].SelectedMessagesArchiveNumbers == null)
                        orders[idUser].SelectedMessagesArchiveNumbers = new List<string>();

                    orders[idUser].IdsSelectedMessages.Add(i);
                    orders[idUser].SelectedMessagesArchiveNumbers.Add(GetForwardedMessageArchiveNumber(i
                        , orders[idUser].TargetMessage.MessageThreadId
                        , msg.ForwardDate));
                }

            }
            PrepareOrder(idUser, null, cancelCallback);
        }


 //Проверяем подходит ли сообщения для множественного удаления
        private bool IsValidForMultipleRemoving(long idActiveUser, ref Message message) {
            
            // if(message.ForwardFrom !=null){
            //     if (message.ForwardFrom.Id != orders[idActiveUser].TargetMessage.From.Id
            //     && orders[idActiveUser].TargetMessage.ForwardFrom ==null)
            //         return false;

            //     if (message.ForwardFrom.Id != orders[idActiveUser].TargetMessage.ForwardFrom.Id)
            //         return false;
            // }
            // if (message.ForwardSenderName != null
            // && message.ForwardSenderName != $"{orders[idActiveUser].TargetMessage.From.FirstName} {orders[idActiveUser].TargetMessage.From.LastName}")
            //     return false;

            if (message.Type != orders[idActiveUser].TargetMessage.Type)
                return false;

            if (orders[idActiveUser].TargetMessage.Type == Telegram.Bot.Types.Enums.MessageType.Text) {
                if (message.Text.Length != orders[idActiveUser].TargetMessage.Text.Length)
                    return false;
            }

            return true;
        }


        // //Проверяем подходит ли сообщения для множественного удаления
        // private bool IsValidForMultipleRemoving2(long idActiveUser, ref Message message){
        //     if(message.ReplyToMessage ==null)
        //         return false;
            

        //     if(message.ReplyToMessage.Chat.Id != orders[idActiveUser].TargetMessage.Chat.Id)
        //         return false;

        //     if(message.ReplyToMessage.From.Id != orders[idActiveUser].TargetMessage.From.Id)
        //         return false;

        //     if(orders[idActiveUser].TargetMessage.ForwardFrom !=null 
        //         && message.ReplyToMessage.ForwardFrom !=null)
        //         return true;

        //     if(message.ReplyToMessage.Type != orders[idActiveUser].TargetMessage.Type)
        //         return false;

        //     if (orders[idActiveUser].TargetMessage.Type == Telegram.Bot.Types.Enums.MessageType.Text) {
        //         if (message.Text.Length != orders[idActiveUser].TargetMessage.Text.Length)
        //             return false;
        //     }
        //     return true;
        // }

        //Выполнить быстрое распоряжение
        protected async void MakeFastOrder(long idUser, Message message, string cancelCallback) {
            if (message.ReplyToMessage == null)
                return;

            Message repMessage = message.ReplyToMessage;
            if (authors[idUser].CallbackData == "MakeOrderMultipleRemovingCallback") {
                MakeArrayRemovingMessageIds(idUser, repMessage, cancelCallback);
                return;
            }

            string header = repMessage.Text;
            if (!string.IsNullOrEmpty(header)) {
                if(header.Length > 59) {
                    header = header.Remove(60) + "...";
                }
            }
            Dictionary<String, PersonInfo> dictPersonsInfo = GetPersonsFromUsers(message.From, message.ReplyToMessage.From);
            AddOrder(idUser, dictPersonsInfo["ActivePerson"], dictPersonsInfo["PassivePerson"], repMessage);

            string text = $"<code>{orders[idUser].MessageArchiveNumber}</code>" +
                $"\n<i>{GetBaseLinkFromMessageId(repMessage.MessageId)}</i>" +
                $"\n\n<a href=\"tg://user?id={repMessage.From.Id}\"" +
                $">Прислал(а):</a> {GetCorrectName(repMessage.From.FirstName)} [<code>{repMessage.From.Id}</code>" +
                $"{BuildPersonsReportLinks(dictPersonsInfo["PassivePerson"], true, true, true)}]";

            keyboard = null;
            if(dictPersonsInfo["PassivePerson"].Verification != null) {
                if(dictPersonsInfo["PassivePerson"].Verification.VK_Id != -2) {

                    if (dictPersonsInfo["ActivePerson"].Verification.VK_Id > -1) {
                        if (dictPersonsInfo["ActivePerson"].Verification.VK_Id > 0
                        || (dictPersonsInfo["ActivePerson"].Verification.VK_Id ==0 && dictPersonsInfo["PassivePerson"].Verification.VK_Id <= 0)) {

                            keyboard = GetOrderKeyboardMarkup(dictPersonsInfo["ActivePerson"], dictPersonsInfo["PassivePerson"], cancelCallback, true);
                        } else {
                            text += $"\n\n<i>*Для выполенения действий с подтверждённым профилями, следует удостоверить вашу учётную запись." +
                                $" Для решения вопроса, прошу обратиться в <a href=\"{BaseChatLink}\">{BaseChatTittle}</a>.</i>";
                        }
                    } else {
                        text += $"\n\n<i>*Доступ к системе для вас приостановлен, так как профиль подозревается во внешнем управлении. Для решения вопроса, прошу обратиться в " +
                            $"<a href=\"{BaseChatLink}\">{BaseChatTittle}</a>.</i>";
                    }
                } else {
                    LongArrestPerson(dictPersonsInfo["PassivePerson"].Id, BaseChatId);
                    text += $"\n\n<i>*Выбранный пользователь находится в бессрочном аресте. Прошу обратиться в " +
                            $"<a href=\"{BaseChatLink}\">{BaseChatTittle}</a>.</i>";
                }
            }

            int forwMessageId = ForwardMessage(repMessage, idUser);
            EditMessageToUser(idUser, text, keyboard);
        }


        //Подготавливаем правоохранительный запрос
        protected void PrepareOrder(long idUser, Message message, string cancelCallback) {

            if (message == null) {  //Если сообщение пустое, значит это callback
                if (orders.ContainsKey(idUser)) {
                    if (string.IsNullOrEmpty(orders[idUser].Description)) {
                        GetText(idUser, message, cancelCallback);
                    } else {
                        MakeOrderNow(idUser);
                    }
                }
            } else {
                GetText(idUser, message, cancelCallback);
            }
        }

        //Выполнить приказ прямо сейчас.
        //Вынес в отдельную функцию, так как предположил, что чаще всего будет исполняться арест, затем - освобождение и после - удаление.
        private void MakeOrderNow(long idUser) {

            switch (authors[idUser].CallbackData) {

                 case "MakeOrderArrestCallback":
                    MakeArrest(idUser);
                    break;

                case "MakeOrderReleaseCallback":
                    MakeRelease(idUser);
                    break;

                case "MakeOrderRemovingCallback":
                    MakeRemoving(idUser);
                    break;

                case "MakeOrderMultipleRemovingCallback":
                    MakeMultipleRemoving(idUser);
                    break;

                case "MakeOrderConfiscationCallback":
                    MakeConfiscate(idUser);
                    break;
            }
        }


        //Сделать арест до окончания голосования
        private void MakeArrest(long idUser) {
            ArrestPerson(orders[idUser], BuildReportText(idUser));
        }

        //Сделать освобождение до окончания голосования
        private void MakeRelease(long idUser) {
            if (IsDayTime()) {
                orders[idUser].ArrestEndTime = DateTime.Now;
                orders[idUser].PassivePerson.SpamCount = 0;

                ReleasePerson(orders[idUser], BuildReportText(idUser));
            } else {
                SendMessage(idUser, $"Освободить участника можно только c {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}", null);
                ResetOrder(idUser);
            }
        }


        //Сделать удаление
        private void MakeRemoving(long idUser) {
            RemoveMessage(orders[idUser], BuildReportText(idUser));
        }

        private void MakeMultipleRemoving(long idUser) {
            RemoveMultipleMessage(orders[idUser], BuildReportText(idUser));
        }


        //Сделать изъятие
        private void MakeConfiscate(long idUser) {

            orders[idUser].PassivePerson.SpamCount = 1;
            ConfiscateMessage(orders[idUser], BuildReportText(idUser, false)) ;
        }

        //Авто-изъятие сообщения
        public void AutoConfiscateMessage(Message message) {
            if (orders.ContainsKey(message.From.Id))
                return;

            Order autoOrder = CreateAutoOrder(message.From.Id, message.From.FirstName, message, true);

            // orders.Add((long) bot.BotId, autoOrder);
            autoOrder.PassivePerson.SpamCount++;

            string text = $"<b>АВТО-ИЗЪЯТИЕ</b>";
            if(message.Chat.Id != BaseChatId)
                    text +=$"\n<i>Событие  из чата <a href=\"{GetLinkFromMessage(message.Chat.Id
                    ,message.MessageId)}\""+
                    $">«{message.Chat.Title}»</a>.</i>\n";

            text+= $"\n<a href=\"tg://user?id={autoOrder.PassivePerson.Id}" +
                $"\">Арестован(а)*:</a> {GetCorrectName(autoOrder.PassivePerson.FirstName)}" +
                $"[ ID <code>{autoOrder.PassivePerson.Id}</code>{BuildPersonsReportLinks(autoOrder.PassivePerson, true, false, false)}] " +
                $"\n🎯  Цель авто-изъятия:" +
                $"\nПодозрение в авто-рассылке. Для снятия подозрений <a href=\"tg://user?id={bot.BotId}\">напишите мне и решите задачку</a>." +
                $"\n\n💼  Служебная информация:" +
                $"\n<a href=\"tg://user?id={autoOrder.ActivePerson.Id}\">Выполнил бот:</a>" +
                $" {GetUserName((long) bot.BotId)} [ <code>{autoOrder.ActivePerson.Id}</code>" +
                $" {BuildPersonsReportLinks(autoOrder.ActivePerson, false, true, false)}]"+
                $"\n<code>{autoOrder.MessageArchiveNumber}</code> [<a href=\"{ArchiveChatLink}\">Архив</a>]" +
                $"\n*Арест в чатах союза до проверки и {autoOrder.ArrestEndTime.ToString("HH:mm:ss")} +/- 1 мин." +
                $" {autoOrder.ArrestEndTime.ToString("dd/MM/yyyy")}";

                    

            ConfiscateMessage(autoOrder, text, true);
        }


        //Получаем текст ареста
        private void GetText(long idUser, Telegram.Bot.Types.Message message, string cancelCallback) {
            switch (authors[idUser].CallbackData) {

                case "MakeOrderArrestCallback":
                    orders[idUser].ServiceText = BuildServiceText(idUser, true, false, false);
                    GetReportText(idUser, message, cancelCallback);
                    break;

                case "MakeOrderReleaseCallback":
                    orders[idUser].ServiceText = BuildServiceText(idUser, true, false, false);
                    GetReportText(idUser, message, cancelCallback);
                    break;

                case "MakeOrderRemovingCallback":
                    orders[idUser].ServiceText = BuildServiceText(idUser, false, true, false);
                    GetReportText(idUser, message, cancelCallback);
                    break;

                case "MakeOrderMultipleRemovingCallback":
                    orders[idUser].ServiceText = BuildServiceText(idUser, false, true, false);
                    GetReportText(idUser, message, cancelCallback);
                    break;

                case "MakeOrderConfiscationCallback":
                    orders[idUser].ServiceText = BuildServiceText(idUser, true, false, false);
                    GetReportText(idUser, message, cancelCallback);
                    MakeOrderNow(idUser);
                    break;
            }
        }


        //Отправляем текст ареста
        private void GetReportText(long idUser, Telegram.Bot.Types.Message message, string cancelCallback) {

            if (orders.ContainsKey(idUser)) {
                if (message != null) {
                    if (!string.IsNullOrEmpty(message.Text)
                        && string.IsNullOrEmpty(orders[idUser].Description)) {
                        orders[idUser].Description = message.Text;
                    }
                }

                if (authors[idUser].CallbackData == "MakeOrderConfiscationCallback") {
                    orders[idUser].Description = $"Подозрение в авто-рассылке.";
                }

                InlineKeyboardButton cancelButton = InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback);
                if (!(authors[idUser].CallbackData == "MakeOrderRemovingCallback"))
                    keyboard = new InlineKeyboardMarkup(new[] {cancelButton});
                else
                    keyboard = new InlineKeyboardMarkup(new[] {
                        InlineKeyboardButton.WithCallbackData("Удалить много", "MakeOrderMultipleRemovingCallback")
                        , cancelButton});

                if (!string.IsNullOrEmpty(orders[idUser].Description)) {
                    keyboard = new InlineKeyboardMarkup(new[] {new[]{cancelButton, BuildActionButton1(idUser) } });
                } 
                string text = BuildReportText(idUser);
                EditMessageToUser(idUser, text, keyboard);
            }
        }


        //Получаем служебный текст
        private string BuildServiceText(long idUser, bool arrestInfo, bool removingInfo, bool solutionInfo) {

            string serviceText = $"<a href=\"tg://user?id={orders[idUser].ActivePerson.Id}\">Выполнил(а):</a>" +
                    $" {GetUserName(idUser)} [ <code>{orders[idUser].ActivePerson.Id}</code>" +
                    $" {BuildPersonsReportLinks(orders[idUser].ActivePerson, arrestInfo, removingInfo, solutionInfo)}]";

            if (authors[idUser].CallbackData == "MakeOrderArrestCallback"
                || authors[idUser].CallbackData == "MakeOrderConfiscationCallback") {

                DateTime endArrestDateTime = orders[idUser].ArrestEndTime;

                serviceText += $"\n*Арест в чатах союза до " +
                    $"{endArrestDateTime.ToString("HH:mm:ss")} +/- 1 мин." +
                    $" {endArrestDateTime.ToString("dd/MM/yyyy")}";
            }

            if (authors[idUser].CallbackData == "MakeOrderReleaseCallback") {
                serviceText += $"\n<i>*Исполянется во всех чатах союза</i>";
            }

            if (authors[idUser].CallbackData== "MakeOrderRemovingCallback"
            || authors[idUser].CallbackData == "MakeOrderMultipleRemovingCallback") {
                serviceText += $"\n<i>*Удалять можно в течение 48 часов.</i>";
            }

            string curChatArchiveLink =ArchiveChatLink;
            if(orders[idUser].TargetMessage !=null
            && orders[idUser].TargetMessage.Chat.Id != BaseChatId)
                curChatArchiveLink =unionChats[orders[idUser].TargetMessage.Chat.Id].ArchiveChatLink;

            if (authors[idUser].CallbackData == "MakeOrderMultipleRemovingCallback"
            && orders[idUser].IdsSelectedMessages !=null) {
                serviceText += "\n\n" +"Удалено " + orders[idUser].IdsSelectedMessages.Count +" сообщений(ие/ия):";
                for(int i=0; i <orders[idUser].IdsSelectedMessages.Count; ++i) {
                    serviceText += $"\n<code>{orders[idUser].SelectedMessagesArchiveNumbers[i]}</code>"+
                    $" [<a href=\"{curChatArchiveLink}\">Архив</a>]";
                }
            }else
                serviceText += $"\n<code>{orders[idUser].MessageArchiveNumber}</code>"+
                $" [<a href=\"{curChatArchiveLink}\">Архив</a>]";
            return serviceText;
        }

        //Получаем первую кнопку действия
        private InlineKeyboardButton BuildActionButton1(long idUser) {
            if (!string.IsNullOrEmpty(orders[idUser].Description)) {
                switch (authors[idUser].CallbackData) {

                    case "MakeOrderArrestCallback":
                        return InlineKeyboardButton.WithCallbackData("Арестовать", "MakeOrderArrestCallback");

                    case "MakeOrderReleaseCallback":
                        if (IsDayTime()) {
                            return InlineKeyboardButton.WithCallbackData("Освободить", "MakeOrderReleaseCallback");
                        } else {
                            return null;
                        }
                    case "MakeOrderMultipleRemovingCallback":
                        return InlineKeyboardButton.WithCallbackData("Удалить все", "MakeOrderMultipleRemovingCallback");

                    case "MakeOrderRemovingCallback":
                        return InlineKeyboardButton.WithCallbackData("Удалить", "MakeOrderRemovingCallback");
                }
            }
            return null;
        }

        //Формируем текст ареста
        private string BuildReportText(long idUser, bool isExecuted =false) {

            if (orders.ContainsKey(idUser)) {

                string servType = string.Empty;
                string userType = string.Empty;
                string text = string.Empty;

                switch (authors[idUser].CallbackData) {
                    case "MakeOrderArrestCallback":
                        text = $"<b>АРЕСТ</b>";

                        userType = "Арестован(а)*";
                        servType = "ареста";
                        break;

                    case "MakeOrderReleaseCallback":
                        text = $"<b>ОСВОБОЖДЕНИЕ</b>";

                        userType = "Освобождён(ена)";
                        servType = "освобождения";
                        break;

                    case "MakeOrderConfiscationCallback":
                        text = $"<b>ИЗЪЯТИЕ</b>";

                        userType = "Арестован(а)*";
                        servType = "изъятия";
                        break;

                    case "MakeOrderMultipleRemovingCallback":
                        text = $"<b>МНОЖЕСТВЕННОЕ УДАЛЕНИЕ</b>";

                        userType = "Удалено от";
                        servType = $"удаления {orders[idUser].IdsSelectedMessages.Count} сообщений";
                        break;

                    case "MakeOrderRemovingCallback":
                        text = $"<b>УДАЛЕНИЕ</b>";
                        servType = "удаления";
                        break;
                }

                if(orders[idUser].TargetMessage !=null)
                    text +=$"\n<i>Событие из чата <a href=\"{GetLinkFromMessage(orders[idUser].TargetMessage.Chat.Id
                    ,orders[idUser].TargetMessage.MessageId)}\""+
                    $">«{orders[idUser].TargetMessage.Chat.Title}»</a>.</i>\n";

                if (authors[idUser].CallbackData != "MakeOrderRemovingCallback") {
                    text += $"\n<a href=\"tg://user?id={orders[idUser].PassivePerson.Id}" +
                    $"\">{userType}:</a> {GetUserName(orders[idUser].PassivePerson.Id)}" +
                    $" [ <code>{orders[idUser].PassivePerson.Id}</code>{BuildPersonsReportLinks(orders[idUser].PassivePerson, true, false, false)}]";
                }

                text += $"\n🎯  Цель {servType}:";
                if (!string.IsNullOrEmpty(orders[idUser].Description)) {
                    text += $"\n{orders[idUser].Description}" +
                        $"\n\n💼  Служебная информация:\n<i>{orders[idUser].ServiceText}</i>";
                } else {
                    text += $"\n(<i>Опишите цель {servType} текстом до {maxSymbols - text.Length} символ(а/ов)</i>)";
                }

                return text;
            } else {
                return null;
            }
        }

        //Получаем информацию о пользователе из архива
        protected async void GetInfo(long idUser, Message message, string cancelCallback) {
            if(message != null) {
                Dictionary<String, PersonInfo> dictPersonsInfo = await GetPersonsFromMessage(message, true);
                if (dictPersonsInfo != null) {
                    if (dictPersonsInfo.ContainsKey("PassivePerson")) {

                        AddOrder(idUser, dictPersonsInfo["ActivePerson"], dictPersonsInfo["PassivePerson"]);
                        ShowPersonInfoPage(idUser, dictPersonsInfo, cancelCallback);
                    } else {
                        ShowOrderPage(idUser, cancelCallback);
                    }
                } else {
                    ShowOrderPage(idUser, cancelCallback);
                }
            }
        }


        //Показываем архивную информацию о пользователе
        private void ShowPersonInfoPage(long idUser,  Dictionary<String, PersonInfo> dictPersonsInfo, string cancelCallback) {

            string text = "🎭  Краткий отчёт:\n\n";

            if(dictPersonsInfo != null) {
                text += FormPersonInfoText(dictPersonsInfo["PassivePerson"]);
            } else {
                text += FormPersonInfoText(dictPersonsInfo["PassivePerson"]);
            }

            if (DateTime.Now.TimeOfDay < StartTime.TimeOfDay
                || DateTime.Now.TimeOfDay > EndTime.TimeOfDay) {

                text += $"\n\n<i>*Арест <b>на ночь</b> до {orders[idUser].ArrestEndTime.ToString("HH:mm dd/MM/yyyy")}.</i>";
            } else {
                text += $"\n\n<i>*Арест <b>днём</b> на {ArrestDurationMins} минут(у/ы).</i>";
            }

            text += "\n\n<b>Для ареста</b>: пришлите ID (уникальный номер) пользователя." +
                "\n<b>Для удаления</b>: перешлите ссылку на сообщение.";

            if (dictPersonsInfo["ActivePerson"].ArrestEndDateTime !=default) {
                text += $"\nВы сможете арестовывать после {dictPersonsInfo["PassivePerson"].ArrestEndDateTime.ToString("HH:mm dd/MM/yyyy")}";
            }

            if(dictPersonsInfo["PassivePerson"].Verification.VK_Id != -2) {

                    if (dictPersonsInfo["ActivePerson"].Verification.VK_Id > -1) {
                        if (dictPersonsInfo["ActivePerson"].Verification.VK_Id > 0
                            || (dictPersonsInfo["ActivePerson"].Verification.VK_Id ==0 && dictPersonsInfo["PassivePerson"].Verification.VK_Id <= 0)) {

                            keyboard = GetOrderKeyboardMarkup(dictPersonsInfo["ActivePerson"], dictPersonsInfo["PassivePerson"], cancelCallback, false);
                        } else {
                            text += $"\n\n<i>*Для выполенения действий с подтверждённым профилями, следует удостоверить вашу учётную запись." +
                                $" Для решения вопроса, прошу обратиться в <a href=\"{BaseChatLink}\">{BaseChatTittle}</a>.</i>";
                        }
                    } else {
                        text += $"\n\n<i>*Доступ к системе для вас приостановлен, так как профиль подозревается во внешнем управлении. Для решения вопроса, прошу обратиться в " +
                            $"<a href=\"{BaseChatLink}\">{BaseChatTittle}</a>.</i>";
                    }
                } else {
                    LongArrestPerson(dictPersonsInfo["PassivePerson"].Id, BaseChatId);
                    text += $"\n\n<i>*Выбранный пользователь находится в бессрочном аресте. Прошу обратиться в " +
                            $"<a href=\"{BaseChatLink}\">{BaseChatTittle}</a>.</i>";
                }

            EditMessageToUser(idUser, text, keyboard);
        }

        


        //Получить специальный набор кнопок
        InlineKeyboardButton btnCancel;
        InlineKeyboardButton btnArrest = InlineKeyboardButton.WithCallbackData("Арест", "MakeOrderArrestCallback");
        InlineKeyboardButton btnRelease = InlineKeyboardButton.WithCallbackData("Освобождение", "MakeOrderReleaseCallback");
        InlineKeyboardButton btnRemove = InlineKeyboardButton.WithCallbackData("Удаление", "MakeOrderRemovingCallback");
        InlineKeyboardButton btnConfiscate = InlineKeyboardButton.WithCallbackData("Изъять (Авто)", "MakeOrderConfiscationCallback");
        InlineKeyboardButton btnAlias = InlineKeyboardButton.WithCallbackData("Псевдоним", "MakeOrderVerificationCallback");

        private InlineKeyboardMarkup GetOrderKeyboardMarkup(PersonInfo activePerson, PersonInfo passivePerson, string cancelCallback, bool hasTargetMessage =false) {

            InlineKeyboardButton[] firstRow;
            InlineKeyboardButton[] secondRow;
            btnCancel = InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback);

            if(activePerson.Id == passivePerson.Id) {
                if(activePerson.Verification.VK_Id >0) {
                    firstRow = new InlineKeyboardButton[] { btnCancel , btnAlias};
                } else {
                    if(verification !=null){
                        InlineKeyboardButton btnVerify = InlineKeyboardButton.WithUrl("Удостоверение", GetVerificationUrl(activePerson.Id));
                        firstRow = new InlineKeyboardButton[] { btnCancel, btnVerify };
                    }else
                        firstRow = new InlineKeyboardButton[] { btnCancel };
                }
                return new InlineKeyboardMarkup(new[] { firstRow });
            } else {
                
                if (activePerson.ArrestEndDateTime < DateTime.Now) {
                    if (IsDayTime()) {
                        if (DateTime.Now > passivePerson.ArrestEndDateTime) {
                             firstRow = new InlineKeyboardButton[] { btnRemove, btnArrest };
                        } else {
                            firstRow = new InlineKeyboardButton[] { btnRemove, btnRelease };
                        }
                    } else {
                        if (DateTime.Now > passivePerson.ArrestEndDateTime) {
                            firstRow = new InlineKeyboardButton[] { btnRemove, btnArrest };
                        } else {
                            firstRow = new InlineKeyboardButton[] { btnRemove };
                        }
                    }    
                } else {
                    firstRow = new InlineKeyboardButton[] { btnRemove};
                }
            }


            secondRow = new InlineKeyboardButton[] { btnCancel, btnConfiscate };

            if (!hasTargetMessage) {
                firstRow= firstRow.Except(new InlineKeyboardButton[] {btnRemove}).ToArray();
                secondRow =secondRow.Except(new InlineKeyboardButton[] {btnConfiscate}).ToArray();
            }
            return new InlineKeyboardMarkup(new[] { firstRow, secondRow });
        }

        //Проверить день или ночь сейчас для приёма обращений и выполнения освобождений
        private bool IsDayTime() {
            if(DateTime.Now.TimeOfDay > StartTime.TimeOfDay
                && DateTime.Now.TimeOfDay < EndTime.TimeOfDay) {

                return true;
            } else {
                return false;
            }
        }

        protected async void PrepareVerification(long idUser, Message message, string cancelCallback) {
            if(orders != null
                && orders.ContainsKey(idUser)) {

                if (orders[idUser].ActivePerson.Verification.VK_Id > 0) {
                    if(message ==null || message.Text.Length > 15) {
                        ShowAliasPage(idUser, cancelCallback);
                    } else {

                        orders[idUser].ActivePerson.Verification.Alias = message.Text;
                        if (await PromoteAndSetAliasToVerifiedUser(orders[idUser].ActivePerson)) {
                            EditMessageToUser(idUser, $"Разрешите поздравить! Псеводним \"{message.Text}\" для вас установлен.", null);
                            ResetOrder(idUser);
                        } else {
                            EditMessageToUser(idUser, $"К сожалению, такой псеводним использовать нельзя. Давайте попробуем снова.",
                                new InlineKeyboardMarkup (new InlineKeyboardButton[] { InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback) }));
                        }
                    }
                } else {

                }
            }
        }

        protected void ShowAliasPage(long idUser, string cancelCallback) {
            string text = "Пришлите желаемый псевдоним (до 16 символов без использования смайликов/эмоджи).";
            EditMessageToUser(idUser, text
                , new InlineKeyboardMarkup(new InlineKeyboardButton[] { InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback) }));
        }

        #endregion
    }
}
