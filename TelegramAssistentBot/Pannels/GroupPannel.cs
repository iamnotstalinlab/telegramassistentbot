﻿//Модуль для создания союзных обращений и их учёта
//

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Telegram.Bot;
using Telegram.Bot.Types;

using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramAssistentBot.AppealsModels {
    public class GroupPannel :AppealPannel {

        #region INITIALIZE
        Dictionary<long, GroupAppeal> groupAppeals = new Dictionary<long, GroupAppeal>();
        public event EventHandler<GroupEventArgs> OnNewUniteAdded;
        public class GroupEventArgs {
            public ChatId ChatId { get; set; }
            public string ChatType { get; set; }
            public int AppealMessageId { get; set; }
            
        }

        public GroupPannel() :base() {

            CreateTableGroupChats(connection);
            OnVotingIsLoaded += GroupPannel_OnVotingIsLoaded;
            OnVotingIsStarted += GroupPannel_OnAppealVotingIsStarted;
            OnVotingIsStopped += GroupPannel_OnVotingIsStopped;
            OnVotingIsFinished += GroupPannel_OnVotingIsFinished;

            LoadUnionChats();
        }

        //Загружаем чаты союза
        protected Dictionary<long, ExternalChatId> unionChats = new Dictionary<long, ExternalChatId>();
        private void LoadUnionChats() {
            unionChats.Clear();
            unionChats = GetUnionChats();
            
            if (unionChats == null)
                return;

            unionChats.Add(BaseChatId, new ExternalChatId (BaseChatId) { Type = ChatType.Supergroup.ToString() });
        }


        //Проверяем состоит ли чат в союзе
        public bool IsChatFromUnion(long chatId) {
            if(BaseChatId ==0)
                return true;
            if(BaseChatId ==chatId)
                return true;
            
            if(unionChats !=null && unionChats.ContainsKey(chatId))
                return true;

            bot.LeaveChatAsync(chatId);
            return false;
        }



        #endregion

        #region GROUP VOTING
        //Выполняем функцию при запуске голосования
        private void GroupPannel_OnAppealVotingIsStarted(object sender, EventArgs e) {
            if(groupAppeals.ContainsKey(voting.AuthorId)) {

                long idAuthor = voting.AuthorId;
                SaveGroupVotingInfo(idAuthor, groupAppeals[idAuthor].Type, groupAppeals[idAuthor].ChatId
                , groupAppeals[idAuthor].ChatName, groupAppeals[idAuthor].ChatType
                , groupAppeals[idAuthor].ArchiveChatLink);
            }
        }

        //Выполняем функцию при загрузке данных действующего голосования
        private void GroupPannel_OnVotingIsLoaded(object sender, EventArgs e) {
            LoadGroupVotingInfo(voting.AuthorId);
        }

        //Останавливаем групповое обращение
        protected void StopGropAppeal(CallbackQuery callback) {
            StopAppeal(callback);
        }

        //Выполняем при остановке группового обращения
        private void GroupPannel_OnVotingIsStopped(object sender, VotingEventArgs e) {
            if(groupAppeals.ContainsKey(e.AuthorId)) {

                StopSolutionText(e.StoperId);
                groupAppeals.Remove(e.AuthorId);
                DeleteMessage(voting.PollMessageId);
                ResetVoting();
            }
        }

        


        //Выполняем функцию при завершении голосования
        private void GroupPannel_OnVotingIsFinished(object sender, VotingEventArgs e) {
            if(groupAppeals.ContainsKey(e.AuthorId)) {
                if(e.IsApproved) {

                    GroupAppeal unAppeal = groupAppeals[e.AuthorId];
                    SaveGroupSolution(unAppeal.Type, unAppeal.ChatId, unAppeal.ChatName
                    , unAppeal.ChatType, unAppeal.ArchiveChatLink);
                    LoadUnionChats();
                    if(unAppeal.Type == "Unite") {
                        OnNewUniteAdded?.Invoke(this, new GroupEventArgs {
                            ChatId = unAppeal.ChatId,
                            ChatType = unAppeal.ChatType,
                            AppealMessageId =voting.AppealMessageId
                        });
                    } else {
                        Confirmation conf = AcceptSolutionText(voting.AppealMessageId);
                        SignatureConfirmation(conf);
                    }
                } else {
                    RejectSolutionText();
                }
                groupAppeals.Remove(e.AuthorId);
                ResetVoting();
            }
        }

        #endregion

        #region SHOW AND CREATE GROUP APPEAL
        //Показываем страницы объединений
        protected void ShowGroupPages(CallbackQuery callback, string cancelCallback) {
            if(callback.Data == "ShowAssociationPageCallback") {
                ResetGroupAppeal(callback.From.Id);
                ShowChannelsAndChatsPage(callback.From.Id, cancelCallback);
            } 
        }

        //Сбрасываем обращение по объединениям
        protected void ResetGroupAppeal(long idUser) {
            if((voting == null) || (voting.AuthorId != idUser)) {
                if(groupAppeals.ContainsKey(idUser)) {

                    groupAppeals.Remove(idUser);
                    authors[idUser].IdEditMessage = 0;
                    ResetAppeal(idUser);
                } else {
                    ResetAppeal(idUser);
                }
            }
        }

        //Показываем страницу каналов и чатов
        private void ShowChannelsAndChatsPage(long idUser, string cancelCallback) {

            string donationUrl = GetDonateUrl(idUser);

            string text = $"{GetVotingText()}" +
                "\n\n<b>Пояснения:</b>" +
                $"\n🕰  Приём с {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}" +
                $"\n⏳  Голосования по {PollDurationMins} минут(ы/е)" +
                $"\n🪖  Отстранение {ArrestDurationMins} мин." +
                $"\n\n<a href =\"{donationUrl}\">🎁  Поддержать систему</a>" +
                $"\n<i>Союз ведёт <a href=\"tg://user?id={editor.Id}\">ID {editor.Id}</a></i>";


            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Объединиться", "CreateUniteAppealCallback")
                },
                new []{

                    InlineKeyboardButton.WithCallbackData("Назад", cancelCallback),
                    InlineKeyboardButton.WithCallbackData("Отделиться", "CreateSeparateAppealCallback")
                }
            });

            if(authors[idUser].IdEditMessage != 0) {
                EditMessage(idUser, text);
            } else {
                SendSettingsMessage(idUser, text, keyboard);
            }
        }

        //Создаём обращение объединений (для соединения и отделения групп)
        protected void CreateGroupAppeal(long idUser, Message message, string cancelCallback) {

            if(authors[idUser].CallbackData == "CreateUniteAppealCallback"
            || authors[idUser].CallbackData == "CreateSeparateAppealCallback") {

                PrepareGroupAppeal(idUser);
                if(string.IsNullOrEmpty(groupAppeals[idUser].ChatName)) {
                    SetChatAddress(idUser, message, "ShowAssociationPageCallback");
                } else {
                    if(string.IsNullOrEmpty(groupAppeals[idUser].ArchiveChatLink))
                        SetArchiveChatLink(idUser, message, "ShowAssociationPageCallback");
                    else
                        CreateAppeal(idUser, message, groupAppeals[idUser], "ShowAssociationPageCallback");
                }

            } else {
                CreateAppeal(idUser, message, null, cancelCallback);
            }
        }

        //Подготавливаем обращение для групп и объединений
        private void PrepareGroupAppeal(long idUser) {
            if(!groupAppeals.ContainsKey(idUser)) {
                GroupAppeal gr = new GroupAppeal();
                gr.AuthorId = authors[idUser].Id;

                groupAppeals.Add(idUser, gr);
                SetGroupAppealType(idUser);
            }
        }

        //Устанавливаем тип группового обращения
        private void SetGroupAppealType(long idUser) {
            switch(authors[idUser].CallbackData) {
                case "CreateUniteAppealCallback":
                    groupAppeals[idUser].Type = "Unite";
                    break;

                case "CreateSeparateAppealCallback":
                    groupAppeals[idUser].Type = "Separate";
                    break;
            }
        }

        //Устанавливаем адрес чата/канала
        private async void SetChatAddress(long idUser, Message message, string cancelCallback) {

            keyboard = null;
            if(message != null
                && !string.IsNullOrEmpty(message.Text)
                && message.Text.Contains("@")) {

                await Task.Run(() => {
                    try {
                        ChatFullInfo chatInfo = bot.GetChatAsync(message.Text).Result;
                        AcceptChat(idUser, chatInfo, cancelCallback);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.Data);
                        Console.WriteLine(ex.HResult);
                        Console.WriteLine(ex.StackTrace);
                        Console.WriteLine(ex.Source);

                        ShowChatAddressPage(idUser, null, cancelCallback);
                    }
                });
            } else {
                ShowChatAddressPage(idUser, null, cancelCallback);
            }
        }

        private void SetArchiveChatLink(long idUser, Message message, string cancelCallback){
            keyboard = null;

            if(message != null
            && !string.IsNullOrEmpty(message.Text)) {
                groupAppeals[idUser].ArchiveChatLink =message.Text;
                groupAppeals[idUser].Title += $" под ведомством архива {groupAppeals[idUser].ArchiveChatLink}";
                CreateGroupAppeal(idUser, null, cancelCallback);
            }else{
                string note = $"Супер! Теперь <b>пришлите ссылку на архивный чат</b>.";
                ShowChatAddressPage(idUser, note, cancelCallback);
            }

            
        }

        private void AcceptChat(long idUser, ChatFullInfo chat, string cancelCallback) {
            bool hasInUnion = HasInUnion(chat.Id);

            if(((!hasInUnion && authors[idUser].CallbackData == "CreateUniteAppealCallback")
                || (hasInUnion && authors[idUser].CallbackData == "CreateSeparateAppealCallback"))
                && chat.Id != BaseChatId) {

                if(chat.Type == ChatType.Channel
                    || chat.Type == ChatType.Supergroup) {

                    groupAppeals[idUser].ChatName = "@" + chat.Username;
                    groupAppeals[idUser].ChatId = chat.Id;
                    groupAppeals[idUser].ChatType = chat.Type.ToString();

                    SetGroupAppealTitle(idUser);
                    CreateGroupAppeal(idUser, null, cancelCallback);
                }
            } else {
                if((hasInUnion && authors[idUser].CallbackData == "CreateUniteAppealCallback")
                    || chat.Id == BaseChatId) {
                    string note = $"@{chat.Username} <b>уже участвует</b> в союзе.";

                    ShowChatAddressPage(idUser, note, cancelCallback);
                } else {
                    if(!hasInUnion && authors[idUser].CallbackData == "CreateSeparateAppealCallback") {
                        string note = $"@{chat.Username} <b>не участвует</b> в союзе.";

                        ShowChatAddressPage(idUser, note, cancelCallback);
                    }
                }
            }
        }

        //Устанавливаем заголовок группового обращения
        private void SetGroupAppealTitle(long idUser) {

            switch(authors[idUser].CallbackData) {
                case "CreateUniteAppealCallback":

                    groupAppeals[idUser].Title = $"Объединиться с <b>{groupAppeals[idUser].ChatName}</b>" +
                        $" <i>[ID{groupAppeals[idUser].ChatId}]</i>";
                    groupAppeals[idUser].ServiceInformation = $"<i>Чтобы отделиться создайте обращение через" +
                        $" <a href=\"tg://user?id={bot.BotId}\">помощника</a></i>." +
                        $"\n<i>Голосования по {PollDurationMins} минут(ы/е).</i> ";
                    break;

                case "CreateSeparateAppealCallback":
                    groupAppeals[idUser].Title = $"Отделиться от <b>{groupAppeals[idUser].ChatName}</b>" +
                        $" <i>[ID{groupAppeals[idUser].ChatId}]</i>";
                    groupAppeals[idUser].ServiceInformation = $"<i>Чтобы объединиться создайте обращение через" +
                        $" <a href=\"tg://user?id={bot.BotId}\">помощника</a></i>." +
                        $"\n<i>Голосования по {PollDurationMins} минут(ы/е).</i> ";
                    break;
            }
        }

        //Показываем страницу адреса чата/канала
        private void ShowChatAddressPage(long idUser, string note, string cancelCallback) {

            Appeal appeal = groupAppeals[idUser];
            string text = $"<b>ОБРАЩЕНИЕ</b>" +
                $"\nОт: {GetUserName(appeal.AuthorId)}" +
                $" <i><a href=\"tg://user?id={appeal.AuthorId}\">[ID {appeal.AuthorId}]</a></i>" +
                $"\n\n<pre>🎯  Цель обращения:</pre>\n{appeal.Title}";

            
            if(!string.IsNullOrEmpty(appeal.Title)) {
                text += $"\n\n";
            }

            text += "*Пришлите адрес чата.\n<i>Пример:@souzigra</i>";

            if(!string.IsNullOrEmpty(note)) {
                text += $"\n\n{note}\n";
            }


            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });

            EditMessageToUser(idUser, text, keyboard);
        }



        #endregion

        #region DATA BASE

        //Сохраняем отчёт об обращении
        private void SaveGroupVotingInfo(long authorId, string type, long chatId, string chatName
        , string chatType, string archiveChatLink) {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Votings SET Type =@type" +
                    ", SetChatId =@setChatId, SetChatName =@setChatName, SetChatType =@setChatType"+
                    ",SetArchiveChatLink =@setArchiveChatLink WHERE AuthorId =@authorId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@authorId", authorId);
                command.Parameters.AddWithValue("@type", type);
                command.Parameters.AddWithValue("@setChatId", chatId);
                command.Parameters.AddWithValue("@setChatName", chatName);
                command.Parameters.AddWithValue("@setChatType", chatType);
                command.Parameters.AddWithValue("@setArchiveChatLink", archiveChatLink); 

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Загружаем данные из базы
        private void LoadGroupVotingInfo(long authorId) {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Votings"+
                " WHERE AuthorId =@authorId AND (Type ='Unite' OR Type ='Separate')";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@authorId", authorId);

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {

                        GroupAppeal groupAppeal = new GroupAppeal();

                        groupAppeal.AuthorId = long.Parse(dataReader["AuthorId"].ToString());
                        groupAppeal.Type = dataReader["Type"].ToString();
                        groupAppeal.ChatId = long.Parse(dataReader["SetChatId"].ToString());
                        groupAppeal.ChatName = dataReader["SetChatName"].ToString();
                        groupAppeal.ChatType = dataReader["SetChatType"].ToString();
                        groupAppeal.ArchiveChatLink = dataReader["SetArchiveChatLink"].ToString();

                        if(!groupAppeals.ContainsKey(groupAppeal.AuthorId)) {
                            groupAppeals.Add(groupAppeal.AuthorId, groupAppeal);
                        }
                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        //Создаём основную таблицу
        private void CreateTableGroupChats(SqliteConnection cnct) {

            OpenDatabase();
            string sCommand = @"CREATE TABLE IF NOT EXISTS UnionChats (
                ChatId   INTEGER NOT NULL UNIQUE,
                ChatName     TEXT,
                ChatType     TEXT,
                ArchiveChatLink TEXT);";

            SqliteCommand command = new SqliteCommand();

            command.Connection = cnct;
            command.CommandText = sCommand;
            command.ExecuteNonQuery();

            CloseDatabase();
        }

        //Проверяем присутствует ли группа в союзе
        private bool HasInUnion(long chatId) {

            lock(dbLocker) {
                OpenDatabase();
                bool hasInUnion = false;
                string sCommand = "SELECT * FROM UnionChats WHERE ChatId =@chatId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@chatId", chatId.ToString());

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    hasInUnion = true;
                }

                dataReader.Close();
                CloseDatabase();
                return hasInUnion;
            }
        }

        //Сохраняем результат группового обращения
        private void SaveGroupSolution(string type, long chatId, string chatName, string chatType
        , string archiveChatLink) {

            lock(dbLocker) {
                OpenDatabase();
                string sCommand;

                if(type == "Unite") {
                    sCommand = "INSERT INTO UnionChats (ChatId, ChatName, ChatType, ArchiveChatLink)" +
                       " VALUES(@chatId, @chatName, @chatType, @archiveChatLink)" +
                       " ON CONFLICT(ChatId) DO UPDATE SET ChatId =@chatId, ChatName =@chatName" +
                       ", ChatType =@chatType, ArchiveChatLink =@archiveChatLink";
                } else {
                    sCommand = "DELETE FROM UnionChats WHERE ChatId =@chatId";
                }

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@chatId", chatId.ToString());
                command.Parameters.AddWithValue("@chatName", chatName);
                command.Parameters.AddWithValue("@chatType", chatType);
                command.Parameters.AddWithValue("@archiveChatLink", archiveChatLink);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }


        //Получаем список союзных объединений
        protected Dictionary<long, ExternalChatId> GetUnionChats() {
            lock (dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM UnionChats";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                Dictionary<long, ExternalChatId> unChats = unionChats;

                if (dataReader.HasRows) {

                    long idChat = 0;
                    ExternalChatId chat = null;
                    unChats = new Dictionary<long, ExternalChatId>();

                    while (dataReader.Read()) {

                        idChat = long.Parse(dataReader["ChatId"].ToString());
                        chat = new ExternalChatId(idChat);
                        chat.Type = dataReader["ChatType"].ToString();
                        chat.ArchiveChatLink = dataReader["ArchiveChatLink"].ToString();
                        unChats.Add(idChat, chat);
                    }
                }
                dataReader.Close();
                CloseDatabase();
                return unChats;
            }
        }

        #endregion

    }
}
