﻿//Основной модуль для создания и исполнения обращений

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.Appeals;
using TelegramAssistentBot.AppealsModels;
using Message = Telegram.Bot.Types.Message;

namespace TelegramAssistentBot {
    public class AppealPannel :BasePannel {

        #region INITIALIZE
        protected const int maxSymbols = 3500;
        System.Timers.Timer timer;
        protected Dictionary<long, Appeal> appeals = new Dictionary<long, Appeal>();
        protected DateTime StartTime { get; private set; }
        protected DateTime EndTime { get; private set; }

        public event EventHandler OnVotingIsStarted;    //Событие при начале голосования
        public event EventHandler OnVotingIsLoaded;     //Событие при загрузке текущего голосования
        public event EventHandler<VotingEventArgs> OnVotingIsStopped;     //Событие при остановке голосования
        public event EventHandler<VotingEventArgs> OnVotingIsFinished;  //Событие при окончании голосования

        public class VotingEventArgs {

            public long AuthorId { get; set; }
            public long StoperId { get; set; }
            public string CallbackId { get; set; }

            public bool IsApproved { get; set; }
            public string SolutionLink { get; set; }

        }

        public Voting voting { get; private set; }  //Действующее голосование
        public int PollDurationMins { get; set; }   //Продолжительность голосования в минутах


        //Создаём экземпляр панели обращения
        public AppealPannel() : base() {
            OnBaseCreated += AppealPannel_OnBaseCreated;
            OnVotingIsStarted += AppealPannel_OnVotingIsStarted;
            //OnVotingIsStopped += AppealPannel_OnVotingIsStopped;
            OnVotingIsFinished += AppealPannel_OnVotingIsFinished;

            CreateTableVotings(connection);
            LoadModeSettings();
        }

        



        //Загружаем информацию о голосовании в самой последней панели связки (те в конструкторе StartPannel)
        //чтобы можно было всеми панелями поймать событие загрузки голосования
        protected void CheckAndLoadActiveVoting() {
            LoadVotingInfo();
            if(voting != null) {
                LaunchTimer(voting.PollEndTime);
            }
        }

        //Выполняем при создании системы
        private void AppealPannel_OnBaseCreated(object sender, EventArgs e) {
            LoadModeSettings();
            LoadVotingInfo();
        }

        #endregion

        #region CREATE AND SEND APPEAL

        //Создаём обращение
        protected void CreateAppeal(long idUser, Message message, Appeal appeal, string cancelCallback) {

            PrepareAppeal(idUser, appeal);
            if(string.IsNullOrEmpty(appeals[idUser].Title)) {
                SetAppealTitleAndServiceInfo(idUser, message, cancelCallback);
            } else {
                if(string.IsNullOrEmpty(appeals[idUser].Description)) {
                    SetAppealDescription(idUser, message, cancelCallback);
                } else {
                    ShowConfirmationPage(idUser, cancelCallback);
                }
            }
        }


        //Подготавливаем обращение
        private void PrepareAppeal(long idUser, Appeal appeal) {

            if(!appeals.ContainsKey(idUser)) {
                if (appeal == null) {
                    appeal = new Appeal();
                    appeal.Type = "Common";
                    appeal.AuthorId = authors[idUser].Id;
                } else {
                    if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                        appeal.ServiceInformation += $"\n\n<i>*Ссылки доступны только участникам <a href=\"{BaseChatLink}\">чата</a>.</i>";
                    }
                }
                appeals.Add(idUser, appeal);
            }
        }

        //Устанавливаем заголовок общего обращения
        private async void SetAppealTitleAndServiceInfo(long idUser, Message message, string cancelCallback) {
            if(message != null
                && !string.IsNullOrEmpty(message.Text)) {

                if(message.Text.Length < 256) {
                    appeals[idUser].Title = message.Text;
                    appeals[idUser].ServiceInformation = $"<i>Голосования по {PollDurationMins} минут(ы/е)</i>";

                    walletBalance = await GetWalletBalanceAsync(WalletNumber);
                    if(walletBalance != nonExistentAmount) {
                        appeals[idUser].ServiceInformation += $"\n<i>Баланс на момент обращения " +
                            $"<b>{walletBalance}</b> руб.</i>";
                    }

                    if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                        appeals[idUser].ServiceInformation += $"\n\n<i>*Ссылки доступны только участникам <a href=\"{BaseChatLink}\">чата</a>.</i>";
                    }
                    ShowDescriptionPage(idUser, null, cancelCallback);
                } else {
                    EditMessageToUser(idUser, "Обратитесь в поддержку", keyboard);
                }

            } else {
                ShowTitlePage(idUser, cancelCallback);
            }
        }



        //Показываем страницу заголовка общего обращения
        protected void ShowTitlePage(long idUser, string cancelCallback) {
            Appeal appeal = appeals[idUser];
            string text = $"<b>ОБРАЩЕНИЕ</b>\nОт: {GetUserName(appeal.AuthorId)}" +
                    $" <i><a href=\"tg://user?id={appeal.AuthorId}\">[ID {appeal.AuthorId}]</a></i>" +
                    $"\n\n<code>🎯  Цель обращения:</code>" +
                    $"\n*Кратко опишите цель обращения до 256 символов." +
                    $"\n<i>Пример: Принять новое положение правил</i>";


            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        //Устанавливаем описание обращения
        //ОШИБКА В ТЕЛЕГРАМ: может быть сообщение с пустым message.ForwardFrom, но имеющее message.ForwardSenderName
        protected void SetAppealDescription(long idUser, Message message, string cancelCallback) {

            if(message != null
                && !string.IsNullOrEmpty(message.Text)
                && message.ForwardFrom == null
                && string.IsNullOrEmpty(message.ForwardSenderName)
                && message.Text.Length < (maxSymbols - FormAppealText(appeals[idUser]).Length)) {

                appeals[idUser].Description = message.Text;
                CreateAppeal(idUser, null, appeals[idUser], cancelCallback);
            } else {
                ShowDescriptionPage(idUser, null, cancelCallback);
            }
        }

        //Показываем страницу описания
        private void ShowDescriptionPage(long idUser, string note, string cancelCallback) {

            Appeal appeal = appeals[idUser];
            string text = $"<b>ОБРАЩЕНИЕ</b>" +
                $"\nОт: {GetUserName(appeal.AuthorId)}" +
                $" <i><a href=\"tg://user?id={appeal.AuthorId}\">[ID {appeal.AuthorId}]</a></i>" +
                $"\n\n<code>🎯  Цель обращения:</code>\n{appeal.Title}" +
                $"\n\n<code>🎨  Дополнительные сведения:</code>\n" +
                $"*Введите дополнительные сведения до {maxSymbols - FormAppealText(appeal).Length} символов." +
                $"\n<i>Пример: Забираю приз.</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        //Показываем страницу подтверждения
        protected void ShowConfirmationPage(long idUser, string cancelCallback) {

            string appealMessage = FormAppealText(appeals[idUser]);
            keyboard = new InlineKeyboardMarkup(new[]{
                            InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
                            , InlineKeyboardButton.WithCallbackData("Отправить", "SendAppealCallback")
                    });

            authors[idUser].IdEditMessage = SendMessageWithId(idUser, appealMessage, keyboard);
        }

        //Собираем текст обращения
        protected string FormAppealText(Appeal appeal) {

            ChatMember member = bot.GetChatMemberAsync(appeal.AuthorId, appeal.AuthorId).Result;
            string appealText = null;

            appealText =$"<b>ОБРАЩЕНИЕ</b>" +
                $"\nОт: {GetCorrectName(member.User.FirstName)}" +
                $" <i><a href=\"tg://user?id={appeal.AuthorId}\">[ID {appeal.AuthorId}]</a></i>" +
                $"\n\n<code>🎯  Цель обращения:</code>\n{appeal.Title}" +
                $"\n\n<code>🎨  Дополнительные сведения:</code>\n{appeal.Description}" +
                $"\n\n<code>💼  Служебная информация:</code>\n{appeal.ServiceInformation}";

            

            return appealText;
        }

        #endregion

        #region FORM AND SEND APPEAL

        //Собираем и отправляем обращение на голосование
        public void FormAndSendAppeal(long idUser) {

            if(appeals.ContainsKey(idUser)) {

                voting = new Voting(appeals[idUser]);
                appeals.Remove(idUser);

                voting.PollEndTime = DateTime.Now.AddMinutes(PollDurationMins);
                string appealText = FormAppealText(voting);
                voting.AppealMessageId = SendMessageWithId(BaseChatId, appealText, null);

                SendAndPinAppealPoll();
                string link = GetBaseLinkFromMessageId(voting.PollMessageId);

                string message = "Отправил ваше обращение." +
                    $"\n<i>(Решение будет через {PollDurationMins} минут(у/ы))</i>" +
                    $"\n\n<a href=\"{link}\">" +
                    "Ссылка на обращение.</a>";

                if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                    message += $"\n<i>*Ссылки доступны только участникам <a href=\"{BaseChatLink}\">чата</a>.</i>";
                }

                if (HasAdminRights(idUser) || AdministratorId == idUser && voting.Type == "Remittance"){
                    keyboard = new InlineKeyboardMarkup(new[]{
                        new []{InlineKeyboardButton.WithCallbackData("Подписать", "SignAppealCallback")}});
                }
                else {
                    keyboard = null;
                }
                EditMessageToUser(idUser, message, keyboard);
                authors[idUser].CallbackData = null;
                OnVotingIsStarted?.Invoke(this, new EventArgs());
            }
        }

        //Отправляем сообщение с возвратом уникального номера
        protected int SendMessageWithId(ChatId idUser, string title, InlineKeyboardMarkup keyboard) {

            try {
                return bot.SendTextMessageAsync(
                    chatId: idUser
                    , text: title
                    , replyMarkup: keyboard
                    , linkPreviewOptions: DisableLinks
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html).Result.MessageId;
            } catch {
                return 0;
            }
        }

        protected async Task<Telegram.Bot.Types.Message> SendMessageWithResult(ChatId idUser, string title, int replyMessageId =0
            , InlineKeyboardMarkup keyboard =null) {

            if (!string.IsNullOrEmpty(title)) {
                try {
                    return await bot.SendTextMessageAsync(
                        chatId: idUser
                        , text: title
                        , replyParameters: new ReplyParameters { MessageId = replyMessageId }
                        , replyMarkup: keyboard
                        , linkPreviewOptions: DisableLinks
                        , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);

                    } catch (Exception ex) {
                    try {
                        Console.WriteLine(ex.Message);
                        return await bot.SendTextMessageAsync(
                            chatId: idUser
                            , text: title
                            , replyMarkup: keyboard
                            , linkPreviewOptions: DisableLinks
                            , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
                    } catch {
                        return null;
                    }
                }
            } else {
                return null;
            }
        }


        //Отправляем и закрепляем опрос по обращению
        private void SendAndPinAppealPoll() {

            try {
                Message pollMessage = bot.SendPollAsync(chatId: BaseChatId
                    , question: "Принимаем решение по обращению"
                    , options: new[] {new InputPollOption {Text="Одобрить"}, new InputPollOption{Text="Отклонить"}}
                    , isAnonymous: false
                    , replyParameters: new ReplyParameters {  MessageId = voting.AppealMessageId } ).Result;


                voting.Poll = pollMessage.Poll;
                voting.PollMessageId = pollMessage.MessageId;
                bot.PinChatMessageAsync(BaseChatId, voting.PollMessageId, false);
            } catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        #endregion

        #region VOTING

        //Выполняем при начале голосования
        private void AppealPannel_OnVotingIsStarted(object sender, EventArgs e) {
            SaveVotingInfo(voting);
            LaunchTimer(voting.PollEndTime);
        }

        //Запускаем таймер на обращение
        private void LaunchTimer(DateTime endPoll) {

            DateTime now = DateTime.Now;
            if(timer == null) {
                timer = new System.Timers.Timer();
                timer.Elapsed += Timer_Elapsed;
            }

            //timer.Interval = 20000;
            //timer.Start();
            if (endPoll > now) {
                timer.Interval = (endPoll - now).TotalMilliseconds;
                timer.Start();
            } else {
                StopVoting(voting.Poll);
            }
        }

        //Вызываем событие при истечении времени на таймере
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            StopVoting(voting.Poll);
        }

        //Останавливаем голосование
        public void StopVoting(Poll poll, CallbackQuery callback = null) {
            if(callback == null) {
                if((timer != null) && (timer.Enabled)) {
                    timer.Stop();
                }
                if((voting != null) && (poll != null) && (voting.Poll.Id == poll.Id)) {

                    if(!poll.IsClosed) {
                        try {
                            voting.Poll = bot.StopPollAsync(BaseChatId, voting.PollMessageId).Result;
                        } catch {
                            ResetVoting();
                            return;
                        }
                    } else {
                        voting.Poll = poll;
                    }

                     if(voting.Poll.Options != null) {
                        int maxVoteOptionIndex = 0;   //Индекс того варианта в котором больше всего голосов
                        List<PollOption> options = voting.Poll.Options.ToList();

                        for(int i = 0; i < options.Count(); ++i) {
                            if(options[i].VoterCount > options[maxVoteOptionIndex].VoterCount) {
                                maxVoteOptionIndex = i;
                            }
                        }

                        if(maxVoteOptionIndex == 0
                            && (options[0].VoterCount != options[1].VoterCount)) {

                            string link = GetBaseLinkFromMessageId(voting.PollMessageId);

                            //Для общих обращений и по выплатам
                            if (voting.Type == "Remittance"
                                || voting.Type =="Common") {

                                if (voting.SignerId != 0){           
                                    OnVotingIsFinished?.Invoke(this, new VotingEventArgs{
                                        AuthorId = voting.AuthorId,
                                        IsApproved = true,
                                        SolutionLink = link
                                    });
                                } else {
                                    OnVotingIsFinished?.Invoke(this, new VotingEventArgs{
                                        AuthorId = voting.AuthorId,
                                        IsApproved = false
                                    });
                                }
                            } else {
                                OnVotingIsFinished?.Invoke(this, new VotingEventArgs {
                                    AuthorId = voting.AuthorId,
                                    IsApproved = true,
                                    SolutionLink = link
                                });
                            }

                        } else {
                            OnVotingIsFinished?.Invoke(this, new VotingEventArgs {
                                AuthorId = voting.AuthorId,
                                IsApproved = false
                            });
                        }
                    }
                }
            } else {
                if(timer != null && (timer.Enabled)) {
                    timer.Stop();
                }
                try {
                    voting.Poll = bot.StopPollAsync(BaseChatId, voting.PollMessageId).Result;
                    OnVotingIsStopped?.Invoke(this, new VotingEventArgs {
                        AuthorId = voting.AuthorId, StoperId = callback.From.Id
                        , CallbackId = callback.Id });
                } catch {
                    ResetVoting();
                    return;
                }
            }

        }

        //Сбрасываем все данные голосования
        protected void ResetVoting() {
            if(voting != null) {
                UnpinMessage(BaseChatId, voting.AppealMessageId);
                ResetAppeal(voting.AuthorId);
                DeleteVotingInfo(voting.AuthorId);
                voting = null;
            }
        }

        //Сбрасываем данные по обращению
        protected bool ResetAppeal(long idUser) {
            if((voting == null) || (voting.AuthorId != idUser)) {
                if(appeals.ContainsKey(idUser)) {
                    appeals.Remove(idUser);
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }

        //Останавливаем обращение
        protected void StopAppeal(CallbackQuery callback) {
            if(voting != null) {

                List<ChatMember> admins = bot.GetChatAdministratorsAsync(BaseChatId).Result.ToList();
                ChatMember admin = admins.Find(item => item.User.Id == callback.From.Id);

                if(admin != null) {
                    StopVoting(voting.Poll, callback);
                }
            }
        }

        //Выполняем при остновке голосования по обращению
        private void AppealPannel_OnVotingIsStopped(object sender, VotingEventArgs e) {
            if (voting != null) {
                if (voting.Type == "Common") {

                    StopSolutionText(e.StoperId);
                    DeleteMessage(voting.PollMessageId);
                    ResetVoting();
                }
            }
        }

        //Выполняем при окончании голосования
        private void AppealPannel_OnVotingIsFinished(object sender, VotingEventArgs e) {
            if(voting != null) {
                if(voting.Type == "Common") {
                    if(e.IsApproved) {
                        AcceptSolutionText(voting.AppealMessageId);
                    } else {
                        RejectSolutionText();
                    }
                    ResetVoting();
                }
            }
        }

        //Подтверждаем обращение по результату решения
        protected Confirmation AcceptSolutionText(int msgSolutionId) {

            Confirmation conf = new Confirmation();

            conf.Text = FormResultText(true);
            conf.MessageId = msgSolutionId;
            EditMessage(BaseChatId, conf.MessageId, conf.Text);

            conf.LastSolutionMessageId = conf.MessageId;
            SaveSolutionMessageId(conf.LastSolutionMessageId);

            string messageToAuthor = $"Ура! Участники одобрили наше обращение." +
                        $"\n\n<i><a href=\"{GetBaseLinkFromMessageId(conf.LastSolutionMessageId)}\">Результат обращения</a></i>";

            if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                messageToAuthor += $"\n<i>*Ссылки доступны только участникам <a href=\"{BaseChatLink}\">чата</a>.</i>";
            }

            EditMessageToUser(voting.AuthorId, messageToAuthor, null);
            return conf;
        }


        //Собираем текст с результатами обращения и голосования
        protected string FormResultText(bool isApproved, long idStopper = 0) {

            string link = GetBaseLinkFromMessageId(voting.PollMessageId);
            string resultText = $"🥁  <b>ОБРАЩЕНИЕ <a href=\"{link}\">ОДОБРЕНО</a></b>";

            if(isApproved == false) {
                resultText = $"🎻  <b>ОБРАЩЕНИЕ <a href=\"{link}\">ОТКЛОНЕНО</a></b>";
                if (idStopper == 0) {

                    if (voting.Type == "Remittance"
                        || voting.Type =="Common") {

                        if (voting.SignerId == 0) {
                            resultText = $"🎻  <b>ОБРАЩЕНИЕ НЕ ПОДПИСАНО</b>";
                        } else {
                            resultText = $"🎻  <b>ОБРАЩЕНИЕ <a href=\"{link}\">ОТКЛОНЕНО</a></b>";
                        }
                    }
                } else {
                    resultText = $"🎺  <b>ОБРАЩЕНИЕ ОСТАНОВЛЕНО</b>";
                }
            }

            
            resultText += $"\nОт: {GetUserName(voting.AuthorId)}" +
                $" <i><a href=\"tg://user?id={voting.AuthorId}\">[ID {voting.AuthorId}]</a></i>" +
                $"\n\n<code>🎯  Цель обращения:</code>\n{voting.Title}" +
                $"\n\n<code>🎨  Дополнительные сведения:</code>\n{voting.Description}" +
                $"\n\n<code>💼  Служебная информация:</code>\n{voting.ServiceInformation}";

            if(idStopper != 0) {
                resultText += $"\n<i>Остановил(a): {GetUserName(idStopper)} " +
                    $"<a href=\"tg://user?id={idStopper}\">[ID {idStopper}]</a></i>";
            }

            int LastSolutionMessageId = LoadLastSolutionMessageId();
            string lastLink = GetBaseLinkFromMessageId(LastSolutionMessageId);
            if(!string.IsNullOrEmpty(lastLink)) {
                resultText += $"\n\n🎙  <a href=\"{lastLink}\">Предыдущее решение</a>";
            }
            return resultText;
        }


        //Пишем текст для остановленного обращения
        protected void StopSolutionText(long idStopper) {
            string stopText = FormResultText(false, idStopper);
            EditMessage(BaseChatId, voting.AppealMessageId, stopText);

            string link = GetBaseLinkFromMessageId(voting.AppealMessageId);
            SaveSolutionMessageId(voting.AppealMessageId);

            string messageToAuthor = $"Представители союза остановили наше обращение." +
                        $"\n\n<i><a href=\"{link}\">Результат обращения</a></i>";

            EditMessageToUser(voting.AuthorId, messageToAuthor, null);
        }

        //Отклоняем обращение по результату решения
        protected void RejectSolutionText() {

            string rejectText = FormResultText(false);
            EditMessage(BaseChatId, voting.AppealMessageId, rejectText);

            string link = GetBaseLinkFromMessageId(voting.AppealMessageId);
            SaveSolutionMessageId(voting.AppealMessageId);

            string messageToAuthor = string.Empty;
            if(voting.SignerId == 0
                && (voting.Type == "Remittance" || voting.Type =="Common")) {

                messageToAuthor = $"Наше обращение не подписали." +
                        $"\n\n<i><a href=\"{link}\">Результат обращения</a></i>";
            } else {
                messageToAuthor = $"Участники отклонили наше обращение." +
                        $"\n\n<i><a href=\"{link}\">Результат обращения</a></i>";
            }

            EditMessageToUser(voting.AuthorId, messageToAuthor, null);
        }


        //Подписываем решение
        protected void SignatureConfirmation(Confirmation confirmation) {

            int place = confirmation.Text.IndexOf("ОДОБРЕНО</a></b>") + "ОДОБРЕНО</a></b>".Length;
            string signText = $" И <b>ИСПОЛНЕНО</b>";

            confirmation.Text = confirmation.Text.Insert(place, signText);
            EditMessage(BaseChatId, confirmation.MessageId, confirmation.Text);
        }

        //Подписываем обращение администратором
        protected void SignAppealByAdmin(CallbackQuery callback) {
            if (voting != null) {

                if (voting.SignerId == 0
                    && (voting.Type == "Remittance" || voting.Type =="Common")){

                    List<ChatMember> admins = bot.GetChatAdministratorsAsync(BaseChatId).Result.ToList();
                    ChatMember admin = admins.Find(item => item.User.Id == callback.From.Id);

                    if (admin != null){
                        voting.SignerId = admin.User.Id;
                        SaveVotingInfo(voting);

                        if (voting.SignerId != 0){
                            voting.ServiceInformation += $"\n<i>Подписал(a): {GetUserName(voting.SignerId)} " +
                                $"<a href=\"tg://user?id={voting.SignerId}\">[ID {voting.SignerId}]</a></i>";
                        }
                        string updateText = FormAppealText(voting);
                        SaveVotingInfo(voting);
                        EditMessage(BaseChatId, voting.AppealMessageId, updateText);
                        bot.AnswerCallbackQueryAsync(callback.Id, "✍️ Обращение подписано.");
                    }
                }
                else {
                    SaveVotingInfo(voting);
                }
            }
        }


        protected string GetVotingText() {
            string result = string.Empty;
            if (voting != null) {
                string link = GetBaseLinkFromMessageId(voting.PollMessageId);
                result = $"Решается <a href=\"{link}\">вопрос</a>";
                if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                    result += $"\n<i>*Ссылки доступны только участникам <a href=\"{BaseChatLink}\">чата</a>.</i>";
                }
            }
            return result;
        }
        #endregion

        #region DATA BASE

        //Создаём основную таблицу
        private void CreateTableVotings(SqliteConnection cnct) {

            OpenDatabase();
            string sCommand = @"CREATE TABLE IF NOT EXISTS Votings (
                AuthorId   INTEGER NOT NULL UNIQUE,
                Title     TEXT,
                Description     TEXT,
                ServiceInformation     TEXT,
                AppealMessageId   INTEGER,
                PollMessageId   INTEGER,
                PollId   TEXT,
                PollEndTime  TEXT,
                Type   TEXT,
                SetPollDurationMins   INTEGER,
                SetArrestDurationMins INTEGER,
                SetStartTime   TEXT,
                SetEndTime  TEXT,
                SetChatId   TEXT,
                SetChatName   TEXT,
                SetChatType     TEXT,
                SetArchiveChatLink      TEXT,
                SetPersonsJson   TEXT,
                SetAmount   INTEGER,
                SetWalletNumberOfRecipient   TEXT,
                SignerId   INTEGER);";

            SqliteCommand command = new SqliteCommand();

            command.Connection = cnct;
            command.CommandText = sCommand;
            command.ExecuteNonQuery();

            CloseDatabase();
        }

        //Сохраняем отчёт об обращении
        private void SaveVotingInfo(Voting vAppeal) {

            lock(dbLocker) {
                OpenDatabase();


                string sCommand = "INSERT INTO Votings (AuthorId, Title, Description" +
                    ", ServiceInformation, AppealMessageId, PollMessageId, PollId, PollEndTime, Type, SignerId)" +
                    " VALUES(@authorId, @title, @description, @serviceInformation" +
                    ", @appealMessageId, @pollMessageId, @pollId, @pollEndTime, @type, @signerId)" +
                    " ON CONFLICT(AuthorId) DO UPDATE SET AuthorId =@authorId, Title =@title" +
                    ", Description =@description, ServiceInformation =@serviceInformation" +
                    ", AppealMessageId =@appealMessageId, PollMessageId =@pollMessageId, PollId =@pollId" +
                    ", PollEndTime =@pollEndTime, Type =@type, SignerId =@signerId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@authorId", vAppeal.AuthorId);
                command.Parameters.AddWithValue("@title", vAppeal.Title);
                command.Parameters.AddWithValue("@description", vAppeal.Description);
                command.Parameters.AddWithValue("@serviceInformation", vAppeal.ServiceInformation);
                command.Parameters.AddWithValue("@appealMessageId", vAppeal.AppealMessageId);
                command.Parameters.AddWithValue("@pollMessageId", vAppeal.PollMessageId);
                command.Parameters.AddWithValue("@pollId", vAppeal.Poll.Id);
                command.Parameters.AddWithValue("@pollEndTime", vAppeal.PollEndTime);
                command.Parameters.AddWithValue("@type", vAppeal.Type);
                command.Parameters.AddWithValue("@signerId", vAppeal.SignerId);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Загружаем данные по действующему голосованию из базы
        private void LoadVotingInfo() {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Votings";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {

                        Appeal appeal = new Appeal();
                        appeal.AuthorId = long.Parse(dataReader["AuthorId"].ToString());

                        appeal.Title = dataReader["Title"].ToString();
                        appeal.Description = dataReader["Description"].ToString();
                        appeal.ServiceInformation = dataReader["ServiceInformation"].ToString();

                        voting = new Voting(appeal);
                        voting.PollMessageId = int.Parse(dataReader["PollMessageId"].ToString());
                        voting.Poll.Id = dataReader["PollId"].ToString();
                        voting.PollEndTime = DateTime.Parse(dataReader["PollEndTime"].ToString());
                        voting.AppealMessageId = int.Parse(dataReader["AppealMessageId"].ToString());
                        voting.Type = dataReader["Type"].ToString();
                        voting.SignerId = long.Parse(dataReader["SignerId"].ToString());

                    }
                }
                dataReader.Close();
                CloseDatabase();

                if(voting !=null) {
                    OnVotingIsLoaded?.Invoke(this, new EventArgs());
                }
            }
        }

        //Сохраняем отчёт об обращении
        private void DeleteVotingInfo(long authorId) {

            lock(dbLocker) {

                OpenDatabase();
                string sCommand = "DELETE FROM Votings WHERE AuthorId =@authorId;";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@authorId", authorId);
                command.ExecuteNonQuery();

                CloseDatabase();
            }
        }

        //Сохраняем ссылку с результатами решения
        protected void SaveSolutionMessageId(int LastSolutionMessageId) {

            lock(dbLocker) {

                OpenDatabase();
                string sCommand = "UPDATE Bases SET LastSolutionMessageId =@lastSolutionMessageId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@lastSolutionMessageId", LastSolutionMessageId);
                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Загружаем ссылку на последнее решение в цепочке
        private int LoadLastSolutionMessageId() {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Bases";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                int msgId =0;
                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {
                         int.TryParse(dataReader["LastSolutionMessageId"].ToString(), out msgId);   
                    }
                }
                dataReader.Close();
                CloseDatabase();
                return msgId;
            }
        }

        //Загружаем настройки процесса голосования
        protected void LoadModeSettings() {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Bases";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {
                        PollDurationMins = int.Parse(dataReader["PollDurationMins"].ToString());

                        StartTime = DateTime.Parse(dataReader["StartTime"].ToString());
                        EndTime = DateTime.Parse(dataReader["EndTime"].ToString());

                        ArrestDurationMins = int.Parse(dataReader["ArrestDurationMins"].ToString());
                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        #endregion 
    }
}
