﻿//Модуль страницы системы для перехода к другим страницам
//


using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.Pannels;

namespace TelegramAssistentBot.AppealsModels {
    public class SystemPannel :OrderPannel {

        public SystemPannel() :base() {}
        protected void ShowSystemPages(CallbackQuery callback, string cancelCallback) {

            long idUser = callback.From.Id;
            if(callback.Data == "ShowSystemPageCallback") {
                if(ResetAppeal(idUser)) {
                    authors[idUser].IdEditMessage = 0;
                }
                ShowSystemPage(idUser, cancelCallback);
            } else {
                if(callback.Data.Contains("ShowBase")) {
                    if(idUser == AdministratorId || (HasAdminRights(idUser) && AllowControl)) {
                        AcceptBasePannelCallback(callback, "ShowSystemPageCallback");
                    } else {
                        try {
                            
                            bot.AnswerCallbackQueryAsync(callback.Id, "⚠️  Только для основателя и представителей.");
                        } catch { }
                    }
                } else {
                    ShowModePages(callback, "ShowSystemPageCallback");
                }
            }
        }

        //Сделал для того, чтобы отметить точку входа на странице системы
        protected void CreateSystemAppeal(long idUser, Message message, string cancelCallback) {
            CreateModeAppeal(idUser, message, "ShowSystemPageCallback");
        }


        //Показываем системную страницу
        private void ShowSystemPage(long idUser, string cancelCallback) {
            string donationUrl = GetDonateUrl(idUser);
            string text = $"{GetVotingText()}" +
                "\n\n<b>Пояснения:</b>" +
                $"\n🕰  Приём с {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}" +
                $"\n⏳  Голосования по {PollDurationMins} минут(ы/е)" +
                $"\n🪖  Арест {ArrestDurationMins} мин." +
                $"\n\n<a href =\"{donationUrl}\">🎁  Поддержать систему</a>" +
                $"\n<i>Союз ведёт <a href=\"tg://user?id={editor.Id}\">ID {editor.Id}</a></i>";

            string donateUrl = GetDonateUrl(idUser);


            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Обращение", "CreateCommonAppealCallback"),
                    
                },

                new []{
                    InlineKeyboardButton.WithCallbackData("Союз", "ShowPublicPageCallback"),
                    InlineKeyboardButton.WithCallbackData("Режим", "ShowModePageCallback")
                },

                

                new []{

                    InlineKeyboardButton.WithCallbackData("Назад", cancelCallback),
                    InlineKeyboardButton.WithCallbackData("Обслуживание", "ShowBasePageCallback")
                }
            }) ;

            if(authors[idUser].IdEditMessage != 0) {
                EditMessage(idUser, text);
            } else {
                SendSettingsMessage(idUser, text, keyboard);
            }
        }


    }
}
