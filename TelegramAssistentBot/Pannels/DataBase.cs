﻿//Самый первый модуль создающий базу данных для основной панели
//Поставил этот модуль в папку "Pannels" т.к от него идёт цепочка всех наследований типов Pannel

using System;
using Microsoft.Data.Sqlite;

namespace TelegramAssistentBot {
    public class DataBase  {

        string connectionString = string.Empty;

        protected SqliteConnection connection =null;
        protected SqliteDataReader dataReader =null;
        protected object dbLocker = new object();
        protected bool isDatabaseExists = false;
        private string password = string.Empty;


        //Создаём объект
        public DataBase() {
            isDatabaseExists = IsDatabaseExists();
        }

        //Проверяем существует ли база данных
        private bool IsDatabaseExists() {
            return System.IO.File.Exists("./database.sqlite3") ? true : false;
        }

        //Создаём базу данных с паролем
        protected void CreateDatabase() {
            if (string.IsNullOrEmpty(password)) {
                Console.WriteLine("Входим в базу данных. Введите пароль доступа:");
                password = Console.ReadLine();
            }
            if (connection ==null) {
                connectionString = new SqliteConnectionStringBuilder("Data Source=database.sqlite3") {
                    Mode = SqliteOpenMode.ReadWriteCreate,
                    Password = this.password
                }.ToString();
                connection = new SqliteConnection(connectionString);
            }
            Console.Clear();
            Console.WriteLine("Ура! Пароль принят, всё верно.");
        }

        //Открываем базу данных
        protected void OpenDatabase() {
            if(connection != null
                && connection.State == System.Data.ConnectionState.Closed) {

                connection.Open();
            }
        }

        //Закрываем базу данных
        protected void CloseDatabase() {
            if(connection != null
                && connection.State == System.Data.ConnectionState.Open) {
                connection.Close();
            }
        }


        //Меняем пароль базы данных
        public void ChangePassword(string newPassword) {
            connection.Open();
            SqliteCommand command = connection.CreateCommand();
            command.CommandText = "SELECT quote($newPassword);";
            command.Parameters.AddWithValue("$newPassword", newPassword);
            string quotedNewPassword = (string)command.ExecuteScalar();

            command.CommandText = "PRAGMA rekey = " + quotedNewPassword;
            command.Parameters.Clear();
            command.ExecuteNonQuery();
            connection.Close();
        }

        protected void SetPassword(string password) {
            this.password = password;
        }

        protected void SetNewDatabasePassword() {
            password = string.Empty;
            if (isDatabaseExists) {
                Console.WriteLine("Введите пароль для доступа к базе данных и запуска программы:");
                password = Console.ReadLine();
                CreateDatabase();
            } else {
                Console.WriteLine("Мы создаём базу данных.\nВведите желаемый пароль доступа:");
                password = Console.ReadLine();
                Console.WriteLine("Продолжаем создавать базу данных. Повторите желаемый пароль доступа:");
                if(password == Console.ReadLine()) {
                    CreateDatabase();
                    Console.WriteLine("Ура! База данных - готова!");
                } else {
                    Console.WriteLine("Ой! Пароли не совпадают. Давайте попробуем заново.");
                    SetNewDatabasePassword();
                }
            }
        }



    }
}
