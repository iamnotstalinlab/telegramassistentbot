﻿//Модуль с созданием обращения по денежному переводу, его исполнению и учёту
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramAssistentBot.AppealsModels;
using TelegramAssistentBot.Pannels;

namespace TelegramAssistentBot {
    public class RemittancePannel :ModePannel {

        #region INITIALIZE
        Dictionary<long, RemittanceAppeal> remAppeals = new Dictionary<long, RemittanceAppeal>();

        //Создаём экземпляр объекта панели денежного перевода
        public RemittancePannel() :base() {
            OnVotingIsLoaded += RemittancePannel_OnVotingIsLoaded;
            OnVotingIsStarted += RemittancePannel_OnVotingIsStarted;
            OnVotingIsStopped += RemittancePannel_OnVotingIsStopped;
            OnVotingIsFinished += RemittancePannel_OnVotingIsFinished;
        }





        #endregion

        #region SHOW PAGES AND CREATE REMITTANCE APPEAL




        //Создаём обращение по денежному переводу
        public void CreateRemittanceAppeal(long idUser, Message message, string cancelCallback) {
            if(authors[idUser].CallbackData == "CreateRemittanceAppealCallback") {

                if(IsRemittanceAvailable()) {
                    PrepareRemittanceAppeal(idUser);
                    if(remAppeals[idUser].Amount <= 0) {
                        SetAppealAmount(idUser, message, cancelCallback);
                    } else {
                        if(string.IsNullOrEmpty(remAppeals[idUser].WalletNumberOfRecipient)) {
                            SetWalletNumberOfRecipient(idUser, message, cancelCallback);
                        } else {
                            CreateAppeal(idUser, message, remAppeals[idUser], cancelCallback);
                        }
                    }
                }else {
                    ShowRemittanceErrorPage(idUser, cancelCallback);
                }
            } else {
                CreateModeAppeal(idUser, message, cancelCallback);
            }
        }

        

        //Подготавливаем обращение по денежному переводу
        private void PrepareRemittanceAppeal(long idUser) {
            if(!remAppeals.ContainsKey(idUser)) {

                RemittanceAppeal rem = new RemittanceAppeal();

                rem.AuthorId = authors[idUser].Id;
                remAppeals.Add(idUser, rem);
                remAppeals[idUser].Type = "Remittance";

                SetRemittanceAppealServiceInformation(idUser);
            }
        }

        //Проверяем доступен ли денежный перевод
        private bool IsRemittanceAvailable() {
            if(WalletNumber != "NULL") {

                walletBalance = GetWalletBalanceAsync(WalletNumber).Result;
                if(walletBalance != nonExistentAmount) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }


        //Устанавливаем служебную информацию в денежном обращении
        private void SetRemittanceAppealServiceInformation(long idUser) {
            remAppeals[idUser].ServiceInformation = $"" +
                $"<i>Баланс на момент обращения <b>{walletBalance}</b> руб.</i>" +
                $"\n<i>Голосования по {PollDurationMins} минут(ы/е).</i>";
        }


        //Показываем страницу ошибки перевода
        private void ShowRemittanceErrorPage(long idUser, string cancelCallback) {

            ResetRemittanceAppeal(idUser);
            string text = "<pre>⚠️  Денежный перевод не доступен</pre>" +
                "\nОбратитесь к представителям, они постараются помочь.";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Назад", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        //Устанавливаем сумму обращения
        private void SetAppealAmount(long idUser, Message message, string cancelCallback) {
            if(message != null
                && !string.IsNullOrEmpty(message.Text)) {

                int amount;
                if(int.TryParse(message.Text, out amount)) {
                    if(amount < walletBalance) {

                        remAppeals[idUser].Amount = amount;
                        CreateRemittanceAppeal(idUser, null, cancelCallback);
                    } else {
                        string text = "На счету системы, пока что нет таких средств." +
                            "Введите другую сумму или повторите позднее.";

                        keyboard = new InlineKeyboardMarkup(new[]{
                            InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
                            });
                        EditMessageToUser(idUser, text, keyboard);
                    }
                } else {
                    ShowAmountPage(idUser, cancelCallback);
                }
            } else {
                ShowAmountPage(idUser, cancelCallback);
            }
        }

        //Показываем страницу суммы обращения
        private void ShowAmountPage(long idUser, string cancelCallback) {

            string text = $"<pre>💼  Служебная информация:</pre>\n{remAppeals[idUser].ServiceInformation}";
            text += "\n\n*Пришлите желаемую сумму для перевода.\n<i>(Пример: 100)</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        private void SetRemittanceAppealTitle(long idUser) {
            if(!string.IsNullOrEmpty(remAppeals[idUser].WalletNumberOfRecipient)) {
                remAppeals[idUser].Title = $"Выполнить перевод суммы <b>{remAppeals[idUser].Amount}</b>" +
                $"+{remAppeals[idUser].Commission} руб. на Qiwi-кошелёк по номеру" +
                $" <b>{remAppeals[idUser].WalletNumberOfRecipient}</b>.";
            }
        }

        //Устанавливам номер кошелька получателя
        private void SetWalletNumberOfRecipient(long idUser, Message message, string cancelCallback) {
            if(message != null
                && !string.IsNullOrEmpty(message.Text)) {

                if(message.Text !=WalletNumber
                    && double.TryParse(message.Text, out double n)) {
                    QwCommission commission = GetPaymentCommission(remAppeals[idUser].Amount, message.Text);

                    if(commission != null) {    //Проверяем запрос комиссии (прошёл или нет)
                        remAppeals[idUser].Commission = commission.amount;

                        remAppeals[idUser].WalletNumberOfRecipient = message.Text;
                        SetRemittanceAppealTitle(idUser);
                        CreateRemittanceAppeal(idUser, null, cancelCallback);
                    } else {
                        ShowRemittanceErrorPage(idUser, cancelCallback);
                    }
                } else {
                    ShowWalletNumberPage(idUser, cancelCallback);
                }
            } else {
                ShowWalletNumberPage(idUser, cancelCallback);
            }
        }

        //Получаем комиссию по переводу
        private QwCommission GetPaymentCommission(double amount, string walletNumberOfRecipient) {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            QiwiCommissionRequest commRequest = new QiwiCommissionRequest();
            commRequest.account = walletNumberOfRecipient;
            commRequest.paymentMethod.type = "Account";
            commRequest.paymentMethod.accountId = "643";

            commRequest.purchaseTotals.total.amount = amount;
            commRequest.purchaseTotals.total.currency = "643";

            string json = JsonConvert.SerializeObject(commRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            string url = $"https://edge.qiwi.com//sinap/providers/99/onlineCommission";
            HttpResponseMessage response = client.PostAsync(url, data).Result;

            string result = response.Content.ReadAsStringAsync().Result;
            QiwiCommissonResponse commResponse = JsonConvert.DeserializeObject<QiwiCommissonResponse>(result);
            if(commResponse != null && commResponse.qwCommission != null) {
                return commResponse.qwCommission;
            } else {
                return null;
            }
        }

        //Показываем страницу номера кошелька
        private void ShowWalletNumberPage(long idUser, string cancelCallback) {

            string text = $"<b>ОБРАЩЕНИЕ</b>\nОт: {GetUserName(remAppeals[idUser].AuthorId)}" +
                    $" <i><a href=\"tg://user?id={remAppeals[idUser].AuthorId}\">[ID {remAppeals[idUser].AuthorId}]</a></i>" +
                    $"\n\n<pre>🎯  Цель обращения:</pre>";

            text += "\n*Пришлите номер Qiwi-кошелька <a" +
                " href=\"https://qiwi.com/support/information/subject5/identifikatsiya-v-qiwi-koshelke\"" +
                ">с основным статусом</a>.\n<i>Пример: 79778140892</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback)
            });
            EditMessageToUser(idUser, text, keyboard);
        }

        #endregion


        #region REMITTANCE VOTING

        //Выполняем при запукске голосования
        private void RemittancePannel_OnVotingIsStarted(object sender, EventArgs e) {

            if(remAppeals.ContainsKey(voting.AuthorId)) {
                long idAuthor = voting.AuthorId;

                SaveVotingRemittanceAppealInfo(idAuthor, "Remittance"
                    , remAppeals[idAuthor].Amount, remAppeals[idAuthor].WalletNumberOfRecipient);
            }
        }

        //Выполняем при загрузке информации о действующем голосовании
        private void RemittancePannel_OnVotingIsLoaded(object sender, EventArgs e) {
            LoadVotingRemittance(voting.AuthorId);
        }

        //Останавливаем обращение денежного перевода
        protected void StopRemittanceAppeal(CallbackQuery callback) {
            StopModeAppeal(callback);
        }

        //Выполняем при остановке голосования
        private void RemittancePannel_OnVotingIsStopped(object sender, VotingEventArgs e) {
            if(remAppeals.ContainsKey(e.AuthorId)) {
                StopSolutionText(e.StoperId);
                remAppeals.Remove(e.AuthorId);
                DeleteMessage(voting.PollMessageId);
                ResetVoting();
            }
        }

        //Выполняем при завершении голосования
        private void RemittancePannel_OnVotingIsFinished(object sender, VotingEventArgs e) {
            if(remAppeals.ContainsKey(e.AuthorId)) {
                if(e.IsApproved) {

                    Confirmation conf = AcceptSolutionText(voting.AppealMessageId);
                    string result = ExecuteRemittance(e.AuthorId, conf);
                    SignatureRemittanceConfirmation(conf, conf.MessageId, result); 
                } else {
                    RejectSolutionText();
                }
                remAppeals.Remove(e.AuthorId);
                ResetVoting();
            }
        }

        //Сбрасываем обращение денежного перевода
        protected void ResetRemittanceAppeal(long idUser) {
            if((voting == null) || (voting.AuthorId != idUser)) {
                if(remAppeals.ContainsKey(idUser)) {
                    remAppeals.Remove(idUser);
                    authors[idUser].IdEditMessage = 0;
                    ResetAppeal(idUser);
                } else {
                    ResetModeAppeal(idUser);
                }
            }
        }

        //Исполняем денежный перевод
        private string ExecuteRemittance(long idUser, Confirmation confirmation) {
            QiwiPayment payment = new QiwiPayment();
            payment.id = GetPaymentId();

            payment.sum.amount = remAppeals[idUser].Amount;
            payment.sum.currency = "643";

            payment.paymentMethod.type = "Account";
            payment.paymentMethod.accountId = "643";

            payment.comment = voting.Description;
            payment.fields.account = remAppeals[idUser].WalletNumberOfRecipient;

            return MakeRemittance(payment);
        }


        //Получаем уникальный идентификатор платежа по документам Qiwi
        private string GetPaymentId() {
            string paymentId = (DateTime.Now.ToUniversalTime().Subtract(
                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds * 1000).ToString();

            paymentId = paymentId.Replace(".", "");
            if(paymentId.Length > 20) {
                paymentId = paymentId.Remove(paymentId.Length, paymentId.Length -20);
            }
            return paymentId;
        }


       
        
        //Рабочий метод оплаты через киви
        private string MakeRemittance(QiwiPayment payment) {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string json = JsonConvert.SerializeObject(payment);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            string url = $"https://edge.qiwi.com/sinap/api/v2/terms/99/payments";
            HttpResponseMessage response = client.PostAsync(url, data).Result;

            return response.Content.ReadAsStringAsync().Result;
        }

        //Получаем изображение чека по оплате и сохраняем его
        private async Task<bool> SavePaymentReceiptImage(string transactionId, string path) {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string url = $"https://edge.qiwi.com/payment-history/v1/transactions" +
                $"/{transactionId}/cheque/file?type=OUT&format=JPEG";
            HttpResponseMessage response = await client.GetAsync(url);

            using(FileStream fs = System.IO.File.Open(path, FileMode.Open, FileAccess.Write, FileShare.None)) {
                byte[] imageBytes = await response.Content.ReadAsByteArrayAsync();
                fs.Write(imageBytes, 0, imageBytes.Length);
            }
            return true;
        }


        //остановился здесь
        //Подписываем исполененое денежное обращение
        private void SignatureRemittanceConfirmation(Confirmation confirmation, int signMessageId, string paymentJson) {

            if(!string.IsNullOrEmpty(paymentJson)) {
                Transaction transaction = JsonConvert.DeserializeObject<QiwiPaymentInfo>(paymentJson).transaction;
                if(transaction != null) {
                    SignSuccessPayment(confirmation, transaction.id);
                } else {
                    SignFailurePayment(confirmation, paymentJson);
                }
            } else {
                string error = "Qiwi-сервер отказал в платеже. Возможно, проблемы с авторизацией.";
                SignErrorPayment(confirmation, signMessageId, error);
            }
        }

        //Подписываем обращение с успешным платежом
        private async void SignSuccessPayment(Confirmation confirmation, string transactionId) {
            
            Stream stream = await GetRecieptImageStream(transactionId);
            string caption = $"Перевод исполнен в соответствии с <a href=\"{GetBaseLinkFromMessageId(confirmation.LastSolutionMessageId)}\">общим решением</a>.";
            if (string.IsNullOrEmpty(GetBaseChatUsername())) {
                caption += $"\n<i>*Ссылки доступны только участникам <a href=\"{BaseChatLink}\">чата</a>.</i>";
            }
            int recieptId = SendPhoto(BaseChatId, stream, caption);
            string link = GetBaseLinkFromMessageId(recieptId);

            int place = confirmation.Text.IndexOf("ОДОБРЕНО</a></b>") + "ОДОБРЕНО</a></b>".Length;
            string signText = $" И <b><a href=\"{link}\">ИСПОЛНЕНО</a></b>";
            confirmation.Text = confirmation.Text.Insert(place, signText);

            EditMessage(BaseChatId, confirmation.MessageId, confirmation.Text);
        }

        //Подписываем с отклонённым платежом
        private void SignFailurePayment(Confirmation confirmation, string paymentJson) {
            int place = confirmation.Text.IndexOf("ОДОБРЕНО</a></b>") + "ОДОБРЕНО</a></b>".Length;
            string signText = $", НО <b>ПЛАТЁЖ ОТКЛОНЁН</b>";
            confirmation.Text = confirmation.Text.Insert(place, signText);

            place = confirmation.Text.LastIndexOf("<i>Баланс на момент обращения");

            QiwiPaymentError qError = JsonConvert.DeserializeObject<QiwiPaymentError>(paymentJson);
            confirmation.Text = confirmation.Text.Insert(place, "<i>*" + qError.message + ": "
                +qError.code + "</i>\n\n");

            EditMessage(BaseChatId, confirmation.MessageId, confirmation.Text);
        }

        //Подписываем с отклонённым платежом
        private void SignErrorPayment(Confirmation confirmation, int signMessageId, string error) {
            int place = confirmation.Text.IndexOf("ОДОБРЕНО</a></b>") + "ОДОБРЕНО</a></b>".Length;
            string signText = $", НО <b>ПЛАТЁЖ ОТКЛОНЁН</b>";
            confirmation.Text = confirmation.Text.Insert(place, signText);

            place = confirmation.Text.LastIndexOf("<i>Баланс на момент обращения");
            confirmation.Text = confirmation.Text.Insert(place, "<i>*" + error + "</i>\n\n");

            EditMessage(BaseChatId, confirmation.MessageId, confirmation.Text);
        }


        //Получаем изображение чека платежа как поток байтов и битов
        private async Task<Stream> GetRecieptImageStream(string transactionId) {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer" + WalletToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string url = $"https://edge.qiwi.com/payment-history/v1/transactions" +
                $"/{transactionId}/cheque/file?type=OUT&format=JPEG";
            HttpResponseMessage response = await client.GetAsync(url);
            
            while(response.StatusCode == System.Net.HttpStatusCode.NotFound) {
                response = await client.GetAsync(url);
            }

            return await response.Content.ReadAsStreamAsync();
        }




        #endregion

        #region DATA BASE
        //Сохраняем отчёт об обращении
        private void SaveVotingRemittanceAppealInfo(long authorId, string type, int amount, string walletNumberOfRecipient) {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE Votings SET Type =@type" +
                    ", SetAmount =@setAmount, SetWalletNumberOfRecipient =@setWalletNumberOfRecipient WHERE AuthorId =@authorId";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@authorId", authorId);
                command.Parameters.AddWithValue("@type", type);
                command.Parameters.AddWithValue("@setAmount", amount);
                command.Parameters.AddWithValue("@setWalletNumberOfRecipient", walletNumberOfRecipient);


                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Загружаем данные из базы
        private void LoadVotingRemittance(long authorId) {
            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Votings WHERE AuthorId =@authorId AND Type ='Remittance'";

                SqliteCommand command = new SqliteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@authorId", authorId);

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {
                    while(dataReader.Read()) {

                        RemittanceAppeal remAppeal = new RemittanceAppeal();
                        remAppeal.AuthorId = long.Parse(dataReader["AuthorId"].ToString());
                        remAppeal.Amount = int.Parse(dataReader["SetAmount"].ToString());
                        remAppeal.WalletNumberOfRecipient = dataReader["SetWalletNumberOfRecipient"].ToString();

                        if(!remAppeals.ContainsKey(remAppeal.AuthorId)) {
                            remAppeals.Add(remAppeal.AuthorId, remAppeal);
                        }
                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        #endregion
    }
}
