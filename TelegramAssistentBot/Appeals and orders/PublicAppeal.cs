﻿//Модуль объекта союзного обращения
//

using System.Collections.Generic;
using Telegram.Bot.Types;

namespace TelegramAssistentBot.AppealsModels {
    public class UnionAppeal :Appeal{

        public bool IsPersonsFull { get; set; }
        public List<PersonInfo> Persons { get; set; }
        

        public UnionAppeal() {

            Persons = new List<PersonInfo>();
        }
    }
}
