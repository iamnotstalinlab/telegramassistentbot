﻿//Модуль объекта денежного обращения
//

namespace TelegramAssistentBot.AppealsModels {
    public class RemittanceAppeal :UnionAppeal {

        public int Amount { get; set; } //Сумма денег для перевода
        public double Commission {get; set;}
        
        public string WalletNumberOfRecipient { get; set; }

        public RemittanceAppeal() {}
    }
}
