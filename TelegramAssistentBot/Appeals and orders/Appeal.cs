﻿//Основной модуль объекта обращения
//

namespace TelegramAssistentBot.AppealsModels {
    public class Appeal {

        public long AuthorId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string ServiceInformation { get; set; }

        public string Type { get; set; }

        public Appeal() {}
    }
}
