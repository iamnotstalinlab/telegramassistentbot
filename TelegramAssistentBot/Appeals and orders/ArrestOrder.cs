﻿using System;
using System.Collections.Generic;
using Telegram.Bot.Types;

namespace TelegramAssistentBot.Appealsandorders {
    public class Order {

        public PersonInfo ActivePerson { get; set; }
        public PersonInfo PassivePerson { get; set; }
        
        public Message TargetMessage { get; set; }
        public string MessageArchiveNumber { get; set; }
        public List<int> IdsSelectedMessages { get; set; }
        public List<string> SelectedMessagesArchiveNumbers { get; set; }

        public DateTime ArrestEndTime { get; set; }

        public string Description { get; set; }
        public string ServiceText { get; set; }

        public string NewAlias { get; set; }
        public Order() {}
    }
}
