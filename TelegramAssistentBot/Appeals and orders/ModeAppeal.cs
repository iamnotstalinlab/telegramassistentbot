﻿//Модуль объекта режимного обращения
//

using System;
namespace TelegramAssistentBot.AppealsModels {
    public class ModeAppeal : Appeal{

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        //Продолжительности в минутах
        public int PollDurationMins { get; set; }
        public int ArrestDurationMins { get; set; }

        public ModeAppeal() {}
    }
}
