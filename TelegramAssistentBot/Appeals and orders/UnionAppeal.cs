﻿//Модуль объекта союзного обращения
//

namespace TelegramAssistentBot.AppealsModels {
    public class GroupAppeal :Appeal {

        public string ChatName { get; set; }
        public long ChatId { get; set; }
        public string ChatType { get; set; }
        public string ArchiveChatLink{get;set;}

        public GroupAppeal(){}
    }
}
