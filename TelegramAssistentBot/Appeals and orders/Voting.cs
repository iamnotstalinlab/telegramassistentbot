﻿//Модуль действующего голосования по обращению
//

using System;
using Telegram.Bot.Types;
using TelegramAssistentBot.AppealsModels;

namespace TelegramAssistentBot.Appeals {
    public class Voting :Appeal{

        public Poll Poll { get; set; }
        public int PollMessageId { get; set; }
        public DateTime PollEndTime { get; set; }

        public int AppealMessageId { get; set; }
        public long SignerId { get; set; }

        public Voting(Appeal appeal) {

            AuthorId = appeal.AuthorId;

            Title = appeal.Title;
            Description = appeal.Description;
            ServiceInformation = appeal.ServiceInformation;

            Type = appeal.Type;
            Poll = new Poll();
        }
    }
}
