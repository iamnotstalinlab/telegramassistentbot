﻿//Модуль телеграм бота, который принимает все события от Телеграм и распределяет их по системе
//

using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramAssistentBot.QiwiModels;
//using static System.Data.Entity.Infrastructure.Design.Executor;

namespace TelegramAssistentBot {
    public class TelegramBot {

        #region CREATE BLOCK

        StartPanel tBase;

        public void SkipUdates(){
            if(tBase ==null || tBase.bot ==null)
                return;
            tBase.bot.DropPendingUpdates();
        }
        protected ReceiverOptions BaseRecieverOption = new ReceiverOptions{ 
            AllowedUpdates = new UpdateType[] { 
            UpdateType.Message
            , UpdateType.CallbackQuery
            , UpdateType.ChatJoinRequest
            , UpdateType.PollAnswer }
        };

        [Obsolete]
        //Создаём экземпляр бота
        public TelegramBot() {
            tBase = new StartPanel();
            tBase.bot.StartReceiving(new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync), BaseRecieverOption);
        }

        //Здесь управляем всеми обновлениями, которые видит бот
        async Task HandleUpdateAsync(ITelegramBotClient botClient, Telegram.Bot.Types.Update update, CancellationToken cancellationToken) {

            switch (update.Type) {
                case UpdateType.Message:
                    AcceptAnyMessage(update.Message);
                    break;
                case UpdateType.CallbackQuery:
                    AcceptAnyCallback(update.CallbackQuery);
                    break;
                case UpdateType.ChatJoinRequest:
                    AcceptAnyChatJoinRequest(update.ChatJoinRequest);
                    break;
                case UpdateType.PollAnswer:
                    AcceptAnyPoolAnswer(update.PollAnswer);
                    break;
            }


        }


        //Принимаем любые сообщения и обратные вызовы
        private void AcceptAnyMessage(Message message) {

            if (message.From.Id != tBase.bot.BotId && message.Chat.Id == message.From.Id){
                tBase.AcceptMessageToBot(message);
                return;
            }
            if (!tBase.IsChatFromUnion(message.Chat.Id))
                return;

            if(message.Entities != null
            && message.Entities.FirstOrDefault(item => item.Type == MessageEntityType.BotCommand) != default) {
                tBase.AcceptCommand(message);
                return;
            }

            tBase.AcceptMessageToBaseChat(message);            
        }

        //Принимаем любые обратные вызовы
        private void AcceptAnyCallback(CallbackQuery callback) {
            if (callback.From.Id != tBase.bot.BotId) {
                tBase.AcceptCallbackToBot(callback);
            }
        }

        private void AcceptAnyChatJoinRequest(ChatJoinRequest chatJoinRequest) {
            tBase.AcceptChatJoinRequest(chatJoinRequest);
        }

        private void AcceptAnyPoolAnswer (PollAnswer pollAnswer) {
            tBase.ApproveOrNotToBaseChat(pollAnswer);
        }

        async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken) {
            try {
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.Data);
                Console.WriteLine(exception.HResult);
                Console.WriteLine(exception.StackTrace);
                Console.WriteLine(exception.Source);
            } catch { }
        }

        #endregion
    }
}
