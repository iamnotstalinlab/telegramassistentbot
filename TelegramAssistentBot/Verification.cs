//Модуль объекта автора обращения.
//

using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using TelegramAssistentBot.Models;
using static TelegramAssistentBot.PersonInfo;

namespace TelegramAssistentBot{
    //Класс удостоверения
    public class VkVerification {
        public int VK_Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int Sex { get; set; }
        public string BirthDay { get; set; }
        public string HashString { get; set; }
        public string Alias { get; set; }

        public VkVerification() { }
        public VkVerification(int vk_Id, string firstName, string lastName, int sex, string birthDay, string alias) {
            VK_Id = vk_Id;
            FirstName = firstName;
            LastName = lastName;
            Sex = sex;
            BirthDay = birthDay;
            HashString = GetVerificationString(this);
            Alias = alias;
        }


        private string GetVerificationString(VkVerification verifiedProfile) {
            return $"{verifiedProfile.FirstName}/{verifiedProfile.LastName}/{verifiedProfile.Sex}/{verifiedProfile.BirthDay}";
        }
    }



    public class VerificationServer{
        // string redirectUri = "https://iamnotstalinlab.webtm.ru/";
        string redirectUri = "https://26c3-109-110-66-249.ngrok-free.app";
        private int AppId { get; set; }
        private string AppServiceKey { get; set; }

        public event EventHandler<VerificationEventArgs> OnVerificationIsPassed;
        public class VerificationEventArgs {
            public long TelegramUserId { get; set; }
            public VkVerification Verification { get; set; }
            public string Status { get; set; }
        }

        public VerificationServer(int appId, string serviceKey) {
            AppId = appId;
            AppServiceKey = serviceKey;
        }

        Thread thread;
        bool isLiveServer = false;
        public void ResetAppIdAndServiceKey(int appId, string serviceKey) {
            AppId = appId;
            AppServiceKey = serviceKey;
        }

        public void StartVerificationServer(){
            isLiveServer = true;

            thread = new Thread(new ThreadStart(HttpServerRun));
            thread.Start();
        }

        public void StopServer() {
            isLiveServer = false;
        }

        string serverAddress = "http://*:8080/";
        HttpListener server;

        private async void HttpServerRun() {
            
            server = new HttpListener();
            server.Prefixes.Add(serverAddress);
            server.Start(); // начинаем прослушивать входящие подключения

            while (isLiveServer) {
                try {

                    var context = await server.GetContextAsync();
                    var request = context.Request;  // получаем данные запроса

                    Console.WriteLine("Пришёл запрос в " + DateTime.Now.ToString("dd/MM/YYYY hh:mm"));

                    if (request.UrlReferrer !=null
                        && request.UrlReferrer.Host.Contains("id.vk.com")
                        && request.QueryString.Count >0) {

                        string data = System.Uri.UnescapeDataString(request.QueryString["payload"]);
                        string state = System.Uri.UnescapeDataString(request.QueryString["state"]);
                        VK_VerificationRequest vkIdPayload = JsonConvert.DeserializeObject<VK_VerificationRequest>(data);

                        var response = context.Response;    // получаем объект для установки ответа
                        SendOkResponse(response);
                        

                        string data1 = $"v=5.131&token={vkIdPayload.token}&access_token={AppServiceKey}&uuid={vkIdPayload.uuid}";
                        string url1 = $"https://api.vk.com/method/auth.exchangeSilentAuthToken?" + data1;

                        string resp = SendVKAnswer(url1);
                        VkIdAccessToken vkIdAccessToken = JsonConvert.DeserializeObject<VkIdAccessTokenResponse>(resp).response;
                        if(vkIdAccessToken != null) {

                            string data2 = $"v=5.131&access_token={vkIdAccessToken.access_token}";
                            string url2 = $"https://api.vk.com/method/account.getProfileInfo?" + data2;

                            resp = SendVKAnswer(url2);
                            VkIdProfileInfo vkIdProfileInfo = JsonConvert.DeserializeObject<VkIdProfileInfoResponse>(resp).response;
                            string text = string.Empty;

                            if (vkIdProfileInfo.Account_verification_profile != null) {
                                text = $"ID <b>Telegram</b>: <code> {state} </code>  подтверждён." +
                                    $"\n\n<b>VK ID</b>: <code>{vkIdProfileInfo.id}</code>" +
                                    $"\n\n<b>Имя</b>:\t{vkIdProfileInfo.Account_verification_profile.first_name}" +
                                    $"\n<b>Фамилия</b>:\t{vkIdProfileInfo.Account_verification_profile.last_name}" +
                                    $"\n<b>Отчество</b>:\t{vkIdProfileInfo.Account_verification_profile.middle_name}" +
                                    $"\n<b>Пол</b>:\t{vkIdProfileInfo.Account_verification_profile.sex}" +
                                    $"\n<b>Дата рождения</b>:\t{vkIdProfileInfo.Account_verification_profile.birthdate}";


                                OnVerificationIsPassed?.Invoke(this, new VerificationEventArgs {
                                    TelegramUserId = long.Parse(state),
                                    Verification = new VkVerification(
                                        vkIdProfileInfo.id,
                                        vkIdProfileInfo.Account_verification_profile.first_name,
                                        vkIdProfileInfo.Account_verification_profile.last_name,
                                        vkIdProfileInfo.Account_verification_profile.sex,
                                        vkIdProfileInfo.Account_verification_profile.birthdate,
                                        "Достоверный"),
                                    Status ="Успешно"
                                }) ;
                            }
                        } else {
                            OnVerificationIsPassed?.Invoke(this, new VerificationEventArgs {
                                TelegramUserId = long.Parse(state),
                                Status = "Ошибка настройки сервиса"
                            });
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            // получаем контекст
            server.Stop(); // останавливаем сервер
        }

        private void SendOkResponse(HttpListenerResponse response){
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/plain";
            response.OutputStream.Write(new byte[] { }, 0, 0);
            response.OutputStream.Close();
        }

        private bool IsVerifictationInformationFull(AccountVerificationProfile information) {

            if(information ==null)
                return false;

            if (string.IsNullOrEmpty(information.first_name)
                    || string.IsNullOrEmpty(information.last_name)
                    || information.sex == 0
                    || string.IsNullOrEmpty(information.birthdate))
                return false;

            return true;
        }



        private string SendVKAnswer(string url) {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(url).Result;

            string result = response.Content.ReadAsStringAsync().Result;
            return result;
        }

        //Авторизуемся
        public string GetVkontakteIDAuthorizationUri(long tgUserId) {
            string uuid = "261B29DEE24729842FD6433A60CAC66AD0CB2A6E52EBBC1E5C3464794E735131";
            string redirect_state = tgUserId.ToString();

            string req = $"uuid={uuid}" +
                $"&app_id={AppId}" +
                $"&response_type=silent_token" +
                $"&redirect_uri={redirectUri}" +
                $"&redirect_state={redirect_state}";


            return $"https://id.vk.com/auth?" + req;
        }



        private void SendPostVKontakteID(string url, string request) {
            HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Host = "id.vk.com";

            var data = new StringContent(request, Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage resp = client.GetAsync(url).Result;
            string result = resp.Content.ReadAsStringAsync().Result;
            //HttpResponseMessage response = client.PostAsync(url, data).Result;
            //string result = response.Content.ReadAsStringAsync().Result;
            Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(result));
        }



    }
}

