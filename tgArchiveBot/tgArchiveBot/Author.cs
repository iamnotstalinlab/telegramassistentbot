//Модуль объекта автора обращения.
//
namespace tgArchiveBot {
    public class Author {
        public long Id { get;}
        public string CallbackData { get; set; } = "null";
        public int IdEditMessage { get; set; }

        public ChatsLinkedObject ChatsLinkedObject {get;set;}
        //Создаём экземпляр автора
        public Author(long idUser) {
            Id = idUser;
        }
    }
}
