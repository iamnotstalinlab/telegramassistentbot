using System;

namespace tgArchiveBot;

public class ChatObject{
    public long Id{get;set;}
    public string Title {get;set;}
    public string Link {get;set;}

    public ChatObject(long id, string title, string link){
        Id =id;
        Title = title;
        Link = link;
    }
}

public class ChatBot{
    public string Token {get;set;}
    public long Id {get;set;}
    public string Username {get;set;}

    public ChatBot(long id, string token){
        Id =id;
        Token =token;
    }
}

public class ChatsLinkedObject {
    public long ChairmanId {get;set;}
    public ChatObject SourceChat {get;set;}
    public ChatObject ArchiveChat {get;set;}
    public ChatBot Bot {get;set;}

    public bool IsDeleting {get;set;}
    public ChatsLinkedObject(){}

}
