using System;
using Microsoft.Data.Sqlite;
using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace tgArchiveBot;
public class ArchiveBot : BaseBot{

    string callback;
    List<TelegramBotClient> bots;
    List<CancellationTokenSource> cancellationTokens;
    Dictionary<long, ChatsLinkedObject> chats;
    ChatsLinkedObject newChatsObj;
    ITelegramBotClient newBot;
    InlineKeyboardMarkup keyboard;
    InlineKeyboardButton cancelBtn = InlineKeyboardButton.WithCallbackData("Отмена", "CancelCallback");
    

    public ArchiveBot(){

        CreateTableGroupChats(connection);
        ReloadArchivedObjects();
        baseBot.StartReceiving(new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync), BaseRecieverOption);
        
        keyboard = new InlineKeyboardMarkup(new[]{cancelBtn});
        OnBaseBotCreated += ArchiveBot_OnBaseBotCreated;
        
        

    }
    private void ArchiveBot_OnBaseBotCreated(object sender, EventArgs eventArgs){
        ShowPannel(ChairmanId, "text", "CancelCallback");
    }

    DefaultUpdateHandler updateHandler;
    CancellationTokenSource cancellationTokenSource;
    private void ReloadArchivedObjects(){
        if(chats !=null)
            chats.Clear();

        chats =GetChats();
        if(chats ==null){
            Console.WriteLine("Не смог найти чаты в базе данных. | I could not find chats in the data base.");
            return;
        }
        if(bots ==null)
            bots =new List<TelegramBotClient>();
        else
            bots.Clear();

        if(cancellationTokens ==null)
            cancellationTokens =new List<CancellationTokenSource>();
            else{
                foreach(var cancel in cancellationTokens)
                    cancel.Cancel();            
                cancellationTokens.Clear();
            }

        if(updateHandler ==null)
            updateHandler = new DefaultUpdateHandler(HandleUpdateArchivatorAsync, HandleErrorAsync);
        for(int i =0; i <chats.Count; ++i){
            try{
                if(baseBot.BotId != chats.ElementAt(i).Value.Bot.Id){

                    bots.Add(new TelegramBotClient(chats.ElementAt(i).Value.Bot.Token));
                    cancellationTokens.Add(new CancellationTokenSource());

                    bots[i].StartReceiving(updateHandler, BaseRecieverOption
                        , cancellationTokens[i].Token);
                    
                }
            }catch (Exception ex){
                Console.WriteLine(ex.Data);
                Console.WriteLine(ex.Message);
                Console.WriteLine($"Проблема со связкой (A) {chats.ElementAt(i).Value.SourceChat.Title}"
                +$"(B) {chats.ElementAt(i).Value.ArchiveChat.Title} и ботом @{chats.ElementAt(i).Value.Bot.Username}");
            }
        }
        // authors =new Dictionary<long, Author>();
    }

    //Обрабатываем ошибки пойманные Телеграмом
    private async Task HandleErrorAsync(ITelegramBotClient client, Exception ex, CancellationToken token){
        bots =bots;
        Console.WriteLine("->" +client.BotId.ToString() +"\n\n" + client.GetMyNameAsync());
        Console.WriteLine(ex.Message);
        Console.WriteLine(ex.Data);
        Console.WriteLine(ex.HResult);
        Console.WriteLine(ex.StackTrace);
        Console.WriteLine(ex.Source);
    }

//Обрабатываем все обновления
    private async Task HandleUpdateAsync(ITelegramBotClient client, Update update, CancellationToken token){
        switch(update.Type){
            case UpdateType.Message:
                AcceptMessage(update.Message, ref client);
            break;
            case UpdateType.CallbackQuery:
                AcceptCallback(update.CallbackQuery, ref client);
            break;
        }
    }
    
    private async Task HandleUpdateArchivatorAsync(ITelegramBotClient client, Update update, CancellationToken token ){
        if(update.Type != UpdateType.Message || update.Message ==null || update.Message.Chat ==null)
            return;
        
            if(chats[update.Message.Chat.Id].Bot.Id == client.BotId)
                SendToArchive(update.Message, ref client);
    }
    

    
    //Принмаем сообщение
    protected void AcceptMessage(Message inMessage, ref ITelegramBotClient botClient){
        if(inMessage ==null || inMessage.From ==null
        || inMessage.From.Id == baseBot.BotId)
            return;

        if(ChairmanId ==0){
            AcceptBaseMessage(inMessage);
            return;
        }

        if(botClient.BotId ==baseBot.BotId && inMessage.Chat.Id == ChairmanId){
            if(callback == "AddNewArchiveObjectCallback")
                CreateChatsLinkedObject(ref inMessage, ref botClient);
            else
                ShowStartPannel(ChairmanId);
            return;
        }
    }

    //Принимаем отзыв
    protected void AcceptCallback(CallbackQuery inCallback, ref ITelegramBotClient botClient){
        if(inCallback ==null) return;

        switch(inCallback.Data){
            case "SetChairman":
                AcceptBaseCallback(inCallback);
            break;
            case  "CancelCallback":
                ResetNewLinkedObject();
                ShowStartPannel(ChairmanId);
            break;
            case "CheckCallback":
                ShowPannel(inCallback.From.Id, "text", "CancelCallback");
            break;
            case "SaveChatLinkedObject":
                SaveOrDeletNewLinkedObject(true);
                ResetNewLinkedObject();
                ReloadArchivedObjects();
                ShowStartPannel(inCallback.From.Id);
            break;
            case "DeleteChatsLinkedObjectCallback":
                DeleteChatsLinkedObject();
                ResetNewLinkedObject();
                ReloadArchivedObjects();
                ShowStartPannel(inCallback.From.Id);
            break;

            case "AddNewArchiveObjectCallback":
                callback ="AddNewArchiveObjectCallback";
                ShowPannel(inCallback.From.Id, "...","CancelCallback");
            break;
        }
    
    }

     //Отправляем сообщение в архив
    protected void SendToArchive(Message message, ref ITelegramBotClient archBot) {
        if (!chats.ContainsKey(message.Chat.Id) || chats[message.Chat.Id] == null 
        || chats[message.Chat.Id].ArchiveChat == null) 
            return;
        if(message ==null || message.From ==null || string.IsNullOrEmpty(message.From.FirstName))
            return;
            
        ChatsLinkedObject chatObj = chats[message.Chat.Id];
        int idMessage = 0;
        try {
            idMessage = archBot.ForwardMessageAsync(chatObj.ArchiveChat.Id
            , chatObj.SourceChat.Id, message.MessageId, disableNotification: true
            , protectContent: false).Result.MessageId;
        } catch (Exception ex){ 

            Console.WriteLine(ex.Message);
        Console.WriteLine(ex.Data);
        Console.WriteLine(ex.HResult);
        Console.WriteLine(ex.StackTrace);
        Console.WriteLine(ex.Source);
        }

        string text = $"Сообщение <b>№{message.MessageId}</b> | Тема <b>№{message.MessageThreadId}</b>" +
            $"\n<code>{GetMessageArchiveNumber(message)}</code> [<a href=\"{chatObj.SourceChat.Link}\">Чат</a>]" +
            $"\n\n{GetLinkFromMessage(message)}" +
            $"\n{GetLinkFromMessage(message, true)}" +
            $"\n{GetLinkFromMessage(message, false, true)}" +
            $"\n\nАвтор: {message.From.FirstName} [ <code>{message.From.Id}</code> ]" +
            $"\nПрислано: <i>{message.Date.AddHours(3).ToString("HH:mm:ss dd/MM/yyyy")} (+3 МСК)</i>";

        archBot.SendTextMessageAsync(
            chatId: chatObj.ArchiveChat.Id
            ,text: text
            ,replyParameters: new ReplyParameters { MessageId = idMessage}
            ,linkPreviewOptions: baseLinkPreviewOptions
            ,parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
        
    }

    //Получаем ссылку по сообщению
        protected string GetLinkFromMessage(Message message, bool IsTopicMessage =false, bool IsThreadMessage =false) {
            if (string.IsNullOrEmpty(message.Chat.Username)) {
                if (message.MessageThreadId == null) {
                    if (IsTopicMessage) 
                        if (message.MessageThreadId == null)
                            return $"https://t.me/{message.Chat.Username.Trim('@')}/1/{message.MessageId}";
                        else
                            if(IsThreadMessage)
                                return $"https://t.me/c/{message.Chat.Id.ToString().Substring(4)}/{message.MessageThreadId}/{message.MessageId}";
                            else
                                return $"https://t.me/c/{message.Chat.Id.ToString().Substring(4)}/1/{message.MessageId}";
                    else
                        return $"https://t.me/c/{message.Chat.Id.ToString().Substring(4)}/{message.MessageId}";
                } else
                    return $"https://t.me/c/{message.Chat.Id.ToString().Substring(4)}"
                        +$"/{message.MessageThreadId}/{message.MessageId}";
            } else {
                if (IsTopicMessage) 
                    if (message.MessageThreadId == null)
                        return $"https://t.me/{message.Chat.Username.Trim('@')}/1/{message.MessageId}";
                    else
                        if(IsThreadMessage)
                            return $"https://t.me/{message.Chat.Username.Trim('@')}/{message.MessageThreadId}/{message.MessageId}";
                        else
                            return $"https://t.me/{message.Chat.Username.Trim('@')}/1/{message.MessageId}";
                else
                    return $"https://t.me/{message.Chat.Username.Trim('@')}/{message.MessageId}";
            }
        }

    //Получаем поисковой номер для сообщения в архиве
        protected string GetMessageArchiveNumber(Message message) {
            if(message != null)
                return $"N{message.MessageId} | T{message.MessageThreadId}" +
                $" | {message.Date.AddHours(3).ToString("dd/MM/yyyy HH:mm:ss")} (+3 МСК)";else 
            return null;
        }
    
    
    //Сбрасываем новый объект связки
    private void ResetNewLinkedObject(){
        newBot =null;
        newChatsObj =null;
        callback =null;
    }

    
    private void CreateChatsLinkedObject(ref Message inMessage, ref ITelegramBotClient botClient){
        if(inMessage ==null || inMessage.From ==null || inMessage.From.Id != ChairmanId) return;

        if(newBot ==null)
            SetLinkedBot(ref inMessage, ref botClient);
            else{
                if(newChatsObj.SourceChat ==null || newChatsObj.ArchiveChat ==null){
                    SetLinkedChats(inMessage.From.Id, inMessage, "CancelCallback");
                }
            }
    }

    private bool IsNewBotAnAdminInArchiveChat(){
        try{
            return (Array.FindIndex(newBot.GetChatAdministratorsAsync(newChatsObj.ArchiveChat.Id)
                .Result, w => w.User.Id ==newBot.BotId) != -1);        
        }catch{
            return false;
        }
        
    }

    private bool IsNewBotMemberOfSourceChat(){
        // return (newBot.GetChatMemberAsync(newChatsObj.SourceChat.Id, newBot.BotId).Result) !=null;
        return true;
    }

    private void SetLinkedBot(ref Message inMessage, ref ITelegramBotClient botClient){
        if(inMessage ==null || string.IsNullOrEmpty(inMessage.Text))
            return;
        
        try{
            newBot = new TelegramBotClient(inMessage.Text);
            if(newChatsObj ==null)
                newChatsObj =new ChatsLinkedObject();
            newChatsObj.Bot =new ChatBot(newBot.BotId, inMessage.Text);
            newChatsObj.Bot.Username =newBot.GetMeAsync().Result.Username;
        }catch(Exception ex){
            Console.WriteLine(ex.Data);
            Console.WriteLine(ex.Message);
            ShowPannel(ChairmanId, "text", "CancelCallback");
            return;
        }
        ShowPannel(ChairmanId, "text", "CancelCallback");
    }

    
    private void SendPannel(long idChat, ref ITelegramBotClient botClient, string htmlText
    , InlineKeyboardMarkup inInlineKeyboard){
        
        botClient.SendTextMessageAsync(chatId: idChat 
            , text: htmlText
            , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
            , linkPreviewOptions: baseLinkPreviewOptions
            , disableNotification: false, replyMarkup: inInlineKeyboard);
        

    }

    //Устанавливаем адрес чата/канала
    private void SetLinkedChats(long idUser, Message message, string cancelCallback) {
// name.Replace("<", "*").Replace(">", "*").Replace("/", "*")
        if(message != null && !string.IsNullOrEmpty(message.Text) && message.Text.Contains("@")) {
                try {
                    Telegram.Bot.Types.ChatFullInfo chatInfo = baseBot.GetChatAsync(message.Text).Result;
                    AcceptChatInfo(idUser, chatInfo, cancelCallback);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.Data);
                    Console.WriteLine(ex.HResult);
                    Console.WriteLine(ex.StackTrace);
                    Console.WriteLine(ex.Source);

                    // ShowChatAddressPage(idUser, cancelCallback);
                }
        } else {
            ShowPannel(ChairmanId, "Пришлите, пожалуйста, адрес исходящего чата (из которого будем пересылать сообщения в архив)."+
            "\n\nПример:\n@iamnotstalin", cancelCallback);
        }
    }

    InlineKeyboardMarkup startKeyboard = new InlineKeyboardMarkup(new[]{
            InlineKeyboardButton.WithCallbackData("Добавить/Удалить", "AddNewArchiveObjectCallback") });
    private void ShowStartPannel(long idUser){
        string text ="<b>ДЕЙСТВУЮЩИЕ АРХИВЫ:</b>\n";
        if(chats !=null){
            int i=0;
            foreach(var chat in chats){
                text += $"\n{++i})" +BuildArchiveLine(chat.Value);
            }
        }else
            text +="Пока нет связанных архивных чатов. Добавьте пожалуйста, по кнопке ниже.";
        SendBaseMessage(idUser, text, startKeyboard);
    }



    //Показываем страницу адреса чата/канала

    string s1 = "1) Пришлите токен бота-архиватора.\n<i>(Бот, который будет пересылать сообщения в архив.)</i>";
    string s2 = "2) Пришлите исходящий чат.\n<i>(Из которого будем пересылать.)</i>";
    string s3 = "3) Пришлите архивный чат.\n<i>(В который будем пересылать.)</i>";
    string s4 = "4) Назначьте бота-архиватора админом в архивный чат.\n<i>(Для успешной работы с архивом.)</i>";

    InlineKeyboardButton rightButton;
    private void ShowPannel(long idChat, string curText, string cancelCallback) {
        InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]{
            InlineKeyboardButton.WithCallbackData("Отмена", cancelCallback) });

        string text = $"СОЗДАËМ СВЯЗКУ ЧАТА С АРХИВОМ" ;
        string callbackData ="null";
        if(newChatsObj ==null || newChatsObj.Bot ==null || newChatsObj.Bot.Id ==0){
            text +="\n\n➡️ <b>" +s1 +"</b>" +"\n\n" +"🕑 "+s2 +"\n\n" +"🕑"+s3+ "\n\n🕑 "+s4;
        }else{
            text += BuildBotPannelString(1, newBot.BotId, newChatsObj.Bot.Username);
            if(newChatsObj == null || newChatsObj.SourceChat ==null)
                text +="\n\n➡️ <b>" +s2 +"</b>\n\n" +"🕑 "+s3 +"\n\n🕑 "+s4;
            else{
                text += BuildChatPannelString(2, newChatsObj.SourceChat.Id
                    , newChatsObj.SourceChat.Title, newChatsObj.SourceChat.Link);

                if(newChatsObj.ArchiveChat ==null){
                    text +="\n\n➡️ <b>" +s3+"</b>" +"\n\n🕑 "+s4;
                }else{
                    text += BuildChatPannelString(3, newChatsObj.ArchiveChat.Id
                        , newChatsObj.ArchiveChat.Title,  newChatsObj.ArchiveChat.Link);
                    if(!IsNewBotAnAdminInArchiveChat() || !IsNewBotMemberOfSourceChat()){
                        if(!IsNewBotAnAdminInArchiveChat()){
                            text +="\n\n➡️ <b>"+$"{BuildAdminPannelString(false)}" +"</b>";
                            callbackData ="CheckCallback";
                        }else{
                            text +="\n\n"+$"{BuildAdminPannelString(true)}";
                            if(!IsNewBotMemberOfSourceChat()){
                                text +="\n\n➡️ <b>"+$"{BuildSourceChatMemberPannelString(false)}" +"</b>";
                                callbackData ="CheckCallback";
                            }else
                                text +="\n\n➡️ <b>"+$"{BuildSourceChatMemberPannelString(true)}" +"</b>";

                        }
                            keyboard = keyboard.AddButton(InlineKeyboardButton.WithCallbackData("Проверить"
                                , callbackData));
                    }else{
                        
                        text = BuildSavePannelString();
                        callbackData ="SaveChatLinkedObject";
                        keyboard = keyboard.AddButton(InlineKeyboardButton.WithCallbackData("Сохранить"
                            , callbackData));
                    }

    //
                    }
                }
            }
            SendBaseMessage(ChairmanId, text, keyboard);
        }

    private string BuildChatPannelString(int num,  long chatId,  string chatTitle, string chatLink){
        if(num ==2){
            if(newChatsObj ==null || newChatsObj.SourceChat.Id ==0 
            || string.IsNullOrEmpty(newChatsObj.SourceChat.Title) || string.IsNullOrEmpty(newChatsObj.SourceChat.Link))
                return null;
            
            return $"\n\n✅ {num}) Исходящим чатом установлен"
            +$": <a href=\"{newChatsObj.SourceChat.Link}\">{newChatsObj.SourceChat.Title}</a>"
            +$" [<code>{newChatsObj.SourceChat.Id}</code>]";

        }else{
            if(newChatsObj ==null || newChatsObj.ArchiveChat.Id ==0 
            || string.IsNullOrEmpty(newChatsObj.ArchiveChat.Title) || string.IsNullOrEmpty(newChatsObj.ArchiveChat.Link))
                return null;

            return $"\n\n✅ {num}) Архивным чатом установлен"
            +$": <a href=\"{newChatsObj.ArchiveChat.Link}\">{newChatsObj.ArchiveChat.Title}</a>"
            +$" [<code>{newChatsObj.ArchiveChat.Id}</code>]";
        }
    }

    private string BuildBotPannelString(int num, long botId,  string botUsername){
        if(botId ==0 || string.IsNullOrEmpty(botUsername))
            return null;
        
        return $"\n\n✅ {num}) Ботом-архиватором чатов назначен: @{botUsername}"+
            $" [<code>{botId}</code>]";
    }


    private string BuildAdminPannelString(bool isAdmin){
        if(!IsBotAndArchiveChatExisted())
            return null;

        if(!isAdmin)
            return $"4) Назначьте бота @{newChatsObj.Bot.Username} админом в архивный чат"
            +$" <a href=\"{newChatsObj.ArchiveChat.Link}\">{newChatsObj.ArchiveChat.Title}</a>."
            +"\n<i>(Для успешной работы с архивом.)</i>";
        else
            return $"\n\n✅ 4) Бот @{newChatsObj.Bot.Username} назначен админом в архивный чат"
            +$" <a href=\"{newChatsObj.ArchiveChat.Link}\">{newChatsObj.ArchiveChat.Title}</a>.";
    }

    private bool IsBotAndArchiveChatExisted(){
        if(newChatsObj ==null || newChatsObj.Bot ==null ||newChatsObj.ArchiveChat ==null 
        || string.IsNullOrEmpty(newChatsObj.ArchiveChat.Title) || string.IsNullOrEmpty(newChatsObj.ArchiveChat.Link))
            return false;
        return true;
    }

    private bool IsBotAndSourceChatExisted(){
        if(newChatsObj ==null || newChatsObj.Bot ==null ||newChatsObj.SourceChat ==null 
        || string.IsNullOrEmpty(newChatsObj.SourceChat.Title) || string.IsNullOrEmpty(newChatsObj.SourceChat.Link))
            return false;
        return true;
    }

    private bool IsPreparedForSaving(){
        if(!IsBotAndSourceChatExisted() || !IsBotAndArchiveChatExisted())
            return false;
        return true;
    }

    private string BuildSourceChatMemberPannelString(bool isMember){
            if(!IsBotAndArchiveChatExisted())
                return null;
            
            if(!isMember)
                return $"\n\n5) Добавьте бота @{newChatsObj.Bot.Username} в исходный чат"
                +$" <a href=\"{newChatsObj.SourceChat.Link}\">{newChatsObj.SourceChat.Title}</a>."
                +"\n<i>(Для успешной работы с архивом.)</i>";
            else
                return $"\n\n✅ 5) Бот @{newChatsObj.Bot.Username} добавлен в исходный чат"
                +$" <a href=\"{newChatsObj.SourceChat.Link}\">{newChatsObj.SourceChat.Title}</a>.";
        }

    private string BuildSavePannelString(){
        if(!IsBotAndSourceChatExisted() || !IsBotAndSourceChatExisted())
            return null;

        return $"СОЗДАНА СВЯЗКА АРХИВНОГО ЧАТА:\n" + BuildArchiveLine(newChatsObj);
    }
    
    InlineKeyboardMarkup deleteKeyboard = new InlineKeyboardMarkup(new[]{
        InlineKeyboardButton.WithCallbackData("Отмена", "CancelCallback"),
        InlineKeyboardButton.WithCallbackData("Удалить", "DeleteChatsLinkedObjectCallback") });
    private void AcceptChatInfo(long idUser, ChatFullInfo chat, string cancelCallback) {
        
        if(string.IsNullOrEmpty(chat.Title) || string.IsNullOrEmpty(chat.InviteLink)){
            SendBaseMessage(idUser,"<b>Назначьте меня администратором в чат с возможностями (отправлять сообщения и тд.).</b>"+
                "\n(Не доступны название или пригласительная ссылка).", null);
            return;
        }


        if(IsSourceChatExisted(chat.Id))
            return;
        if(newChatsObj ==null)
            newChatsObj =new ChatsLinkedObject();

        string text =string.Empty ;
        if(newChatsObj.SourceChat ==null){
            newChatsObj.SourceChat =new ChatObject(chat.Id, chat.Title, chat.InviteLink);
            text ="Супер! Теперь пришлите адрес чата арихва (в который будем пересылать)";
        }else
            newChatsObj.ArchiveChat = new ChatObject(chat.Id, chat.Title, chat.InviteLink);

        ShowPannel(ChairmanId, text , "CancelCallback");
    }

    protected bool IsSourceChatExisted(long checkedChatId ){
    
        ChatsLinkedObject exChat =GetOneChat(checkedChatId);
        if(exChat ==null)
            return false;
        newChatsObj =exChat;
        SendBaseMessage(ChairmanId, "Чат уже связан с архивом:\n" +BuildArchiveLine(exChat), deleteKeyboard);
        return true;
    }

    //Собираем строку текста для действующих чатов
    protected string BuildArchiveLine(ChatsLinkedObject chatObj){
        if(chatObj ==null || chatObj.SourceChat ==null || chatObj.ArchiveChat ==null
            || string.IsNullOrEmpty(chatObj.SourceChat.Link) || string.IsNullOrEmpty(chatObj.ArchiveChat.Link)
            || string.IsNullOrEmpty(chatObj.SourceChat.Title) || string.IsNullOrEmpty(chatObj.ArchiveChat.Title))
            return null;
        
        string tempTitle =chatObj.SourceChat.Title;
        string text = $" 🅰️ <a href=\"{chatObj.SourceChat.Link}\">{tempTitle
            .Substring(0,tempTitle.Length <26 ? tempTitle.Length : 25)}...</a>";

        tempTitle =chatObj.ArchiveChat.Title;
        text += $"➡️ <a href=\"t.me/{chatObj.Bot.Username}\">Бот</a> ➡️";
            
        text +=$" 🅱️ <a href=\"{chatObj.ArchiveChat.Link}\">{chatObj.ArchiveChat.Title
            .Substring(0,tempTitle.Length <16 ? tempTitle.Length : 15)}...</a>";
        
        return text;
    }
        //Создаём основную таблицу
    private void CreateTableGroupChats(SqliteConnection cnct) {

        OpenDatabase();
        string sCommand = @"CREATE TABLE IF NOT EXISTS ChatsObjects (
            SourceChatId   INTEGER NOT NULL UNIQUE,
            SourceChatTitle     TEXT,
            SourceChatLink      TEXT,
            
            ArchiveChatId   INTEGER NOT NULL UNIQUE,
            ArchiveChatTitle     TEXT,
            ArchiveChatLink TEXT,
            
            ChairmanId    INTEGER NOT NULL,
            BotToken    TEXT,
            BotId   INTEGER NOT NULL UNIQUE,
            BotUsername     TEXT);";

        SqliteCommand command = new SqliteCommand();
        command.Connection = cnct;
        command.CommandText = sCommand;
        command.ExecuteNonQuery();

        CloseDatabase();
    }

    //Получаем список союзных объединений
    protected Dictionary<long, ChatsLinkedObject> GetChats() {
        lock (dbLocker) {
            OpenDatabase();

            string sCommand = "SELECT * FROM ChatsObjects";

            SqliteCommand command = new SqliteCommand();
            command.CommandText = sCommand;
            command.Connection = connection;

            dataReader = command.ExecuteReader();
            Dictionary<long, ChatsLinkedObject> chatObjects =null;

            if (dataReader.HasRows) {

                long idChat = 0;
                ChatsLinkedObject chatObj = null;
                chatObjects = new Dictionary<long, ChatsLinkedObject>();

                while (dataReader.Read()) {

                    chatObj = new ChatsLinkedObject();
                    chatObj.SourceChat =new ChatObject(long.Parse(dataReader["SourceChatId"].ToString())
                        , dataReader["SourceChatTitle"].ToString()
                        , dataReader["SourceChatLink"].ToString());

                    chatObj.ArchiveChat =new ChatObject(long.Parse(dataReader["ArchiveChatId"].ToString())
                        , dataReader["ArchiveChatTitle"].ToString()
                        , dataReader["ArchiveChatLink"].ToString());
                    
                    chatObj.ChairmanId =long.Parse(dataReader["ChairmanId"].ToString());
                    chatObj.Bot =new ChatBot(long.Parse(dataReader["BotId"].ToString())
                        , dataReader["BotToken"].ToString());
                    chatObj.Bot.Username =dataReader["BotUsername"].ToString();
                    chatObjects.Add(chatObj.SourceChat.Id, chatObj);
                }
            }
            dataReader.Close();
            CloseDatabase();
            return chatObjects;
        }
    }

    //Сохраняем результат группового обращения
    private void SaveOrDeletNewLinkedObject(bool isSave) {
        if(!IsPreparedForSaving())
            return;

        lock(dbLocker) {
            OpenDatabase();
            string sCommand;

            if(isSave) {
                sCommand = "INSERT INTO ChatsObjects (SourceChatId, SourceChatTitle, SourceChatLink"+
                ", ArchiveChatId, ArchiveChatTitle, ArchiveChatLink, ChairmanId, BotToken, BotId, BotUsername)" +
                " VALUES(@sourceChatId, @sourceChatTitle, @sourceChatLink, @archiveChatId, @archiveChatTitle"+
                ", @archiveChatLink, @chairmanId, @botToken, @botId, @botUsername)" +
                    
                " ON CONFLICT(SourceChatId) DO UPDATE SET SourceChatId =@sourceChatId" +
                ", SourceChatTitle =@sourceChatTitle, SourceChatLink =@sourceChatLink"+
                ", ArchiveChatId =@archiveChatId, ArchiveChatTitle =@archiveChatTitle"+
                ", ArchiveChatLink =@archiveChatLink, ChairmanId =@chairmanId, BotToken =@botToken, BotId =@botId"+
                ", BotUsername =@botUsername";
            } else {
                sCommand = "DELETE FROM ChatsObjects WHERE SourceChatId =@sourceChatIdatId";
            }

            SqliteCommand command = new SqliteCommand();
            command.CommandText = sCommand;
            command.Connection = connection;

            command.Parameters.AddWithValue("@sourceChatId", newChatsObj.SourceChat.Id.ToString());
            command.Parameters.AddWithValue("@sourceChatTitle", newChatsObj.SourceChat.Title);
            command.Parameters.AddWithValue("@sourceChatLink", newChatsObj.SourceChat.Link);
            command.Parameters.AddWithValue("@archiveChatId", newChatsObj.ArchiveChat.Id.ToString());
            command.Parameters.AddWithValue("@archiveChatTitle", newChatsObj.ArchiveChat.Title);
            command.Parameters.AddWithValue("@archiveChatLink", newChatsObj.ArchiveChat.Link);
            command.Parameters.AddWithValue("@chairmanId", newChatsObj.ChairmanId);
            command.Parameters.AddWithValue("@botToken", newChatsObj.Bot.Token);
            command.Parameters.AddWithValue("@botId", newChatsObj.Bot.Id);
            command.Parameters.AddWithValue("@botUsername", newChatsObj.Bot.Username);

            command.ExecuteNonQuery();
            CloseDatabase();
        }
    }

    private void DeleteChatsLinkedObject(){
        if(newChatsObj ==null || newChatsObj.SourceChat ==null || newChatsObj.SourceChat.Id  ==0)
            return;

        lock(dbLocker) {
            OpenDatabase();
            string sCommand;

            sCommand = "DELETE FROM ChatsObjects WHERE SourceChatId =@sourceChatId";

            SqliteCommand command = new SqliteCommand();
            command.CommandText = sCommand;
            command.Connection = connection;

            command.Parameters.AddWithValue("@sourceChatId", newChatsObj.SourceChat.Id.ToString());
            command.ExecuteNonQuery();
            CloseDatabase();
        }
    }


    //Проверяем присутствует ли группа в союзе
    private ChatsLinkedObject GetOneChat(long chatId) {
        
        ChatsLinkedObject chatObj =null;
        lock(dbLocker) {
            OpenDatabase();
            string sCommand = "SELECT * FROM ChatsObjects WHERE SourceChatId =@sourceChatId";

            SqliteCommand command = new SqliteCommand();
            command.CommandText = sCommand;
            command.Connection = connection;
            command.Parameters.AddWithValue("@sourceChatId", chatId.ToString());

            dataReader = command.ExecuteReader();
            
            if(dataReader.HasRows) {
                chatObj = new ChatsLinkedObject();
                while (dataReader.Read()) {

                    chatObj.SourceChat =new ChatObject(long.Parse(dataReader["SourceChatId"].ToString())
                        , dataReader["SourceChatTitle"].ToString()
                        , dataReader["SourceChatLink"].ToString());

                    chatObj.ArchiveChat =new ChatObject(long.Parse(dataReader["ArchiveChatId"].ToString())
                        , dataReader["ArchiveChatTitle"].ToString()
                        , dataReader["ArchiveChatLink"].ToString());
                    
                    chatObj.ChairmanId =long.Parse(dataReader["ChairmanId"].ToString());
                    chatObj.Bot =new ChatBot(long.Parse(dataReader["BotId"].ToString())
                        , dataReader["BotToken"].ToString());
                    chatObj.Bot.Username =dataReader["BotUsername"].ToString();
                }
            }

            dataReader.Close();
            CloseDatabase();
            return chatObj;
        }
    }
}
