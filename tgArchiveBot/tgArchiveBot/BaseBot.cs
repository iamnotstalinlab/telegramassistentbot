using System;
using Microsoft.Data.Sqlite;
using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace tgArchiveBot;

public class BaseBot: DataBase{
    protected TelegramBotClient baseBot;
    public long ChairmanId {get;set;}
    private string BotToken {get;set;}
    protected ReceiverOptions BaseRecieverOption = new ReceiverOptions{ 
        AllowedUpdates = new UpdateType[] { UpdateType.Message, UpdateType.CallbackQuery }
    };


    public event EventHandler OnBaseBotCreated;  
    protected LinkPreviewOptions baseLinkPreviewOptions =new LinkPreviewOptions{ IsDisabled = true};
    protected BaseBot(){
        CreateDatabase();
        CreateBaseTable(connection);
        LoadBases();
        if(string.IsNullOrEmpty(BotToken)){
            Console.WriteLine("Здравствуйте, введите, пожалуйста, токен основного бота.");
            BotToken =Console.ReadLine();
            baseBot =new TelegramBotClient(BotToken);
        }

        baseBot = new TelegramBotClient(BotToken);

    }

    private async Task HandleErrorAsync(ITelegramBotClient client, Exception exception, CancellationToken token){}




    protected InlineKeyboardMarkup keyboard = null;
    protected void AcceptBaseMessage(Message message){

        InlineKeyboardButton yesButton = InlineKeyboardButton.WithCallbackData("Да, я руководитель.", "SetChairman");
        InlineKeyboardButton noButton = InlineKeyboardButton.WithCallbackData("Нет, я помогаю.", "NoAnswer");
        keyboard = new InlineKeyboardMarkup(new[]{yesButton, noButton});
        if(ChairmanId ==0){
            baseBot.SendTextMessageAsync(message.From.Id, "Вы руководитель?", replyMarkup: keyboard);
        }
    }

    protected void AcceptBaseCallback(CallbackQuery callback){
        if(ChairmanId ==0 && callback.Data =="SetChairman"){
            ChairmanId = callback.From.Id;
            SaveBaseBot_DB();
            OnBaseBotCreated?.Invoke(this, new EventArgs());
        }
    }



protected void SendBaseMessage(long inChatId, string inText, IReplyMarkup inReplyKeyboard){
    baseBot.SendTextMessageAsync(chatId: inChatId
        , text: inText
        , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
        , linkPreviewOptions: baseLinkPreviewOptions
        , disableNotification: false, replyMarkup: inReplyKeyboard);

}


    //Создаём основную таблицу
    private void CreateBaseTable(SqliteConnection cnct) {

        OpenDatabase();
        string sCommand = @"CREATE TABLE IF NOT EXISTS Base (
            ChairmanId    INTEGER NOT NULL UNIQUE,
            BotToken    TEXT);";

        SqliteCommand command = new SqliteCommand();
        command.Connection = cnct;
        command.CommandText = sCommand;
        command.ExecuteNonQuery();

        CloseDatabase();
    }

    //Сохраняем результат группового обращения
    private void SaveBaseBot_DB() {

        lock(dbLocker) {
            OpenDatabase();
            string sCommand;

            sCommand = "INSERT INTO Base (ChairmanId, BotToken) VALUES(@chairmanId, @botToken)" +
            " ON CONFLICT(ChairmanId) DO UPDATE SET ChairmanId =@chairmanId, BotToken =@botToken";

            SqliteCommand command = new SqliteCommand();
            command.CommandText = sCommand;
            command.Connection = connection;

            command.Parameters.AddWithValue("@chairmanId", ChairmanId);
            command.Parameters.AddWithValue("@botToken", BotToken);

            command.ExecuteNonQuery();
            CloseDatabase();
        }
    }

    //Получаем список союзных объединений
    protected void LoadBases() {
        lock (dbLocker) {
            OpenDatabase();

            string sCommand = "SELECT * FROM Base";

            SqliteCommand command = new SqliteCommand();
            command.CommandText = sCommand;
            command.Connection = connection;

            dataReader = command.ExecuteReader();
            if (dataReader.HasRows) {
                while (dataReader.Read()) {
                    ChairmanId =long.Parse(dataReader["ChairmanId"].ToString());
                    BotToken =dataReader["BotToken"].ToString();
                }
            }
            dataReader.Close();
            CloseDatabase();
        }
    }
}
